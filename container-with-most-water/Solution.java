public class Solution {
    public int maxArea(int[] height) {
        int area = 0;

        for (int i = 0, j = height.length - 1; i < j; )
            area = Math.max(area, (j-i)*(height[i] < height[j] ? height[i++] : height[j--]));

        return area;
    }

    public static void main(String [] args) {
    }
}
