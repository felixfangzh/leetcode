#include <iostream>
#include <vector>

using namespace std;

class Solution {
    public:
        int maxArea(vector<int> &height) {
            int area = 0;

            for (ssize_t i = 0, j = height.size()-1; i < j; )
                area = max(area, static_cast<int>((j-i)*(height[i] < height[j] ? height[i++] : height[j--])));

            return area;
        }
};

int main() {
    return 0;
}
