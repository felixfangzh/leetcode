#include <iostream>
#include <queue>
#include <utility>
#include <climits>
using namespace std;

struct TreeNode {
   int val;
   TreeNode *left;
   TreeNode *right;
   TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
   public:
      int sumNumbers(TreeNode *root) {
         int sum = 0;
         if (!root)
            return sum;
         queue<pair<TreeNode*, int> > node_queue;
         node_queue.push(make_pair(root, root->val));
         while (!node_queue.empty()) {
            pair<TreeNode*, int> node_pair = node_queue.front(); 
            node_queue.pop();
            if (!node_pair.first->left && !node_pair.first->right)
               sum += node_pair.second;
            if (node_pair.first->left)
               node_queue.push(make_pair(node_pair.first->left, node_pair.second*10 + node_pair.first->left->val));
            if (node_pair.first->right)
               node_queue.push(make_pair(node_pair.first->right, node_pair.second*10 + node_pair.first->right->val));
         }
         return sum;
      }
};

int main() {
   int vals [] = {1, 2, 3};
   vector<TreeNode*> nodes;
   for (auto i = begin(vals); i != end(vals); ++i) {
      nodes.push_back(*i == INT_MIN ? NULL : new TreeNode(*i));
   }
   for (int i = 0; i < nodes.size(); ++i) {
      if (!nodes[i])
         continue;
      if ((i+1)*2-1 < nodes.size())
         nodes[i]->left = nodes[(i+1)*2-1];
      if ((i+1)*2 < nodes.size())
         nodes[i]->right = nodes[(i+1)*2];
      cout << nodes[i]->val << ": (" 
         << (nodes[i]->left ? nodes[i]->left->val : INT_MIN)
         << ", "
         << (nodes[i]->right ? nodes[i]->right->val : INT_MIN)
         << "), " << endl;
   }
   Solution s;
   cout << s.sumNumbers(nodes[0]) << endl;
   return 0;
}
