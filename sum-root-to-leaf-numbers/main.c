/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
  * };
 */
void _sumNumbers(struct TreeNode *root, int *sum) {
    if (!root) {
        *sum = 0;
        return;
    }
    
    int newSum = 0;
    int tmp = *sum * 10 + root->val;
    
    *sum = tmp;
    _sumNumbers(root->left, sum);
    newSum += *sum;
    
    *sum = tmp;
    _sumNumbers(root->right, sum);
    newSum += *sum;
    
    *sum = newSum > tmp ? newSum : tmp;
}

int sumNumbers(struct TreeNode *root) {
    int sum = 0;
    _sumNumbers(root, &sum);
    return sum;
}
