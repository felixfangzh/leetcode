/**
 * Definition for binary tree
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    public int sumNumbers(TreeNode root) {
        if (root == null) return 0;
        
        int sum = 0;
        Deque<TreeNode> nodeQueue = new ArrayDeque<TreeNode>();
        Deque<Integer> valueQueue = new ArrayDeque<Integer>();
        
        nodeQueue.offer(root);
        valueQueue.offer(root.val);
        
        while (!nodeQueue.isEmpty()) {
            TreeNode node = nodeQueue.poll();
            Integer val = valueQueue.poll();
            
            if (node.left == null && node.right == null)
                sum += val;
            
            if (node.left != null) {
                nodeQueue.offer(node.left);
                valueQueue.offer(val * 10 + node.left.val);
            }
            
            if (node.right != null) {
                nodeQueue.offer(node.right);
                valueQueue.offer(val * 10 + node.right.val);
            }
        }
        
        return sum;
    }
}
