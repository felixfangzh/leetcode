public class Solution {
    public String longestPalindrome(String s) {
        int [] p = new int[s.length()*2 + 1];
        char [] s2 = new char[p.length];

        for (int i = 0; i < s2.length-1; i += 2) {
            s2[i] = '|';
            s2[i+1] = s.charAt(i/2);
        }
        s2[s2.length-1] = '|';

        int c = 0, r = 0;
        int m = 0, n = 0;

        for (int i = 1; i < s2.length; ++i) {
            if (i > r) {
                p[i] = 0; m = i-1; n = i+1;
            }
            else {
                int i2 = c*2 - i;
                if (p[i2] < r - i) {
                    p[i] = p[i2];
                    m = -1;
                }
                else {
                    p[i] = r - i;
                    n = r + 1; m = i*2 - n;
                }
            }

            for (; m >=0 && n < s2.length && s2[m] == s2[n]; --m, ++n, ++p[i]);

            if (p[i] + i > r) {
                c = i;
                r = p[i];
            }
        }

        int len = 0;
        c = 0;

        for (int i = 1; i < s2.length; ++i) {
            if (len < p[i]) {
                c = i;
                len = p[i];
            }
        }

        char [] res = new char[len];
        int index = 0;

        for (int i = c - len + 1, k = c + len + 1; i < k; i += 2) {
            res[index++] = s2[i];
        }

        return String.valueOf(res);
    }

    public static void main(String [] args) {
    }
}
