#include <iostream>
using namespace std;

class Solution {
    public:
        string longestPalindrome(string s) {
            size_t p[2001];
            char s2[2001];
            size_t s2_len = s.size()*2 + 1;

            for (size_t i = 0; i < s2_len - 1; i += 2) {
                s2[i] = '|';
                s2[i+1] = s[i/2];
            }

            s2[s2_len - 1] = '|';

            size_t c = 0, r = 0;
            size_t m = 0, n = 0;

            for (size_t i = 1; i < s2_len; ++i) {
                if (i > r) {
                    p[i] = 0; m = i - 1; n = i + 1;
                }
                else {
                    size_t i2 = c*2 - i;
                    if (p[i2] < r - i) {
                        p[i] = p[i2];
                        m = -1;
                    }
                    else {
                        p[i] = r - i;
                        n = r + 1; m = i*2 - n;
                    }
                }

                for (; m >=0 && n < s2_len && s2[m] == s2[n]; --m, ++n, ++p[i]);

                if (p[i] + i > r) {
                    c = i; r = p[i] + i;
                }
            }

            size_t len = 0;
            c = 0;
            for (size_t i = 1; i < s2_len; ++i) {
                if (len < p[i]) {
                    c = i;
                    len = p[i];
                }
            }

            string res;
            res.reserve(len);
            for (size_t i = c - len + 1, n = c + len + 1; i < n; i += 2) {
                res.push_back(s2[i]);
            }

            return res;
        }
};

void test(string s, string anwser) {
    string result = Solution().longestPalindrome(s);

    if (anwser != result)
        cout << "FAILED: Input: " << s << ", Expected: " << anwser << ", Result: " << result << endl;
    else
        cout << "PASSED: Input: " << s << ", Result: " << result << endl;
}

int main() {
    Solution s;
    test("ccccbbbaabbbcbba", "cbbbaabbbc");
    test("cccc", "cccc");
    test("aaabaaaa", "aaabaaa");
    return 0;
}
