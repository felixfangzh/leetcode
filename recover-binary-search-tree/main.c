#define NULL 0

/**
 * Definition for binary tree
 */
struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

void recoverTree(struct TreeNode *root) {
    if (!root) return;

    struct TreeNode *toRecover1 = NULL, *toRecover2 = NULL, *prev = NULL;

    for (; root; root = root->right) {
        for (; root->left; root = root->left) {
            struct TreeNode* leftRight = root->left;
            for (; leftRight->right && leftRight->right != root; leftRight = leftRight->right);
            if (!leftRight->right)
                leftRight->right = root;
            else if (leftRight->right == root) {
                leftRight->right = NULL;
                break;
            }
        }

        if (prev && prev->val > root->val) {
            toRecover2 = root;
            if (!toRecover1)
                toRecover1 = prev;
        }

        prev = root;
    }

    if (!toRecover1 || !toRecover2) return;

    int tmp = toRecover1->val;
    toRecover1->val = toRecover2->val;
    toRecover2->val = tmp;
}

int main() {
    struct TreeNode root = {0}, left = {0};
    root.val = 0;
    left.val = 1;
    root.left = &left;
    recoverTree(&root);
    return 0;
}
