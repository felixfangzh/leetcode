#include <algorithm>
using namespace std;

struct TreeNode {
   int val;
   TreeNode *left;
   TreeNode *right;
   TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
   private:
      void inorder(TreeNode *root, TreeNode *&n1, TreeNode *&n2, TreeNode *&prev) {
         if (!root)
            return;
         inorder(root->left, n1, n2, prev);
         if (prev && prev->val > root->val) {
            n2 = root;
            if (!n1)
               n1 = prev;
         }
         prev = root;
         inorder(root->right, n1, n2, prev);
      }
   public:
      void recoverTree(TreeNode *root) {
         TreeNode *n1, *n2, *prev;
         n1 = n2 = prev = NULL;
         inorder(root, n1, n2, prev);
         if (n1 && n2)
            swap(n1->val, n2->val);
      }
};

int main() {
   return 0;
}
