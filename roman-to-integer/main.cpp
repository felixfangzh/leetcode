#include <iostream>
#include <unordered_map>
using namespace std;

class Solution {
   private:
      unordered_map<char, int> conversion;
   public:
      Solution() {
         conversion['i'] = conversion['I'] = 1;
         conversion['v'] = conversion['V'] = 5;
         conversion['x'] = conversion['X'] = 10;
         conversion['l'] = conversion['L'] = 50;
         conversion['c'] = conversion['C'] = 100;
         conversion['d'] = conversion['D'] = 500;
         conversion['m'] = conversion['M'] = 1000;
      }
      int romanToInt(string s) {
         int val = 0;
         for (int i = 0, size = s.size(); i < size; ++i) {
            if (i < size - 1) {
               switch (s[i]) {
                  case 'i':
                  case 'I':
                     if (s[i+1] == 'v' || s[i+1] == 'V') 
                     {
                        val += 4;
                        ++i;
                     }
                     else if (s[i+1] == 'x' || s[i+1] == 'X') 
                     {
                        val += 9;
                        ++i;
                     }
                     else
                        val += conversion[s[i]];
                     continue;
                  case 'x':
                  case 'X':
                     if (s[i+1] == 'l' || s[i+1] == 'L') 
                     {
                        val += 40;
                        ++i;
                     }
                     else if (s[i+1] == 'c' || s[i+1] == 'C') 
                     {
                        val += 90;
                        ++i;
                     }
                     else
                        val += conversion[s[i]];
                     continue;
                  case 'c':
                  case 'C':
                     if (s[i+1] == 'd' || s[i+1] == 'D') 
                     {
                        val += 400;
                        ++i;
                     }
                     else if (s[i+1] == 'm' || s[i+1] == 'M') 
                     {
                        val += 900;
                        ++i;
                     }
                     else
                        val += conversion[s[i]];
                     continue;
               }
            }
            val += conversion[s[i]];
         }
         return val;
      }
};

int main() {
   Solution s;
   cout << s.romanToInt("MCMLIV") << endl;
   return 0;
}
