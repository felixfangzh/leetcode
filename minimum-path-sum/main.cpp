#include <iostream>
#include <vector>
#include <limits.h>

using namespace std;

class Solution {
   public:
      int minPathSum(vector<vector<int> > &grid) {
         vector<vector<int> > sums(grid);

         for(ssize_t n = grid.size()-1, i = n; i >=0; --i)
            for(ssize_t m = grid[0].size()-1, j = m; j >=0; --j) {
               int downSum = i + 1 > n ? INT_MAX : sums[i+1][j];
               int rightSum = j + 1 > m ? INT_MAX : sums[i][j+1];

               if (downSum == INT_MAX && rightSum == INT_MAX)
                  continue;

               sums[i][j] += min(downSum, rightSum);
            }

         return sums[0][0];
      }
};

int main() {
   return 0;
}
