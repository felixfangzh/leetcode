#include <iostream>
#include <queue>
#include <vector>
using namespace std;

struct TreeNode {
   int val;
   TreeNode *left;
   TreeNode *right;
   TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

bool isLeaveNode(TreeNode* root) {
   return root && (!root->left && !root->right);
}

void printTreeNode(TreeNode* root) {
   if (!root) {
      return;
   }
   cout << root->val << " ";
   if (isLeaveNode(root))
      return;
   queue<TreeNode*> q;
   q.push(root);
   while (!q.empty()) {
      TreeNode* node = q.front();
      q.pop();
      if (node->left) {
         cout << node->left->val << " ";
         if (!isLeaveNode(node->left))
            q.push(node->left);
      }
      else {
         cout << "# ";
      }
      if (node->right) {
         cout << node->right->val << " ";
         if (!isLeaveNode(node->right))
            q.push(node->right);
      }
      else {
         cout << "# ";
      }
   }
}

class Solution {
   private:
      vector<TreeNode *> _generateTrees(int s, int n) {
         vector<TreeNode* > trees;
         int k = n - s;
         if (k < 0) {
            trees.push_back(NULL);
            return trees;
         }
         if (k == 0) {
            trees.push_back(new TreeNode(n));
            return trees;
         }
         for (int i = s; i <= n; ++i) {
            vector<TreeNode* > leftTrees = _generateTrees(s, i-1);
            vector<TreeNode* > rightTrees = _generateTrees(i+1, n);
            for (auto &l : leftTrees) {
               for (auto &r : rightTrees) {
                  TreeNode* root = new TreeNode(i);
                  root->left = l;
                  root->right = r;
                  trees.push_back(root);
               }
            }
         }
         return trees;
      }
   public:
      vector<TreeNode *> generateTrees(int n) {
         return _generateTrees(1, n);
      }
};

int main() {
   Solution s;
   for (auto &n : s.generateTrees(3)) {
      printTreeNode(n);
      cout << endl;
   }
   return 0;
}
