import java.util.*;

public class Solution {
    /**
     * Definition for binary tree
     */
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) { val = x; left = null; right = null; }
    }

    private List<TreeNode> generateTrees(int start, int end) {
        List<TreeNode> nodes = new ArrayList<TreeNode>();
        if (start > end) {
            nodes.add(null);
            return nodes;
        }

        for (int i = start; i <= end; ++i) {
            for (TreeNode left : generateTrees(start, i-1)) {
                for (TreeNode right : generateTrees(i+1, end)) {
                    TreeNode node = new TreeNode(i);
                    node.left = left;
                    node.right = right;
                    nodes.add(node);
                }
            }
        }

        return nodes;
    }

    public List<TreeNode> generateTrees(int n) {
        return generateTrees(1, n);
    }
}
