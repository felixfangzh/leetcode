import java.util.*;

class UndirectedGraphNode {
   int label;
   ArrayList<UndirectedGraphNode> neighbors;
   UndirectedGraphNode(int x) { label = x; neighbors = new ArrayList<UndirectedGraphNode>(); }
};

public class Solution {
   public UndirectedGraphNode cloneGraph(UndirectedGraphNode node) {
      if (null == node)
         return node;
      Map<UndirectedGraphNode, UndirectedGraphNode> node_map = new HashMap<UndirectedGraphNode, UndirectedGraphNode>();
      Deque<UndirectedGraphNode> stack = new ArrayDeque<UndirectedGraphNode>();
      UndirectedGraphNode newNode = new UndirectedGraphNode(node.label);
      node_map.put(node, newNode);
      stack.push(node);
      while (!stack.isEmpty()) {
         UndirectedGraphNode n = stack.pop();
         UndirectedGraphNode _n = node_map.get(n);
         for (UndirectedGraphNode neighbor : n.neighbors) {
            UndirectedGraphNode _neighbor = node_map.get(neighbor);
            if (null == _neighbor) {
               _neighbor = new UndirectedGraphNode(neighbor.label);
               node_map.put(neighbor, _neighbor);
               stack.push(neighbor);
            }
            _n.neighbors.add(_neighbor);
         }
      }
      return node_map.get(node);
   }
}
