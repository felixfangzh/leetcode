#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

struct UndirectedGraphNode {
   int label;
   vector<UndirectedGraphNode *> neighbors;
   UndirectedGraphNode(int x) : label(x) {};
};

class Solution {
   private:
      unordered_map<int, UndirectedGraphNode*> node_map;
      UndirectedGraphNode *_cloneGraph(UndirectedGraphNode *node) {
         if (!node)
            return node;
         auto iter = node_map.find(node->label);
         UndirectedGraphNode *root;
         if (iter == node_map.end()) {
            root = new UndirectedGraphNode(node->label);
            node_map[node->label] = root;
            for (auto &n : node->neighbors) {
               auto _iter = node_map.find(n->label);
               root->neighbors.push_back(_iter == node_map.end() ? _cloneGraph(n) : _iter->second);
            }
         }
         else {
            root = iter->second;
         }

         return root;
      }
   public:
      UndirectedGraphNode *cloneGraph(UndirectedGraphNode *node) {
         node_map.clear();
         return _cloneGraph(node);
      }
};

int main() {
   Solution s;
   return 0;
}
