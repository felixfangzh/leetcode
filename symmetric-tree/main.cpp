#include <iostream>
using namespace std;

struct TreeNode {
   int val;
   TreeNode *left;
   TreeNode *right;
   TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
   private:
      bool _isSymmetric(TreeNode *l, TreeNode *r) {
         return (!l && !r) || (l && r && l->val == r->val && _isSymmetric(l->left, r->right) && _isSymmetric(l->right, r->left));
      }
   public:
      bool isSymmetric(TreeNode *root) {
         return root ? _isSymmetric(root->left, root->right) : true;
      }
};

int main() {
   return 0;
}
