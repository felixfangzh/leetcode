class Solution:
    # @return a list of lists of integers
    def combine(self, n, k):
        lists = []
        if n <= 0 or k <= 0:
            return lists
        self.__combine(1, n, k, [], lists)
        return lists

    def __combine(self, start, n, k, l, lists):
        length = len(l)
        if length > k:
            return
        if length == k:
            lists.append(list(l))

        for i in range(start, n + 1):
            l.append(i)
            self.__combine(i + 1, n, k, l, lists)
            del l[-1]


if __name__ == "__main__":
    s = Solution()
    print s.combine(10, 7)
