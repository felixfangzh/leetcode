import java.util.*;

public class Solution {
    public void combine(int start, int n, int k, List<Integer> list, List<List<Integer>> lists) {
        if (list.size() > k) return;

        if (list.size() == k) {
            lists.add(new ArrayList<Integer>(list));
            return;
        }

        for (int i = start; i <= n; ++i) {
            list.add(i);
            combine(i + 1, n, k, list, lists);
            list.remove(list.size()-1);
        }
    }

    public List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> lists = new ArrayList<List<Integer>>();
        if (n <= 0 || k <= 0) return lists;
        combine(1, n, k, new ArrayList(), lists);
        return lists;
    }

    public static void test(int n, int k) {
        for (List<Integer> l : new Solution().combine(n, k)) {
            for (Integer i : l)
                System.out.print(i + " ");
            System.out.println("\b");
        }
    }

    public static void main(String [] args) {
        test(10, 7);
    }
}
