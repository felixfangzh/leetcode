#include <iostream>
#include <vector>
using namespace std;

class Solution {
   private:
      void _combine(int n, int k, int l, vector<int> &comb, vector<vector<int> > &res) {
           if (comb.size() == k) {
              res.push_back(comb);
              return;
           }

           for (int i = l; i <= n; ++i) {
              comb.push_back(i);
              _combine(n, k, i+1, comb, res);
              comb.pop_back();
           }

      }
   public:
      vector<vector<int> > combine(int n, int k) {
         vector<vector<int> > res;
         vector<int> comb;
         _combine(n, k, 1, comb, res);
         return res;
      }
};

int main() {
   Solution s;
   for (auto &v : s.combine(1, 1)) {
      for (auto i : v)
         cout << i << " ";
      cout << endl;
   }

   return 0;
}
