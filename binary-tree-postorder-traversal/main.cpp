#include <vector>
#include <stack>

using namespace std;
#define NULL 0
/**
 * Definition for binary tree
 */
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
    public:
        vector<int> postorderTraversal(TreeNode *root) {
            vector<int> res;
            if (!root)  return res;

            stack<TreeNode*> nodes;
            nodes.push(root);
            TreeNode* prev = NULL;

            while (!nodes.empty()) {
                TreeNode* cur = nodes.top();

                if (!prev || prev->left == cur || prev->right == cur) {
                    if (cur->left)
                        nodes.push(cur->left);
                    else if (cur->right)
                        nodes.push(cur->right);
                    else {
                        res.push_back(cur->val);
                        nodes.pop();
                    }
                }
                else if (prev == cur->left) {
                    if (cur->right)
                        nodes.push(cur->right);
                    else {
                        res.push_back(cur->val);
                        nodes.pop();
                    }
                }
                else if (prev == cur->right) {
                    res.push_back(cur->val);
                    nodes.pop();
                }

                prev = cur;
            }

            return res;
        }
};

int main () {
    Solution s;
    return 0;
}
