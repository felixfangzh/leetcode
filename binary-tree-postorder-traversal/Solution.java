import java.util.*;

public class Solution {
    /**
     * Definition for binary tree
     */
    static public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) { val = x; }
    }

    public List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<Integer>();
        if (null == root)
            return list;
        Deque<TreeNode> stack = new ArrayDeque<TreeNode>();
        TreeNode prev = null;

        stack.push(root);

        while (!stack.isEmpty()) {
            TreeNode current = stack.peek();

            if (prev == null || prev.left == current || prev.right == current) {
                if (current.left != null)
                    stack.push(current.left);
                else if (current.right != null)
                    stack.push(current.right);
                else {
                    list.add(current.val);
                    stack.pop();
                }
            }
            else if (prev == current.left) {
                if (current.right != null)
                    stack.push(current.right);
                else {
                    list.add(current.val);
                    stack.pop();
                }
            }
            else if (prev == current.right) {
                list.add(current.val);
                stack.pop();
            }

            prev = current;
        }

        return list;
    }

    public static void main(String [] args) {
    }
}
