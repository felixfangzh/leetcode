#include <iostream>
using namespace std;

class Solution {
   public:
      bool isMatch(const char *s, const char *p) {
         if (!s || !p) return false;
         if (!p[0]) return !s[0];
         if (p[1] != '*')
            return (s[0] == p[0] || (p[0] == '.' && s[0])) && isMatch(s+1, p+1);
         for (; s[0] == p[0] || (p[0] == '.' && s[0]); ++s) {
            if (isMatch(s, p+2)) return true;
         }
         return isMatch(s, p+2);
      }
};

int main() {
   Solution s;
   cout << s.isMatch("aa","a") <<       ", expected false" << endl;
   cout << s.isMatch("aa","aa") <<      ", expected true" << endl;
   cout << s.isMatch("aaa","aa") <<     ", expected false" << endl;
   cout << s.isMatch("aa", "a*") <<     ", expected true" << endl;
   cout << s.isMatch("aa", ".*") <<     ", expected true" << endl;
   cout << s.isMatch("ab", ".*") <<     ", expected true" << endl;
   cout << s.isMatch("aab", "c*a*b") << ", expected true" << endl;
   cout << s.isMatch("aaaaaaaaaaaaab", "a*a*a*a*a*a*a*a*a*a*c") << ", expected false" << endl;
   return 0;
}
