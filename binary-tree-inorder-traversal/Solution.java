import java.util.*;

public class Solution {
   /**
    * Definition for binary tree
    */
   public class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode(int x) { val = x; }
   }

   public List<Integer> inorderTraversal(TreeNode root) {
      List<Integer> list = new ArrayList<Integer>();

      if (null == root) return list;

      for (; null != root; root = root.right) {
         for (; null != root.left; root = root.left) {
            TreeNode leftRight = root.left;
            for(; null != leftRight.right && root != leftRight.right; leftRight = leftRight.right); 

            if (root == leftRight.right) {
               leftRight.right = null;
               break;
            }

            if (null == leftRight.right)
               leftRight.right = root;
         }
         list.add(root.val);
      }

      return list;
   }
}
