#include <iostream>
#include <vector>
using namespace std;

class Solution {
   private:
      vector<int> steps;
      int _climbStairs(int n) {
         if (n <= 2)
            return n;
         if (!steps[n-1])
            steps[n-1] = _climbStairs(n - 1);
         if (!steps[n-2])
            steps[n-2] = _climbStairs(n - 2);

         return steps[n-1] + steps[n-2];
      }
   public:
      int climbStairs(int n) {
         if (n > steps.size())
            steps.resize(n);
         return _climbStairs(n);
      }
};

int main() {
   Solution s;
   cout << s.climbStairs(2) << endl;
   cout << s.climbStairs(3) << endl;
   cout << s.climbStairs(4) << endl;
   return 0;
}
