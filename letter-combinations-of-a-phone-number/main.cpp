#include <iostream>
#include <vector>
using namespace std;

template <typename T>
void printVector(vector<T> &v) {
   for (T &i : v) 
      cout << i << " ";
   cout << endl;
}

#define PLACEHOLDER "~"

class Solution {
   private:
      string letters [8][4] = {
         {"a", "b", "c", PLACEHOLDER},
         {"d", "e", "f", PLACEHOLDER},
         {"g", "h", "i", PLACEHOLDER},
         {"j", "k", "l", PLACEHOLDER},
         {"m", "n", "o", PLACEHOLDER},
         {"p", "q", "r", "s"},
         {"t", "u", "v", PLACEHOLDER},
         {"w", "x", "y", "z"},
      };
   public:
      vector<string> letterCombinations(string digits) {
         vector<string> combs;
         combs.push_back("");
         for (int k = 0, size = digits.size(); k < size; ++k) {
            int d = digits[k] - '0' - 2;
            if (d < 0)
               continue;

            vector<string> _combs;
            for (auto &s : combs) {
               for (int i = 0; i < 4; ++i) {
                  if (letters[d][i] != PLACEHOLDER) {
                     string s2(s);
                     s2.push_back(letters[d][i][0]);   
                     _combs.push_back(s2);
                  }
               }
            }
            combs.swap(_combs);
         }
         return combs;
      }
};

int main() {
   Solution s;
   vector<string> v = s.letterCombinations("23");
   printVector(v);
   v = s.letterCombinations("");
   printVector(v);
   v = s.letterCombinations("22");
   printVector(v);
   return 0;
}
