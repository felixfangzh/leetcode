#include <iostream>
#include <vector>
using namespace std;

class Solution {
   public:
      string multiply(string num1, string num2) {
         int n1(num1.size()), n2(num2.size()), n(n1 + n2), msb(n-1);
         string ret(n , '0');
         vector<int> num(n, 0);
         for (int i = n-1, carry = 0; i >= 0; --i) {
            for (int j = max(0, i - n2); j < n1 && i - j -1 >=0; ++j)
               num[i] += (num1[j] - '0') * (num2[i - j - 1] - '0');
            num[i] += carry;
            carry = num[i] / 10;
            if ((ret[i] = (num[i]%10) + '0') != '0') msb = i; 
         }

         return string(ret.begin() + msb, ret.end());
      }
};

int main() {
   Solution s;
   cout << s.multiply("120", "12") << endl;
}
