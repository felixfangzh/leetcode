#include <iostream>
#include <algorithm>
#include <climits>
using namespace std;

class Solution {
   private:
      int threeSum(vector<int> &num, int i, int j, int k) {
         return num[i] + num[j] + num[k];
      }
   public:
      int threeSumClosest(vector<int> &num, int target) {
         if (num.size() < 3) {
            return 0;
         }
         sort(num.begin(), num.end());
         int sum =  threeSum(num, 0, 1, 2);
         for (int i = 0, size = num.size() - 2; i < size; ++i) {
            if (i > 0 && num[i] == num[i-1])
               continue;
            int s = i + 1;
            int e = num.size()-1;
            while (s < e) {
               int _sum = threeSum(num, i, s, e);
               if (abs(_sum - target) < abs(sum - target))
                  sum = _sum;
               if (_sum == target)
                  return target;
               if (_sum < target)
                  ++s;
               else
                  --e;
            }
         }
         return sum;
      }
};

int main() {
   Solution s;
   int vals [] = {0,5,-1,-2,4,-1,0,-3,4,-5};
   vector<int> num(begin(vals), end(vals));
   cout << s.threeSumClosest(num, 1) << endl;
   return 0;
}
