import java.util.*;

public class Solution {
    public int threeSumClosest(int[] num, int target) {
        Arrays.sort(num);
        
        int sum = Integer.MAX_VALUE >> 1;
        
        for (int i = 0; i < num.length - 2; ++i) {
            if (i > 0 && num[i] == num[i-1]) continue;
            
            int j = i + 1;
            int k = num.length -1;
                
            while (j < k) {
                if (j > i + 1 && num[j] == num[j-1]) {
                    ++j;
                    continue;
                }

                if (k < num.length - 1 && num[k] == num[k+1]) {
                    --k;
                    continue;
                }
                    
                int newSum = num[i] + num[j] + num[k];
                if (Math.abs(target - newSum) < Math.abs(target - sum)) {
                    sum = newSum;
                }
                
                if (newSum > target) {
                    --k;
                }
                else {
                    ++j;
                }
            }
        }
        
        return sum;
    }

    public static void test(int [] num, int target) {
        System.out.println(new Solution().threeSumClosest(num, target));
    }

    public static void main(String [] args) {
        test(new int [] {1, 1, 1, 1}, 0);
    }
}
