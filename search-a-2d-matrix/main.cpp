#include <iostream>
#include <vector>
using namespace std;

class Solution {
   public:
      bool searchMatrix(vector<vector<int> > &matrix, int target) {
         if (matrix.empty())
            return false;
         int n(matrix.size()), m(matrix[0].size()), i(0), j(n*m);
         while (i < j) {
            int mid((i + j)/2), p(mid/m), q(mid-p*m);
            if (matrix[p][q] == target)
               return true;
            else if (matrix[p][q] < target)
               i = mid + 1;
            else
               j = mid;
         }
         return false;
      }
};

int main() {
   Solution s;
   int vals [][2] = 
   {{-10,-10},{-9,-9},{-8,-6},{-4,-2},{0,1},{3,3},{5,5},{6,8}};
#if 0
   {
      {1,   3,  5,  7},
      {10, 11, 16, 20},
      {23, 30, 34, 50}
   };
#endif
   vector<vector<int> > matrix;
   for (auto val : vals) {
      vector<int> row;
      for (int i = 0; i < 2; ++i)
         row.push_back(val[i]);
      matrix.push_back(row);
   }
   for (auto &row : matrix) {
      for (auto i : row)
         cout << i << " ";
      cout << endl;
   }
   cout << s.searchMatrix(matrix, 0) << endl;
   return 0;
}
