public class Solution {
    public boolean isPalindrome(int x) {
        if (x < 0) return false;
        
        int digits = findDigits(x);
        if (digits <= 1) return true;
        int tens = (int)power10(digits - 1);
        
        for (int i = 0; i < digits/2; ++i) {
            int firstDigit = x / tens;
            if (firstDigit != x % 10) return false;
            x -= firstDigit*tens;
            tens /= 100;
            x /= 10;
        }
        return true;
    }
    public static int findDigits(int x) {
        if (x < 10) return 1;
        if (x < 100) return 2;
        if (x < 1000) return 3;
        if (x < 10000) return 4;
        if (x < 100000) return 5;
        if (x < 1000000) return 6;
        if (x < 10000000) return 7;
        if (x < 100000000) return 8;
        if (x < 1000000000) return 9;
        return findDigits(x / 1000000000) + 9;
    }
    public static double power10(int x) {
        if (x >= 0) {
            switch (x) {
                case 0: return 1;
                case 1: return 10;
                case 2: return 100;
                case 3: return 1000;
                case 4: return 10000;
                case 5: return 100000;
                case 6: return 1000000;
                case 7: return 10000000;
                case 8: return 100000000;
                case 9: return 1000000000;
                default: return power10(x - 9)*1000000000;
            }
        }
        else {
            switch (x) {
                case -1: return .1;
                case -2: return .01;
                case -3: return .001;
                case -4: return .0001;
                case -5: return .00001;
                case -6: return .000001;
                case -7: return .0000001;
                case -8: return .00000001;
                case -9: return .000000001;
                default: return power10(x + 9)*.000000001;
            }
        }
    }

   public static void main(String [] args) {
      Solution sol = new Solution();
      System.out.println("Expected: true, Actual: " + sol.isPalindrome(9999));
   }
}
