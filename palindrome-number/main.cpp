#include <iostream>
#include <cmath>
using namespace std;

class Solution {
   public:
      bool isPalindrome(int x) {
         if (x < 0)
            return false;
         if (x < 10)
            return true;
         int z, y;
         for (z = y = 0; y < x ; ++z, y = 10*y + x%10, x/=10);
         return (x == y || (x == y/10 && x > 0)) && (y >= pow(10, z-1));
      }
};

int main() {
   Solution s;
   cout << s.isPalindrome(12321) << ", expected true" << endl;
   cout << s.isPalindrome(123321) << ", expected true" << endl;
   cout << s.isPalindrome(12331) << ", expected false" << endl;
   cout << s.isPalindrome(1) << ", expected true" << endl;
   cout << s.isPalindrome(-1) << ", expected false" << endl;
   cout << s.isPalindrome(10) << ", expected false" << endl;
   cout << s.isPalindrome(131000) << ", expected false" << endl;
   return 0;
}
