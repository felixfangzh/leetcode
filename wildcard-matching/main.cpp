#include <iostream>
#include <vector>
#include <cstring>
using namespace std;

class Solution {
   public:
      bool isMatch(const char *s, const char *p) {
         int n(strlen(s)), m(strlen(p)), i, j, chars;
         for (i = 0, chars = 0; p[i]; ++i) 
            if (p[i] != '*' && n < ++chars) return false;
         vector<bool> dp(n+2, false);
         for (i = m-1, dp[n] = true; i >= 0; --i) {
            if (p[i] == '*') {
               for (; i>0 && p[i-1] == '*'; --i);
               for (j = n; j >=0 && !dp[j]; --j);
               for (; j >=0; --j) dp[j] = true;
            }
            else {
               for (j = 0; j < n + 1; ++j)
                  dp[j] = (p[i] == s[j] || p[i] == '?') && dp[j+1];
            }
         }

         return dp[0];
      }
};

int main() {
   Solution s;
   cout << s.isMatch("hi","*?")      << ", expected true" << endl;
   cout << s.isMatch("aa","a")       << ", expected false" << endl;
   cout << s.isMatch("aa","aa")      << ", expected true" << endl;
   cout << s.isMatch("aaa","aa")     << ", expected false" << endl;
   cout << s.isMatch("aa", "*")      << ", expected true" << endl;
   cout << s.isMatch("aa", "a*")     << ", expected true" << endl;
   cout << s.isMatch("ab", "?*")     << ", expected true" << endl;
   cout << s.isMatch("aab", "c*a*b") << ", expected false" << endl;
   cout << s.isMatch("aaabbbaabaaaaababaabaaabbabbbbbbbbaabababbabbbaaaaba", "a*******b") << ", expected false" << endl;
   cout << s.isMatch("abbabaaabbabbaababbabbbbbabbbabbbabaaaaababababbbabababaabbababaabbbbbbaaaabababbbaabbbbaabbbbababababbaabbaababaabbbababababbbbaaabbbbbabaaaabbababbbbaababaabbababbbbbababbbabaaaaaaaabbbbbaabaaababaaaabb", "**aa*****ba*a*bb**aa*ab****a*aaaaaa***a*aaaa**bbabb*b*b**aaaaaaaaa*a********ba*bbb***a*ba*bb*bb**a*b*bb") << ", expected false" << endl;
   return 0;
}
