import java.util.*;

public class Solution {
    public String convertToTitle(int n) {
        if (n <= 0) return "";
        StringBuilder builder = new StringBuilder();
        Deque<Character> stack = new ArrayDeque<Character>();

        
        for (; n > 0; n /= 26) {
            stack.push((char)('A' + ((--n)%26)));
        }

        while (!stack.isEmpty())
            builder.append(stack.pop());

        return builder.toString();
    }

    public static void main(String [] args) {
        Solution sol =  new Solution();
        System.out.println(sol.convertToTitle(26));
    }
}
