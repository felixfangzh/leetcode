#include <iostream>
#include <stack>

using namespace std;

class Solution {
    public:
        string convertToTitle(int n) {
            if (n <= 0) return "";
            stack<char> s;
            string str;

            for (; n > 0; n /= 26)
                s.push('A' + (--n)%26);

            str.reserve(s.size());

            while (!s.empty()) {
                str.push_back(s.top());
                s.pop();
            }

            return str;
        }
};

int main() {
    Solution sol;
    return 0;
}
