#include <iostream>
#include <vector>
#include <sstream>
#include <cstdlib>
using namespace std;

class Solution {
   size_t upper[3] = {9, 99, 255};
   private:
      void restore(string &s, int start, vector<string> &partial, vector<string> &ret) {
         if (partial.size() == 4 && start == s.size()) {
            stringstream ip;
            for (size_t i = 0, size = partial.size(); i < size; ++i) {
               if (i != 0)
                  ip << ".";
               ip << partial[i];
            }
            ret.push_back(ip.str());
            return;
         }
         if (partial.size() == 4 || start == s.size()) {
            return;
         }
         for (int i = 1, k = s.size() - start; i <= 3; ++i) {
            if (k >= i) {
               if (i > 1 && s[start] == '0')
                  continue;
               string _s = s.substr(start, i);
               int j = atoi(_s.c_str());
               if (j >= 0 && j <= upper[i-1]) {
                  partial.push_back(_s);
                  restore(s, start+i, partial, ret);
                  partial.pop_back();
               }
            }
         }
      }
   public:
      vector<string> restoreIpAddresses(string s) {
         vector<string> partial;
         vector<string> ret;
         restore(s, 0, partial, ret);
         return ret;
      }
};

int main() {
   Solution s;
   string inputs [] = {
      "25525511135",
      "010010"
   };
   for (auto &i : inputs) {
      for (auto &_s : s.restoreIpAddresses(i))
         cout << _s << " ";
      cout << endl;
   }
   return 0;
}
