import java.util.*;

public class Solution {
    private void search(String s, List<List<Integer>> palindromes, int index, List<String> list, List<List<String>> res) {
        if (index == s.length()) {
            List<String> newList = new ArrayList<String>();
            newList.addAll(list);
            res.add(newList);
            return;
        }

        for (int i = 0; i < palindromes.get(index).size(); ++i) {
            list.add(s.substring(index, palindromes.get(index).get(i) + 1));
            search(s, palindromes, palindromes.get(index).get(i) + 1, list, res);
            list.remove(list.size()-1);
        }
    }

    public List<List<String>> partition(String s) {
        List<List<String>> res = new ArrayList<List<String>>();
        if (null == s || s.isEmpty()) return res;

        List<List<Integer>> palindromes = new ArrayList<List<Integer>>(s.length());
        for (int i = 0; i < s.length(); ++i)
            palindromes.add(new ArrayList<Integer>());
        boolean [][] mtx = new boolean[s.length()][s.length()];
        for (int i = 0; i < s.length(); ++i)
            mtx[i] = new boolean[s.length()];


        for (int len = 1; len <= s.length(); ++len) {
            for (int i = 0, n = s.length() - len; i <= n; ++i) {
                if (len == 1) {
                    palindromes.get(i).add(i);
                    mtx[i][i] = true;
                }
                else if (len == 2 && s.charAt(i) == s.charAt(i+1)) {
                    palindromes.get(i).add(i+1);
                    mtx[i][i+1] = true;
                }
                else if (s.charAt(i) == s.charAt(i + len - 1) && mtx[i+1][i+len-2]) {
                    palindromes.get(i).add(i+len-1);
                    mtx[i][i+len-1] = true;
                }
            }
        }

        search(s, palindromes, 0, new ArrayList<String>(), res);

        return res;
    }

    public static void test(String s) {
        for (List<String> l : new Solution().partition(s)) {
            for (String str : l)
                System.out.print(str + " ");
            System.out.println("\b");
        }
    }

    public static void main(String [] args) {
        test("aab");
    }
}
