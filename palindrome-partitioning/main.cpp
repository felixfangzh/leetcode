#include <iostream>
#include <vector>
using namespace std;

class Solution {
    private:
        void search(string &s, vector<vector<int>> &palindromes, size_t index, vector<string> &list, vector<vector<string>> &partitions) {
            if (index == s.size()) {
                partitions.push_back(list);
                return;
            }

            for (auto &p : palindromes[index]) {
                list.push_back(s.substr(index, p - index + 1));
                search(s, palindromes, p + 1, list, partitions);
                list.pop_back();
            }
        }
    public:
        vector<vector<string>> partition(string s) {
            vector<vector<string>> partitions;
            if (s.empty())
                return partitions;

            vector<vector<int>> palindromes (s.size(), vector<int>());
            vector<vector<bool>> mtx (s.size(), vector<bool>(s.size(), false));

            for (size_t len = 1; len <= s.size(); ++len)
                for (size_t i = 0, n = s.size() - len; i <= n; ++i) {
                    if (len == 1) {
                        palindromes[i].push_back(i);
                        mtx[i][i] = true;
                    }
                    else if (len == 2 && s[i] == s[i+1]) {
                        palindromes[i].push_back(i+1);
                        mtx[i][i+1] = true;
                    }
                    else if (s[i] == s[i + len - 1] && mtx[i+1][i + len - 2]) {
                        palindromes[i].push_back(i + len - 1);
                        mtx[i][i + len - 1] = true;
                    }
                }


            vector<string> list;
            search(s, palindromes, 0, list, partitions);

            return partitions;
        }
};

int main() {
    Solution s;
    for (auto &v : s.partition("cbbbcc")) {
        for (auto &s : v)
            cout << s << " ";
        cout << endl;
    }
    return 0;
}
