#include <iostream>
using namespace std;

struct TreeNode {
   int val;
   TreeNode *left;
   TreeNode *right;
   TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
   private:
      bool isValidLeftSubtree(TreeNode *current, TreeNode *root) {
         return !current || (current 
               && (current->val < root->val)
               && isValidLeftSubtree(current->left, root)
               && isValidLeftSubtree(current->right, root));
      }
      bool isValidRightSubtree(TreeNode *current, TreeNode *root) {
         return !current || (current 
               && (current->val > root->val)
               && isValidRightSubtree(current->left, root)
               && isValidRightSubtree(current->right, root));
      }
   public:
      bool isValidBST(TreeNode *root) {
         return !root || (root 
               && isValidLeftSubtree(root->left, root)
               && isValidRightSubtree(root->right, root)
               && isValidBST(root->left)
               && isValidBST(root->right));
      }
};

int main() {
return 0;
}
