#include <iostream>
#include <vector>
#include <string.h>

using namespace std;

class Solution {
    public:
        int lengthOfLongestSubstring(string s) {
            int chars[129] = {0};

            int result = 0, count = 0;

            const char* str = s.c_str();
            for (int prev = 0, i = 0, n = s.size(); i < n; ++i) {
                if (prev < chars[str[i]]) {
                    if (result < count) result = count;

                    count = i - chars[str[i]];
                    prev = chars[str[i]];
                }

                ++count;
                chars[str[i]] = i + 1;
            }

            return max(result, count);
        }
};

void test(string s, int answer) {
    int result = Solution().lengthOfLongestSubstring(s);
    if (result != answer)
        cout << "FAILED: Input: " << s << ", Expected: " << answer << ", Actual: " << result << endl;
    else
        cout << "PASSED: Input: " << s << ", Result: " << result << endl;
}

int main() {
    test("abcabcbb", 3);
    test("bbbbb", 1);
    test("dvdf", 3);
    test("aab", 2);
    test("abba", 2);
    test("tmmzuxt", 5);
    test("wobgrovw", 6);
    return 0;
}
