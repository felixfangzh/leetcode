public class Solution {
    public int lengthOfLongestSubstring(String s) {
        final int charNum = 129;
        int [] chars = new int [charNum];
        for (int i = 0; i < charNum; ++i)
            chars[i] = 0;

        int result = 0, count = 0;

        char [] charArray = s.toCharArray();
        for (int i = 0, prev = 0; i < charArray.length; ++i) {
            if (prev < chars[charArray[i]]) {
                if (result < count) result = count;

                count = i - chars[charArray[i]];
                prev = chars[charArray[i]];
            }

            ++count;
            chars[charArray[i]] = i + 1;
        }

        return result > count ? result : count;
    }

    public static void main(String [] args) {

    }
}
