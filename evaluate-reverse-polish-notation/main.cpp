#include <iostream>
#include <queue>
#include <vector>
#include <stack>
using namespace std;

class Solution {
   private:
      bool isOperator(string t) {
         return t.back() == '+' || t.back() == '-' || t.back() == '*' || t.back() == '/'; 
      }
      int eval(int i, int j, string op) {
         switch (op[0]) {
            case '+':
               return i + j;
            case '-':
               return i - j;
            case '*':
               return i * j;
            case '/':
               return i / j;
         }
         return 0;
      }
   public:
      int evalRPN(vector<string> &tokens) {
         stack<int> vals;
         queue<string> q;
         for (auto &t : tokens)
            q.push(t);
         while (!q.empty()) {
            string s = q.front(); q.pop();
            if (!isOperator(s))
               vals.push(atoi(s.c_str()));
            else {
               int j = vals.top(); vals.pop();
               int i = vals.top(); vals.pop();
               vals.push(eval(i, j, s));
            }
         }

         return vals.top();
      }
};

int main() {
   Solution s;
   //string ops[] = {"2", "1", "+", "3", "*"};
   //string ops[] = {"4", "13", "5", "/", "+"};
   string ops[] = {"-1","1","*","-1","+"};
   vector<string> tokens(begin(ops), end(ops));
   cout << s.evalRPN(tokens) << endl;
   return 0;
}
