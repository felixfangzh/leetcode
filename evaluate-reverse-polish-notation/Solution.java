import java.util.*;

public class Solution {
    public int evalRPN(String[] tokens) {
        Deque<Integer> stack = new ArrayDeque<Integer>();

        for (int i = 0; i < tokens.length; ++i) {
            if (tokens[i].equals("+"))
            {
                stack.push(stack.pop() + stack.pop());
            }
            else if (tokens[i].equals("-"))
            {
                Integer i2 = stack.pop();
                stack.push(stack.pop() - i2);
            }
            else if (tokens[i].equals("*"))
            {
                stack.push(stack.pop() * stack.pop());
            }
            else if (tokens[i].equals("/"))
            {
                Integer i2 = stack.pop();
                stack.push(stack.pop() / i2);
            }
            else {
                stack.push(Integer.parseInt(tokens[i]));
            }
        }

        return stack.pop();
    }
}
