#include <vector>
using namespace std;

class Solution {
   public:
      int minimumTotal(vector<vector<int> > &triangle) {
         int n = triangle.size();
         vector<int> sums(n+1, 0);
         while (n-- > 0) {
            for (int i = 0; i < triangle[n].size(); ++i) {
               sums[i] = triangle[n][i] + min(sums[i], sums[i+1]);
            }
         }
         return sums[0];
      }
};

int main () {
   return 0;
}
