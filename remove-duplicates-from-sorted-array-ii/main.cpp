#include <iostream>
using namespace std;

class Solution {
   public:
      int removeDuplicates(int A[], int n) {
         if (n <= 2) return n;
         size_t i(1), j(0), count(0);
         for (; i < n; ++i) {
            if (A[i] == A[i-1])
               ++count;
            else {
               count = 0;
            }
            if (count < 2)
               ++j;
            A[j] = A[i];
         }
         return j+1;
      }
};

int main() {
   Solution s;
   int input [] = {1,1,1,1,1,2,2,2,2,2,3};
   size_t n = s.removeDuplicates(input, distance(begin(input), end(input)));
   cout << n << ", expect 5" << endl;
   for (size_t i = 0; i < n; ++i)
      cout << input[i] << " ";
   cout << endl;
   return 0;
}
