public class Solution {
   public int removeDuplicates(int[] A) {
      int length = A.length;
      for (int i = 0, j = 1, count = 0; j < A.length; ++j) {
         A[i+1] = A[j];
         if (A[i] != A[++i]) {
            count = 0;
         }
         else if (++count >= 2) {
            --i; 
            --length;
         }
      }

      return length;
   }

   public static void test(int[] A) {
      Solution s = new Solution();
      for (int i = 0, n = s.removeDuplicates(A); i < n; ++i) {
         System.out.print(A[i] + " ");
      }
      System.out.println("\b");
   }

   public static void main(String [] args) {
      test(new int[] {1, 1, 1});
      test(new int[] {1, 1, 1, 2, 2, 3});
   }
}
