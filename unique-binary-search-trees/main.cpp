#include <iostream>
#include <vector>
using namespace std;

class Solution {
   public:
      int _numTrees(int n, vector<int> &nums) {
         if (n <= 1)
            return 1;
         int count = 0;
         for (int i = 0; i < n; ++i) {
            int left = i - 0, right = n - i - 1;
            if (!nums[left])
               nums[left] = _numTrees(left, nums);
            if (!nums[right])
               nums[right] = _numTrees(right, nums);
            count += nums[left] * nums[right];
         }

         return count;
      }
      int numTrees(int n) {
         if (n <= 0)
            return 0;
         vector<int> nums(n, 0);
         return _numTrees(n, nums);
      }
};

int main() {
   Solution s;
   cout << s.numTrees(3) << endl;
   return 0;
}
