public class Solution {
    private int [] values;

    private int _numTrees(int n) {
        if (n <= 0) return 0;
        if (values[n] != 0) return values[n];
        if (n == 1) return (values[n] = 1);

        int num = 0;

        for (int i = 0; i < n; ++i) {
            if (i == 0 || i == n - 1) {
                num += _numTrees(n - 1);
                continue;
            }
            num += _numTrees(i) * _numTrees(n - i - 1);
        }

        return (values[n] = num);
    }

    public int numTrees(int n) {
        values = new int[n+1];
        return _numTrees(n);
    }

    public static void main(String [] args) {
        Solution sol = new Solution();
        System.out.println(sol.numTrees(3));
    }
}
