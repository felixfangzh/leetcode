void reverse(int nums[], int start, int end) {
    int i, n, l;
    for (i = start, n = start + (end - start)/2; i < n; ++i) {
        l = nums[i];
        nums[i] = nums[end - 1 - i + start];
        nums[end - 1 - i + start] = l;
    }
}

void rotate(int nums[], int n, int k) {
   k %= n;
   if (k <= 0) return;
   reverse(nums, 0, n);
   reverse(nums, 0, k);
   reverse(nums, k, n);
}

int main() {
   return 0;
}
