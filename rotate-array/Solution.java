public class Solution {
    private void reverse(int [] nums, int start, int end) {
        for (int i = start, n = (end - start)/2 + start; i < n; ++i) {
            int l = nums[i];
            nums[i] = nums[end-1-i+start];
            nums[end-1-i+start] = l;
        }
    }

    public void rotate(int[] nums, int k) {
        k %= nums.length;
        if (k <= 0) return;

        reverse(nums, 0, nums.length);
        reverse(nums, 0, k);
        reverse(nums, k, nums.length);
    }
}
