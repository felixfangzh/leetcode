#include <iostream>
#include <stack>
using namespace std;

class Solution {
   public:
      bool isValid(string s) {
         stack<char> S; 
         for (auto &c : s) {
            if (!S.empty()
                  && ((S.top() == '(' && c == ')') 
                     || (S.top() == '[' && c == ']')
                     || (S.top() == '{' && c == '}')))
               S.pop();
            else 
               S.push(c);
         }
         return S.empty();
      }
};

int main() {
   Solution s;
   cout << s.isValid("[") << ", expected false" << endl;
   cout << s.isValid("[({(())}[()])]") << ", expected true" << endl;
   return 0;
}
