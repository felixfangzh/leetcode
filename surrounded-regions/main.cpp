#include <iostream>
#include <vector>
#include <queue>
#include <utility>
using namespace std;

class Solution {
   private:
      const char c = '+';
      void _mark(vector<vector<char>> &board, char m, size_t i, size_t j, queue<pair<int, int>> &q) {
         if (i < 0 || i >= board.size() || j < 0 || j >= board[i].size() || board[i][j] != 'O')
            return;

         board[i][j] = m;
         q.push(make_pair(i, j));
      }
      void mark(vector<vector<char>> &board, char m, size_t i, size_t j) {
         queue<pair<int, int>> q;
         _mark(board, c, i, j, q);
         while (!q.empty()) {
            pair<int, int> p = q.front(); q.pop();
            _mark(board, m, p.first-1, p.second, q);
            _mark(board, m, p.first+1, p.second, q);
            _mark(board, m, p.first, p.second-1, q);
            _mark(board, m, p.first, p.second+1, q);
         }
      }
   public:
      void solve(vector<vector<char>> &board) {
         if (board.empty()) return;
         size_t n(board.size()), m(board[0].size());
         for (size_t i = 0; i < m; ++i) {
            mark(board, c, 0, i);
            mark(board, c, n-1, i);
         }
         for (size_t i = 1; i < n-1; ++i) {
            mark(board, c, i, 0);
            mark(board, c, i, m-1);
         }

         for (auto &v : board)
            for (auto &k : v)
               if (k == 'O')
                  k = 'X';
               else if (k == c)
                  k = 'O';
      }
};

#define ROW 4
#define COL 4

int main() {
   Solution s;
   char _board [][COL] = {
      {'X', 'X', 'X', 'X'},
      {'X', 'O', 'O', 'X'},
      {'X', 'X', 'O', 'X'},
      {'X', 'O', 'X', 'X'}
   };
   vector<vector<char> > board(ROW, vector<char>(COL));
   for (size_t i = 0; i < ROW; ++i)
      for (size_t j = 0; j < COL; ++j)
         board[i][j] = _board[i][j];
   s.solve(board);
   for (size_t i = 0; i < ROW; ++i) {
      for (size_t j = 0; j < COL; ++j)
         cout << board[i][j] << " ";
      cout << endl;
   }
   return 0;
}
