#include <iostream>
#include <algorithm>
using namespace std;

class Solution {
    private:
        double findKth(int A[], int m, int B[], int n, int k) {
            if (m > n) return findKth(B, n, A, m, k);
            if (m <= 0) return B[k-1];
            if (k == 1) return A[0] < B[0] ? A[0] : B[0];

            int pa = min(m, k/2);
            int pb = k - pa;
            if (A[pa - 1] == B[pb - 1]) return A[pa - 1];

            return A[pa - 1] < B[pb - 1] ?
                findKth(A + pa, m - pa, B, pb, k - pa) :
                findKth(A, pa, B + pb, n - pb, k - pb);
        }
    public:
        double findMedianSortedArrays(int A[], int m, int B[], int n) {
            int length = m + n;
            if (length & 1) return findKth(A, m, B, n, length/2+1);
            else return (findKth(A, m, B, n, length/2) + findKth(A, m, B, n, length/2+1)) / 2;
        }
};

void test(int A[], int m, int B[], int n, double answer) {
    double result = Solution().findMedianSortedArrays(A, m, B, n);

    if (answer != result)
        cout << "FAILED: Expected: " << answer << ", Actual: " << result << endl;
    else
        cout << "PASSED: Result: " << result << endl;
}

int main () {
    int A1[] = {};
    int B1[] = {2, 3};
    test(A1, 0, B1, 2, 2.5);

    int A2[] = {1};
    int B2[] = {1};
    test(A2, 1, B2, 1, 1);

    return 0;
}
