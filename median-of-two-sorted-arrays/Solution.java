public class Solution {
    private double findKth(int A[], int B[], int k)
    {
        if (A.length > B.length) return findKth(B, A, k);
        if (A.length <= 0) return B[k-1];
        if (k == 1) return A[0] < B[0] ? A[0] : B[0];

        int pa = k/2 < A.length ? k/2 : A.length;
        int pb = k - pa;

        if (A[pa - 1] == B[pb - 1]) return A[pa - 1];
        if (A[pa - 1] < B[pb - 1]) {
            int [] newA = new int[A.length - pa];
            System.arraycopy(A, pa, newA, 0, newA.length);
            int [] newB = new int[pb];
            System.arraycopy(B, 0, newB, 0, newB.length);
            return findKth(newA, newB, k - pa);
        }
        else {
            int [] newA = new int[pa];
            System.arraycopy(A, 0, newA, 0, newA.length);
            int [] newB = new int[B.length - pb];
            System.arraycopy(B, pb, newB, 0, newB.length);
            return findKth(newA, newB, k - pb);
        }
    }

    public double findMedianSortedArrays(int A[], int B[]) {
        int length = A.length + B.length;
        return ((length & 1) == 1) ?
            findKth(A, B, length/2 + 1) :
            (findKth(A, B, length/2) + findKth(A, B, length/2 + 1)) / 2;
    }

    public static void main(String [] args) {
    }
}
