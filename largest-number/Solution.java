import java.util.*;

public class Solution {
   public String largestNumber(int[] num) {
      int zeroCount = 0;
      for (int i = 0; i < num.length; ++i)
         if (num[i] == 0)
            ++zeroCount;
      if (zeroCount == num.length)
         return "0";

      String [] str = new String[num.length];
      for (int i = 0; i < num.length; ++i)
         str[i] = Integer.toString(num[i]);

      Arrays.sort(str, new Comparator<String>() {
         public int compare(String o1, String o2)
         {
            return (o2 + o1).compareTo(o1 + o2);
         }
      });

      StringBuilder builder = new StringBuilder();
      for (String s : str)
         builder.append(s);

      return builder.toString();
   }
}
