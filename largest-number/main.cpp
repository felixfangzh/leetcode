#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution {
   public:
      string largestNumber(vector<int> &num) {
         bool allZeros = true;
         for (int n : num)
            if (n > 0)
               allZeros = false;
         if (allZeros)
            return "0";

         sort(num.begin(), num.end(), [](int l, int r) {
            size_t bl = fastCount(l);
            size_t br = fastCount(r);

            if (bl == br)
               return l > r;

            size_t cl = l;
            for (size_t i = 0; i < br; ++i)
               cl *= 10;
            cl += r;

            size_t cr = r;
            for (size_t i = 0; i < bl; ++i)
               cr *= 10;
            cr += l;

            return cl > cr;
         });

         string result;
         char buf[1024];
         for (int n : num) {
            sprintf(buf, "%d", n);
            result.append(buf);
         }

         return result;
      }
   private:
      static size_t fastCount(int num) {
         if (num < 10) return 1;
         if (num < 100) return 2;
         if (num < 1000) return 3;
         if (num < 10000) return 4;
         if (num < 100000) return 5;
         if (num < 1000000) return 6;
         if (num < 10000000) return 7;
         if (num < 100000000) return 8;
         if (num < 1000000000) return 9;
         return fastCount(num/1000000000) + 9; }
};

int main() {
   Solution solution;
   int num [] = {100000000, 2};
   vector<int> input(begin(num), end(num));
   cout << solution.largestNumber(input) << endl;
   return 0;
}
