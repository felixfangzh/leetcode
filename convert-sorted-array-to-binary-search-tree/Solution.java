/**
 * Definition for binary tree
 */
public class TreeNode {
   int val;
   TreeNode left;
   TreeNode right;
   TreeNode(int x) { val = x; }
}
public class Solution {
   public TreeNode _sortedArrayToBST(int[] num, int low, int high) {
      if (low > high) return null;
      int mid = low + ((high - low) >> 1);

      TreeNode node = new TreeNode(num[mid]);
      node.left = _sortedArrayToBST(num, low, mid - 1);
      node.right = _sortedArrayToBST(num, mid + 1, high);

      return node;
   }
   public TreeNode sortedArrayToBST(int[] num) {
      if (num.length <= 0) return null;
      return _sortedArrayToBST(num, 0, num.length-1);
   }
}
