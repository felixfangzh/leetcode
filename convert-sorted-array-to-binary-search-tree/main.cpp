#include <iostream>
#include <vector>
using namespace std;

struct TreeNode {
   int val;
   TreeNode *left;
   TreeNode *right;
   TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void printTreeNode(TreeNode* node) {
   if (!node)
      return;
   printTreeNode(node->left); 
   cout << node->val << " ";
   printTreeNode(node->right); 
}

template <typename T>
void printVector(vector<T> &v) {
   for (T &i : v) 
      cout << i << " ";
   cout << endl;
}

class Solution {
   public:
      TreeNode *sortedArrayToBST(vector<int> &num) {
         if (num.empty())
            return NULL;
         if (num.size() == 1)
            return new TreeNode(num[0]);
         int mid = num.size()/2;
         TreeNode* root = new TreeNode(num[mid]);
         vector<int> left(num.begin(), next(num.begin(), mid));
         root->left = sortedArrayToBST(left);
         vector<int> right(next(num.begin(), mid+1), num.end());
         root->right = sortedArrayToBST(right);
         return root;
      }
};

int main() {
   Solution s;
   int vals[] = {1, 2, 3}; 
   vector<int> num(begin(vals), end(vals));
   printTreeNode(s.sortedArrayToBST(num));
   cout << endl;
   return 0;
}
