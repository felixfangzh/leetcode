#include <iostream>
#include <unordered_map>
using namespace std;

struct RandomListNode {
   int label;
   RandomListNode *next, *random;
   RandomListNode(int x) : label(x), next(NULL), random(NULL) {}
};

class Solution {
   public:
      RandomListNode *copyRandomList(RandomListNode *head) {
         if (!head)
            return NULL;
         unordered_map<int, RandomListNode*> node_map;
         RandomListNode *newHead;

         for (RandomListNode* current = head; current; current = current->next) {
            auto iter = node_map.find(current->label);
            if (iter == node_map.end()) {
               node_map[current->label] = new RandomListNode(current->label);
            }
            if (current->next) {
               iter = node_map.find(current->next->label);
               if (iter == node_map.end()) {
                  node_map[current->next->label] = new RandomListNode(current->next->label);
               }
               node_map[current->label]->next = node_map[current->next->label];
            }
            if (current->random) {
               iter = node_map.find(current->random->label);
               if (iter == node_map.end()) {
                  node_map[current->random->label] = new RandomListNode(current->random->label);
               }
               node_map[current->label]->random = node_map[current->random->label];
            }
            if (current == head)
               newHead = node_map[current->label];
         }

         return newHead;
      }
};

int main() {
   Solution s;
   return 0;
}
