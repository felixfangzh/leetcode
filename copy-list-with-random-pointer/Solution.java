import java.util.*;

public class Solution {
    /**
     * Definition for singly-linked list with a random pointer.
     */
    class RandomListNode {
        int label;
        RandomListNode next, random;
        RandomListNode(int x) { this.label = x; }
    };
    public RandomListNode copyRandomList(RandomListNode head) {
        if (null == head) return head;
        Map<RandomListNode, RandomListNode> map = new HashMap<RandomListNode, RandomListNode>();

        for (RandomListNode current = head; current != null; current = current.next)
            map.put(current, new RandomListNode(current.label));

        for (RandomListNode current = head; current != null; current = current.next) {
            RandomListNode newNode = map.get(current);
            newNode.next = map.get(current.next);
            newNode.random = map.get(current.random);
        }

        return map.get(head);
    }
}
