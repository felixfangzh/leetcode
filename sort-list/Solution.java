public class Solution {
    /**
     * Definition for singly-linked list.
     */
    static class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {
            val = x;
            next = null;
        }
    }

    public ListNode merge(ListNode head1, ListNode head2) { 
        if (null == head2) return head1;
        if (null == head1) return head2;

        if (head1.val > head2.val) {
            ListNode tmp = head1;
            head1 = head2;
            head2 = tmp;
        }

        ListNode newHead = head1;

        while (head2 != null) {
            for (; head1.next != null && head1.next.val <= head2.val; head1 = head1.next);
            ListNode tmp1 = head1.next, tmp2 = head2.next;
            head1.next = head2;
            head2.next = tmp1;
            head2 = tmp2;
        }

        return newHead;
    }

    public ListNode split(ListNode head) { 
        ListNode slow = head;
        ListNode fast = head.next.next;

        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }

        ListNode newHead = slow.next;
        slow.next = null;

        return newHead;
    }

    public ListNode sortList(ListNode head) {
        if (head == null || head.next == null) return head;
        ListNode newHead = split(head);
        return merge(sortList(head), sortList(newHead));
    }

    public static void main(String [] args) {
    }
}
