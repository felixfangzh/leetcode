#include <iostream>
#include <vector>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
    public:
        ListNode *split(ListNode *head) {
            ListNode *slow = head, *fast = head->next->next;

            while (fast && fast->next) {
                slow = slow->next;
                fast = fast->next->next;
            }

            ListNode* newHead = slow->next;
            slow->next = NULL;
            return newHead;
        }

        ListNode *merge(ListNode *head1, ListNode* head2) {
            if (!head2) return head1;
            if (!head1) return head2;

            if (head1->val > head2->val) {
                ListNode *tmp = head1;
                head1 = head2;
                head2 = tmp;
            }

            ListNode *newHead = head1;

            while (head2) {
                for (; head1->next && head1->next->val <= head2->val; head1 = head1->next);
                ListNode *tmp1 = head1->next, *tmp2 = head2->next;
                head1->next = head2;
                head2->next = tmp1;
                head2 = tmp2;
            }

            return newHead;
        }

        ListNode *sortList(ListNode *head) {
            if (!head || !head->next) return head;
            ListNode* newHead = split(head);
            return merge(sortList(head), sortList(newHead));
        }
};

int main() {
    vector<ListNode*> nodes;
    int vals[] = {-21,10,17,8,4,26,5,35,33,-7,-16,27,-12,6,29,-12,5,9,20,14,14,2,13,-24,21,23,-21,5};
    for (auto i = begin(vals); i != end(vals); ++i) {
        nodes.push_back(new ListNode(*i));
    }
    for (int i = 0; i < nodes.size()-1; ++i) {
        nodes[i]->next = nodes[i+1]; 
    }

    Solution s;
    cout << "head->";
    for (ListNode* current = s.sortList(nodes[0]); current; current = current->next)
        cout << current->val << "->";
    cout << "end" << endl;
    return 0;
}
