#include <stdio.h>

/**
 * Definition for singly-linked list.
 */
struct ListNode {
    int val;
    struct ListNode *next;
};

struct ListNode* split(struct ListNode *head) {
    struct ListNode *slow = head, *fast = head->next->next;

    while (fast && fast->next) {
        slow = slow->next;
        fast = fast->next->next;
    }

    struct ListNode* newHead = slow->next;
    slow->next = NULL;
    return newHead;
}

struct ListNode* merge(struct ListNode* head1, struct ListNode* head2) {
    if (!head2) return head1;
    if (!head1) return head2;

    struct ListNode *newHead, *node;

    if (head1->val > head2->val) { 
        node = head1;
        head1 = head2;
        head2 = node;
    }

    newHead = head1;

    while (head2) {
        for (; head1->next && head1->next->val <= head2->val; head1 = head1->next);
        node = head1->next;
        head1->next = head2;
        head2 = head2->next;
        head1->next->next = node;
    }

    return newHead;
}

struct ListNode* sortList(struct ListNode *head) {
    if (!head || !head->next) return head;
    struct ListNode* head2 = split(head);
    return merge(sortList(head), sortList(head2));
}

int main() {
    return 0;
}
