#include <iostream>

using namespace std;

class Solution {
    public:
        string addBinary(string a, string b) {
            string s;
            s.resize(max(a.size(), b.size()) + 1);

            size_t carry = 0;
            for (ssize_t i = a.size() - 1, j = b.size() - 1, k = s.size() - 1;
                    i >= 0 || j >= 0; ) {
                size_t val;
                if (i >= 0 && j >= 0) {
                    val = a[i--] - '0' + b[j--] - '0' + carry;
                }
                else if (i >= 0) {
                    val = a[i--] - '0' + carry;
                }
                else {
                    val = b[j--] - '0' + carry;
                }

                s[k--] = (val & 1) + '0';
                carry = (val & 2) >> 1;

                cout << (val & 1) << endl;
                cout << carry << endl << endl;
            }

            if (carry > 0) {
                s[0] = '1';
                return s;
            }

            return s.substr(1);
        }
};

int main() {
    Solution sol;
    cout << sol.addBinary("11", "1") << endl;
    return 0;
}
