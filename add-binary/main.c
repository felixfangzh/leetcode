#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *addBinary(char *a, char *b) {
    if (!a) return a;
    if (!b) return b;

    size_t len_a = strlen(a);
    size_t len_b = strlen(b);

    size_t len_s = len_a > len_b ? len_a : len_b;
    char * s = malloc((++len_s+1) * sizeof(char));
    s[len_s] = '\0';

    size_t carry = 0, val;
    ssize_t i = len_a - 1;
    ssize_t j = len_b - 1;
    ssize_t k = len_s - 1;

    while (i >= 0 || j >= 0) {
        if (i >= 0 && j >= 0) {
            val = a[i--] - '0' + b[j--] - '0' + carry;
        }
        else if (i >= 0) {
            val = a[i--] - '0' + carry;
        }
        else {
            val = b[j--] - '0' + carry;
        }

        s[k--] = (val & 1) + '0';
        carry = (val & 2) >> 1;
    }

    if (carry > 0) {
        s[k] = '1';
    }
    else {
        memcpy(s, s+1, len_s);
    }

    return s;
}

int main() {
    char * s = addBinary("11", "1001");
    printf("%s\n", s);
    free(s);
    return 0;
}
