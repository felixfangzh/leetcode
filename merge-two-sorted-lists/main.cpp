/**
 * Definition for singly-linked list.
 */
#define NULL 0
struct ListNode {
   int val;
   ListNode *next;
   ListNode(int x) : val(x), next(NULL) {}
};
class Solution {
   public:
      ListNode *mergeTwoLists(ListNode *l1, ListNode *l2) {
         if (!l1)
            return l2;
         if (!l2)
            return l1;
         ListNode *head, *tail, *current;
         head = tail = current = NULL;
         while (l1 || l2) {
            if (l1 && l2) {
               if (l1->val < l2->val) {
                  current = l1;
                  l1 = l1->next;
               }
               else {
                  current = l2;
                  l2 = l2->next;
               }
            }
            else if (l1) {
               current = l1;
               l1 = l1->next;
            }
            else if (l2) {
               current = l2;
               l2 = l2->next;
            }
            if (!head)
               head = tail = current;
            else {
               tail->next = current;
               tail = tail->next;
            }
         }
         return head;
      }
};

int main() {
   return 0;
}
