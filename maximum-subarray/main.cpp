#include <iostream>
using namespace std;

class Solution {
   public:
      int maxSubArray(int A[], int n) {
         if (n <= 0)
            return 0;
         int ret = A[0], sum = A[0];
         for (int i = 1; i < n; ++i) {
            sum = max(A[i], sum+A[i]);
            ret = max(ret, sum);
         }
         return ret;
      }
};

int main() {
   Solution s;
   int A [] = {-2,1,-3,4,-1,2,1,-5,4};
   cout << s.maxSubArray(A, distance(begin(A), end(A))) << endl;
   return 0;
}
