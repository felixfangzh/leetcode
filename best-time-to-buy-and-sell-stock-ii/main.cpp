#include <iostream>
using namespace std;

class Solution {
   public:
      int maxProfit(vector<int> &prices) {
         int profit = 0;
         for (int i = 1, size = prices.size(); i < size; ++i)
            if (prices[i] > prices[i-1])
               profit += prices[i] - prices[i-1];
         return profit;
      }
};

int main() {
   return 0;
}
