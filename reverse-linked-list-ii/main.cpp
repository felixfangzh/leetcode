#include <iostream>
#include <vector>
using namespace std;

struct ListNode {
   int val;
   ListNode *next;
   ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
   public:
      ListNode *reverseBetween(ListNode *head, int m, int n) {
         ListNode* current = NULL;
         for (int i = 1; i < m; ++i) {
            current = current ? current->next : head;
         }
         ListNode* previous = NULL;
         ListNode* _head = current;
         current = current ? current->next : head;
         ListNode* _tail = current;
         for (int i = m; i <= n; ++i) {
            ListNode* next = current->next;
            current->next = previous;
            previous = current;
            current = next;
         }
         _tail->next = current;
         if (_head) {
            _head->next = previous;
            return head;
         }
         return previous;
      }
};

int main() {
   int vals [] = {1, 2, 3, 4, 5};
   vector<ListNode*> nodes;
   for (auto i = begin(vals); i != end(vals); ++i) {
      nodes.push_back(new ListNode(*i));
   }
   for (int i = 1; i < nodes.size(); ++i) {
      nodes[i-1]->next = nodes[i];
   }
   Solution s;
   for (ListNode* node = s.reverseBetween(nodes[0], 1, 5); node; node = node->next)
      cout << node->val << " ";
   cout << endl;
   return 0;
}
