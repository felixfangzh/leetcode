#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

struct TreeNode {
   int val;
   TreeNode *left;
   TreeNode *right;
   TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
   void _zigzagLevelOrder(TreeNode *root, int level, vector<vector<int> > &zigzags) {
      if (!root)
         return;

      if (zigzags.size() == level)
         zigzags.push_back(vector<int>());
      zigzags[level].push_back(root->val);
      _zigzagLevelOrder(root->left, level+1, zigzags);
      _zigzagLevelOrder(root->right, level+1, zigzags);
   }
   public:
   vector<vector<int> > zigzagLevelOrder(TreeNode *root) {
      vector<vector<int> > zigzags;
      _zigzagLevelOrder(root, 0, zigzags);
      for (int i = 0, size = zigzags.size(); i < size; ++i)
         if (i & 1)
            reverse(zigzags[i].begin(), zigzags[i].end());
      return zigzags;
   }
};

int main() {
   Solution s;
   return 0;
}
