/**
 * Definition for binary tree
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> lists = new ArrayList<List<Integer>>();
        if (null == root) return lists;

        List<Integer> list = new ArrayList<Integer>();

        Deque<TreeNode> queue = new ArrayDeque<TreeNode>();
        queue.offer(root);

        int count = 1;
        int nextCount = 0;

        while (!queue.isEmpty()) {
            TreeNode node = queue.poll();
            list.add(node.val);

            if (node.left != null) {
                queue.offer(node.left);
                ++nextCount;
            }

            if (node.right != null) {
                queue.offer(node.right);
                ++nextCount;
            }

            if (--count <= 0) {
                if ((lists.size() & 1) == 1)
                    Collections.reverse(list);
                lists.add(list);
                list = new ArrayList<Integer>();
                count = nextCount;
                nextCount = 0;
            }
        }

        return lists;
    }
}
