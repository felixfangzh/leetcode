import java.util.*;

public class Solution {
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        Map<Integer, List<Integer>> graph = new HashMap<Integer, List<Integer>>();

        for (int i = 0; i < numCourses; ++i) {
            graph.put(i, new ArrayList<Integer>());
        }

        // Populate graph
        for (int i = 0; i < prerequisites.length; ++i) {
            graph.get(prerequisites[i][1]).add(prerequisites[i][0]);
        }

        Map<Integer, Integer> inDegrees = new HashMap<Integer, Integer>();

        for (int i = 0; i < numCourses; ++i) {
            inDegrees.put(i, 0);
        }

        for(Map.Entry<Integer, List<Integer>> entry : graph.entrySet()) {
            for (Integer neighbor : entry.getValue()) {
                inDegrees.put(neighbor, inDegrees.get(neighbor) + 1);
            }
        }

        while (!inDegrees.isEmpty()) {
            boolean foundVertexWithZeroInDegrees = false;
            Iterator<Map.Entry<Integer, Integer>> entries = inDegrees.entrySet().iterator();
            while (entries.hasNext()) {
                Map.Entry<Integer, Integer> entry = entries.next();
                if (entry.getValue() == 0) {
                    for (Integer neighbor : graph.get(entry.getKey())) {
                        inDegrees.put(neighbor, inDegrees.get(neighbor) - 1);
                    }

                    entries.remove();

                    foundVertexWithZeroInDegrees = true;
                }
            }

            if(!foundVertexWithZeroInDegrees) {
                return false;
            }
        }

        return true;
    }
}
