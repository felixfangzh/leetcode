#include <vector>
#include <unordered_map>

using namespace std;

class Solution {
    public:
        bool canFinish(int numCourses, vector<pair<int, int>>& prerequisites) {
            unordered_map<int, vector<int>> graph;
            for(int i = 0; i < numCourses; ++i) {
                graph[i] = vector<int>();
            }

            for(const pair<int, int>& p : prerequisites) {
                graph[p.second].push_back(p.first);
            }

            unordered_map<int, int> inDegrees;
            for(int i = 0; i < numCourses; ++i) {
                inDegrees[i] = 0;
            }

            for(auto& vertex : graph) {
                for(int neighbor : vertex.second) {
                    ++inDegrees[neighbor];
                }
            }

            while(!inDegrees.empty()) {
                bool foundVertexWithZeroInDegrees = false;
                for(auto iterator = inDegrees.begin(); iterator != inDegrees.end();) {
                    if (iterator->second == 0)
                    {
                        for(int neighbor : graph[iterator->first]) {
                            --inDegrees[neighbor];
                        }

                        iterator = inDegrees.erase(iterator);
                        foundVertexWithZeroInDegrees = true;
                    }
                    else {
                        ++iterator;
                    }
                }

                if(!foundVertexWithZeroInDegrees) {
                    return false;
                }
            }

            return true;
        }
};

int main() {
    return 0;
}
