#include <iostream>
#include <vector>
using namespace std;

struct ListNode {
   int val;
   ListNode *next;
   ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
   public:
      ListNode *deleteDuplicates(ListNode *head) {
         for (ListNode *current = head, *previous = NULL; current; current = current->next) {
            if (!current->next)
               break;
            if (current->val != current->next->val) {
               previous = current;
               continue;
            }
            for (; current->next && current->val == current->next->val;) {
               ListNode* tmp = current->next;
               delete current;
               current = tmp;
            }
            if (previous)
               previous->next = current->next;
            else
               head = current->next;
         }
         return head;
      }
};

int main() {
   vector<ListNode*> nodes;
   int vals[] = {1, 2, 3, 3, 4, 4, 5};
   for (auto i = begin(vals); i != end(vals); ++i) {
      nodes.push_back(new ListNode(*i));
   }
   for (int i = 0; i < nodes.size(); ++i) {
      nodes[i]->next = nodes[i+1]; 
   }

   cout << "head->";
   for (ListNode* current = nodes[0]; current; current = current->next) {
      cout << current->val << "->";
   }
   cout << "end" << endl;

   Solution s;
   cout << "head->";
   for (ListNode* current = s.deleteDuplicates(nodes[0]); current; current = current->next) {
      cout << current->val << "->";
   }
   cout << "end" << endl;
   return 0;
}
