#include <iostream>
#include <vector>
using namespace std;

class Solution {
   public:
      int maxProfit(vector<int> &prices) {
         size_t n = prices.size();
         if (n <= 1)
            return 0;
         vector<int> first(n, 0);
         vector<int> second(n, 0);
         int curMin = prices[0], curMax = prices[n-1], profit = 0;

         for (int i = 1; i < n; ++i) {
            curMin = min(prices[i], curMin);
            first[i] = max(first[i-1], prices[i] - curMin);
         }
         for (int i = n - 2; i >= 0; --i) {
            curMax = max(prices[i], curMax);
            second[i] = max(second[i+1], curMax - prices[i]);
         }

         for (int i = 0; i < n; ++i)
            profit = max(profit, first[i] + second[i]);

         return profit;
      }
};

int main() {
   Solution s;
   int vals [] = {3,3,5,0,0,3,1,4};
   vector<int> prices(begin(vals), end(vals));
   cout << s.maxProfit(prices) << endl;
   return 0;
}
