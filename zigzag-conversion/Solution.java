public class Solution {
    public String convert(String s, int nRows) {
        if (nRows <= 0) return "";

        if (nRows >= s.length() || nRows == 1) return s;
        StringBuilder builder = new StringBuilder();

        for (int currentRow = 0; currentRow < nRows; ++currentRow) {
            for (int currentIndex = currentRow, direction = 1, nextStep; currentIndex < s.length(); currentIndex += nextStep) {
                nextStep = (direction == 1 ? (nRows - currentRow - 1) : (currentRow)) * 2; 
                direction ^= 1;
                if (nextStep == 0) continue;

                builder.append(s.charAt(currentIndex));
            }
        }

        return builder.toString();
    }
}
