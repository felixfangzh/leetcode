#include <iostream>

using namespace std;

class Solution {
    public:
        string convert(string s, int nRows) {
            if (nRows <= 0) return "";
            if (nRows == 1 || nRows >= s.size()) return s;

            string newS;
            newS.reserve(s.size());

            for (int currentRow = 0; currentRow < nRows; ++currentRow) {
                for (int currentIndex = currentRow, direction = 1, nextStep; currentIndex < s.size(); currentIndex += nextStep) {
                    nextStep = (direction ? (nRows - currentRow - 1) : (currentRow)) * 2;
                    direction ^= 1;
                    if (!nextStep) continue;
                    newS.push_back(s[currentIndex]);
                }
            }

            return newS;
        }
};

template<typename TArg1, typename TArg2, typename TAnswer>
void test(TArg1 s, TArg2 nRows, TAnswer answer) {
    string result = Solution().convert(s, nRows);
    if (answer != result)
        cout << "FAILED: Input: (" << s << "," << nRows << "), Expected: " << answer << ", Actual: " << result << endl;
    else
        cout << "PASSED: Input: (" << s << "," << nRows << "), Result: " << result << endl;
}

int main() {
    test("ABCDE", 2, "ACEBD");
    return 0;
}
