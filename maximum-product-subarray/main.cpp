#include <iostream>
#include <iterator>

using namespace std;

class Solution {
   public:
      int maxProduct(int A[], int n) {
         if (n <= 0)
            return 0;

         int currentMax = A[0];
         int currentMin = A[0];
         int maxResult = A[0];

         for (size_t i = 1; i < n; ++i) {
               int tmp = currentMax;
               currentMax = max(currentMin*A[i], max(currentMax*A[i], A[i]));
               currentMin = min(tmp*A[i], min(currentMin*A[i], A[i]));
               maxResult = max(currentMax, maxResult);
         }

         return maxResult;
      }
};

int main() {
   return 0;
}
