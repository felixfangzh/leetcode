#include <iostream>
#include <vector>
using namespace std;

struct ListNode {
   int val;
   ListNode *next;
   ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
   private: 
      ListNode* reverseLinkedList(ListNode* head) {
         if(!head || !head->next) return head;

         ListNode* newHead = nullptr;
         while (head) {
            ListNode* tmp = head->next;
            head->next = newHead;
            newHead = head;
            head = tmp;
         }

         return newHead;
      }

   public:
      bool isPalindrome(ListNode* head) {
         if(!head || !head->next) return true;

         ListNode* slow = head;
         ListNode* fast = head;
         for (; (fast && fast->next); slow = slow->next, fast = fast->next->next);

         ListNode* headEnd = slow;

         if (fast) {
            slow = slow->next;
         }

         slow = reverseLinkedList(slow);

         for (; head != headEnd || slow; head = head->next, slow = slow->next) {
            if (head != headEnd && slow) {
               if (head->val != slow->val) return false;
            }
            else {
               return false;
            }
         }

         return true;
      }
};

int main() {
   vector<int> nums = {1, 2, 2, 1};
   vector<ListNode> nodes = vector<ListNode>(nums.size(), ListNode(0));


   for(size_t i = 0; i < nums.size(); ++i) {
      nodes[i] = ListNode(nums[i]);
      cout << &nodes[i] << " ";
   }

   cout << endl;

   for(size_t i = 1; i < nums.size(); ++i) {
      nodes[i - 1].next = &nodes[i];
   }

   Solution sol;
   cout << sol.isPalindrome(&nodes[0]) << endl;

   return 0;
}
