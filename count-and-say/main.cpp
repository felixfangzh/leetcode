#include <iostream>
#include <vector>
using namespace std;

class Solution {
   vector<string> results;
   public:
      string countAndSay(int n) {
         if (n < 1)
            return "";
         n--;
         if (results.size() > n)
            return results[n];
         string s;
         for (size_t i = results.size(); i <= n; ++i) {
            if (!i)
               s = "1";
            else {
               string _s;
               char c = s[0];
               int counter = 1;
               for (int j = 1; j < s.size(); ++j) {
                  if (c == s[j])
                     ++counter;
                  else {
                     _s += string(to_string(counter));
                     _s += c;
                     c = s[j];
                     counter = 1;
                  }
               }
               _s += string(to_string(counter));
               _s += c;
               s = _s;
            }
            results.push_back(s);
         }
         return s;
      }
};

int main () {
   Solution s;
   cout << s.countAndSay(2) << endl;
   //cout << s.countAndSay(5) << endl;
   //cout << s.countAndSay(3) << endl;
   return 0;
}
