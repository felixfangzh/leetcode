public class Solution {
    public boolean canJump(int[] A) {
        for (int i = 1, next = 0, current = A[0]; i < A.length; ++i) {
            if (next < i) {
                if (current < i) return false;
                next = current;
            }

            current = Math.max(current, i + A[i]);
        }

        return true;
    }
}
