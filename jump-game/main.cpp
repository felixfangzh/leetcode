#include <iostream>
using namespace std;

class Solution {
   public:
      bool canJump(int A[], int n) {
         if (n <= 1) return true;
         for (size_t i = 1, next = 0, current = A[0]; i < n; ++i) {
            if (next < i) {
               if (current < i) return false;
               next = current;
            }
            current = max(current, i + A[i]);
         }
         return true;
      }
};

int main() {
   Solution s;
   return 0;
}
