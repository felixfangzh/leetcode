#define NULL 0

/**
 * Definition for binary tree with next pointer.
 */
struct TreeLinkNode {
   int val;
   struct TreeLinkNode *left, *right, *next;
};

void connect(struct TreeLinkNode *root) {
   if (!root) return;
   struct TreeLinkNode *head = root, *nextHead = NULL, *current = NULL, *prev = NULL;

   for (current = head; current; ) {
      if (!nextHead) {
         if (current->left)
            nextHead = current->left;
         else if (current->right)
            nextHead = current->right;
         if (!nextHead) continue;
      }

      if (prev) {
         if (current->left)
            prev->next = current->left;
         else if (current->right)
            prev->next = current->right;
      }

      if (current->right)
         prev = current->right;
      else if (current->left)
         prev = current->left;

      if (!current->next) {
         head = nextHead;
         current = head;
         nextHead = NULL;
         prev = NULL;
      }
      else {
         current = current->next;
      }
   }
}

int main() {
   return 0;
}
