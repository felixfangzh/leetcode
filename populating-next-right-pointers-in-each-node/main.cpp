/**
 * Definition for binary tree with next pointer.
 */
struct TreeLinkNode {
   int val;
   TreeLinkNode *left, *right, *next;
   TreeLinkNode(int x) : val(x), left(NULL), right(NULL), next(NULL) {}
};


class Solution {
   public:
      void connect(TreeLinkNode *root) {
         if (!root) return;
         TreeLinkNode *head = root, *nextHead = NULL, *current = NULL, *prev = NULL;

         for (current = head; current; ) {
            if (!nextHead) {
               if (!head->left) break;
               nextHead = head->left;
            }

            if (!prev) {
               if (!current->right) break;
            }
            else {
               prev->next = current->left;
            }

            if (current->left) {
               current->left->next = current->right;
            }

            prev = current->right;

            if (!current->next) {
               head = nextHead;
               current = head;
               nextHead = NULL;
               prev = NULL;
            }
            else {
               current = current->next;
            }
         }
      }
};
