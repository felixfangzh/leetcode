#include <vector>
#include <iostream>
#include <unordered_map>
using namespace std;

class Solution {
   public:
      vector<int> findSubstring(string S, vector<string> &L) {
         vector<int> indices;
         size_t wordSize = L[0].size();
         size_t windowSize = wordSize * L.size();
         unordered_map<string, int> word_map;

         for (auto &word : L)
            ++word_map[word];

         for (auto i = begin(S); i <= prev(end(S), windowSize); ++i) {
            unordered_map<string, int> new_map(word_map);
            for (auto j = i; j != next(i, windowSize); j+=wordSize) {
               auto iter = new_map.find(string(j, next(j, wordSize)));
               if (iter == new_map.end())
                  break;
               if (!--iter->second)
                  new_map.erase(iter);
            }
            if (new_map.empty())
               indices.push_back(distance(begin(S), i));
         }

         return indices;
      }
};

int main () {
   string S = "lingmindraboofooowingdingbarrwingmonkeypoundcake";
   string _L [] = {"fooo","barr","wing","ding","wing"};
   vector<string> L(begin(_L), end(_L));
   Solution s;
   for (int &i : s.findSubstring(S, L))
      cout << i << " ";
   cout << endl;
   return 0;
}
