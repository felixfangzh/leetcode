public class Solution {
    public int reverse(int x) {
        int sign = x >= 0 ? 1 : -1;
        int num = 0;

        if (x < 0) x = -x;

        while (x > 0) {
            if (num > Integer.MAX_VALUE/10) return 0;
            num *= 10;
            if (num > Integer.MAX_VALUE - x%10) return 0;
            num += x%10;
            x /=10;
        }

        return num * sign;
    }
}
