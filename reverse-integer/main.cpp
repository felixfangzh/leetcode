#include <iostream>
using namespace std;

class Solution {
   public:
      int reverse(int x) {
          int sign = x >= 0 ? 1 : -1;
          int num = 0;

          if (x < 0) x = -x;

          while (x > 0) {
              if (num > INT_MAX/10) return 0;
              num *= 10;
              if (num > INT_MAX - x%10) return 0;
              num += x%10;
              x /= 10;
          }

          return num * sign;
      }
};

int main() {
   Solution s;
   cout << s.reverse(123) << endl;
   cout << s.reverse(-123) << endl;
   return 0;
}
