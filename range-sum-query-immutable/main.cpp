#include <vector>

using namespace std;

class NumArray {
    private:
        vector<int> sums;
    public:
        NumArray(vector<int> &nums) {
            sums.reserve(nums.size());
            for(int& num : nums)
                sums.push_back(num + sums.back());
        }

        int sumRange(int i, int j) {
            return sums[j] - (i > 0 ? sums[i-1] : 0);
        }
};
