#include <iostream>
#include <climits>
using namespace std;

class Solution {
   public:
      int atoi(const char *str) {
         if (!str)
            return -1;
         int num = 0;
         bool isNegative = false;
         bool digitStart = false;
         for (; *str; ++str) {
            if (!digitStart) {
               if (*str == ' ')
                  continue;
               if (*str == '+') {
                  digitStart = true;
                  continue;
               }
               if (*str == '-') {
                  digitStart = true;
                  isNegative = true;
                  continue;
               }
            }
            if (*str >= '0' && *str <= '9') {
               int digit = *str - '0';
               if (isNegative) {
                  if (num >= (INT_MIN + digit)/10) {
                     num *= 10;
                     num -= digit;
                  }
                  else {
                     num = INT_MIN;
                  }
               }
               else {
                  if (num <= (INT_MAX - digit)/10) {
                     num *= 10;
                     num += digit;
                  }
                  else {
                     num = INT_MAX;
                  }
               }
               if (!digitStart && isNegative) {
                  num = -num;
               }
               digitStart = true;
            }
            else 
               return num; 
         }
         return num; 
      }
};

int main() {
   Solution s;
   //cout << s.atoi("   -12323") << endl;
   //cout << s.atoi("1") << endl;
   //cout << s.atoi("11231") << endl;
   cout << s.atoi("  -0012a42") << endl;
   //cout << s.atoi("2147483648") << endl;
   return 0;
}
