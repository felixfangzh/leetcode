#include <iostream>
#include <climits>
using namespace std;

class Solution {
   public:
      int divide(int dividend, int divisor) {
         if (divisor == -1 && dividend == INT_MIN) return INT_MAX;
         long long a(abs((long long)dividend)), b(abs((long long)divisor)), ret(0), i(0);
         bool asc = true;
         while (a >= b) {
            long long c = b << i;
            if (a >= c) {
               a -= c;
               ret += 1 << i;
            }
            if (asc) {
               ++i;
               asc = (c << 1) <= a;
            }
            else {
               --i;
            }
         }
         return ((dividend^divisor) >> 31) ? -ret : ret;
      }
};

int main() {
   Solution s;
   cout << s.divide(3, 2) << endl;
   cout << s.divide(-13, 2) << endl;
   cout << s.divide(-13, 3) << endl;
   cout << s.divide(INT_MIN, -1) << endl;
   cout << s.divide(INT_MIN, 1) << endl;
   return 0;
}
