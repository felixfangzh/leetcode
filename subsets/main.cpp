#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution {
   public:
      vector<vector<int> > subsets(vector<int> &S) {
         sort(S.begin(), S.end());
         vector<vector<int> > subs;
         for (int i = 0, _size = S.size(), size = 1 << _size; i < size; ++i) {
            vector<int> sub;
            for (int j = 0; j < _size; ++j)
               if (i & (1 << j))
                  sub.push_back(S[j]);
            subs.push_back(sub);
         }

         return subs;
      }
};

int main() {
   Solution s;
   int vals [] = {1, 2, 3};
   vector<int> S(begin(vals), end(vals));
   for (auto &v : s.subsets(S)) {
      for (auto i : v)
         cout << i << " ";
      cout << endl;
   }
   return 0;
}
