import java.util.*;

public class Solution {
    public List<List<Integer>> subsets(int[] S, int start, int end, int size) {
        List<List<Integer>> lists = new ArrayList<List<Integer>>();
        if (size <= 0 || start > end) {
            lists.add(Collections.EMPTY_LIST);
            return lists;
        }

        for (int i = start; i <= end - size + 1; ++i) {
            for (List<Integer> l : subsets(S, i + 1, end, size - 1)) {
                List<Integer> list = new ArrayList<Integer>();
                list.add(S[i]);
                list.addAll(l);
                lists.add(list);
            }
        }

        return lists;
    }

    public List<List<Integer>> subsets(int[] S) {
        List<List<Integer>> lists = new ArrayList<List<Integer>>();

        Arrays.sort(S);

        for (int i = 0; i <= S.length; ++i)
            lists.addAll(subsets(S, 0, S.length-1, i));

        return lists;
    }

    public static void test(int [] S) {
        for (List<Integer> list : new Solution().subsets(S)) {
            for (Integer i : list)
                System.out.print(i + " ");
            System.out.println("\b");
        }
    }

    public static void main(String [] args) {
        test(new int [] {1, 2, 3});
    }
}
