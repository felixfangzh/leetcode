#include <iostream>
using namespace std;

struct ListNode {
   int val;
   ListNode *next;
   ListNode(int x) : val(x), next(NULL) {}
};


struct TreeNode {
   int val;
   TreeNode *left;
   TreeNode *right;
   TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
   public:
      TreeNode *sortedListToBST(ListNode *head) {
         if (!head)
            return NULL;
         TreeNode *root;
         ListNode *slow = NULL, *fast = head;
         while (fast->next) {
            fast = fast->next;
            if (!fast->next)
               break;
            fast = fast->next;
            slow = slow ? slow->next : head; 
         }

         if (!slow) {
            root = new TreeNode(fast->val);
            if (fast != head) {
               root->left = new TreeNode(head->val);
            }
         }
         else {
            root = new TreeNode(slow->next->val);
            ListNode* rightStart = slow->next->next;
            slow->next = NULL;
            root->left = sortedListToBST(head);
            root->right = sortedListToBST(rightStart);
         }
         return root;
      }
};

int main() {
   return 0;
}
