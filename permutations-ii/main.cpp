#include <algorithm>
#include <iostream>
#include <vector>
#include <unordered_set>

using namespace std;

class Solution {
    private:
        vector<vector<int> > _permuteUnique(vector<int> &num) {
            unordered_set<int> num_set;
            vector<vector<int> > res;
            for (size_t i = 0; i < num.size(); ++i) {
                if (num_set.find(num[i]) != num_set.end()) continue;
                num_set.insert(num[i]);

                vector<int> subRes;
                subRes.reserve(num.size() - 1);
                for (size_t j = 0; j < num.size(); ++j)
                    if (i != j)
                        subRes.push_back(num[j]);
                if (subRes.empty()) {
                    subRes.push_back(num[i]);
                    res.push_back(subRes);
                }
                else {
                    for (auto &ret : _permuteUnique(subRes)) {
                        ret.push_back(num[i]);
                        res.push_back(ret);
                    }
                }
            }
            return res;
        }
    public:
        vector<vector<int> > permuteUnique(vector<int> &num) {
            return _permuteUnique(num);
        }
};

int main() {
    Solution sol;
    int input [] = {1, 1, 2};
    vector<int> num(begin(input), end(input));
    for (auto &ret : sol.permuteUnique(num)) {
        for (auto i : ret)
            cout << i << " ";
        cout << endl;
    }
    return 0;
}
