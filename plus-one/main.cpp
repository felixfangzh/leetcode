#include <iostream>
#include <vector>
using namespace std;

class Solution {
   public:
      vector<int> plusOne(vector<int> &digits) {
         int i, carry = 1;
         for (i = digits.size()-1; i >=0; --i) {
            if (!carry)
               break;
            digits[i] += carry;
            carry = digits[i] / 10;
            digits[i] %= 10;
         }
         if (carry) digits.insert(digits.begin(), carry);
         return digits;
      }
};

int main() {
   Solution s;
   return 0;
}
