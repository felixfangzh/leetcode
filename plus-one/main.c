#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int *plusOne(int *digits, int len, int *newLen) {
    ssize_t i;
    int carry = 1;
    for (i = len-1; i >=0; --i) {
        if (!carry) break;
        digits[i] += carry;
        carry = digits[i]/10;
        digits[i] %= 10;
    }

    *newLen = len;
    int *newDigits = digits;
    if (carry > 0) {
        ++*newLen;
        newDigits = calloc(*newLen, sizeof(int));
        memcpy(newDigits+1, digits, len*sizeof(int));
        newDigits[0] = carry;
    }
    else {
        newDigits = calloc(*newLen, sizeof(int));
        memcpy(newDigits, digits, len*sizeof(int));
    }

    return newDigits;
}

int main() {
    int digits[] = {1, 0};
    int newLen;
    int *newDigits = plusOne(digits, 2,  &newLen);

    size_t i;
    for (i = 0; i < newLen; ++i) {
        printf("%d", newDigits[i]);
    }

    printf("\n");

    return 0;
}
