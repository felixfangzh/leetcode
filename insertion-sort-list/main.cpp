#include <iostream>
#include <vector>
using namespace std;

struct ListNode {
   int val;
   ListNode *next;
   ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
   public:
      ListNode *insertionSortList(ListNode *head) {
         for (ListNode *previous = NULL, *current = head; current; ) {
            ListNode* next = current->next;
            if (current == head) {
               previous = current;
               current = next;
               continue;
            }
            ListNode *_next, *node = NULL; 
            bool found = false;
            for (;node != current; node = _next) {
               _next = node ? node->next : head;
               if (current->val < _next->val) {
                  found = true;
                  break;
               }
            }
            if (found) {
               previous->next = next;
               current->next = _next;
               if (node)
                  node->next = current;
               else
                  head = current;
            }
            else {
               previous = current;
            }
            current = next;
         }
         return head;
      }
};

int main() {
   int vals [] = {5, 2, 4, 3, 1};
   vector<ListNode*> nodes;
   for (auto i = begin(vals); i != end(vals); ++i) {
      nodes.push_back(new ListNode(*i));
   }
   for (int i = 1; i < nodes.size(); ++i) {
      nodes[i-1]->next = nodes[i];
   }
   Solution s;
   for (ListNode* node = s.insertionSortList(nodes[0]); node; node = node->next)
      cout << node->val << " ";
   cout << endl;
   return 0;
}
