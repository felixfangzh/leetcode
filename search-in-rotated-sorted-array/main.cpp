#include <iostream>
#include <climits>
using namespace std;

class Solution {
   public:
      int search(int A[], int n, int target) {
         int i(0), j(n-1);
         while (i < j) {
            int mid = (i+j)/2;
            if (target == A[mid])
               return mid;
            else if (target < A[mid]) {
               if (A[i] <= A[mid]) {
                  if (target == A[i])
                     return i;
                  else if (target > A[i])
                     j = mid-1;
                  else
                     i = mid+1;
               }
               else {
                  j = mid-1;
               }
            }
            else {
               if (A[j] >= A[mid]) {
                  if (target == A[j])
                     return j;
                  else if (target < A[j])
                     i = mid+1;
                  else
                     j = mid-1;
               }
               else {
                  i = mid + 1;
               }
            }
         }
         return A[i] == target ? i : -1;
      }
};

int main() {
   Solution s;
   int vals [] = {3, 1};
   cout << s.search(vals, distance(begin(vals), end(vals)), 1) << ", expected 1" << endl;
   return 0;
}
