#include <iostream>
using namespace std;

class Solution {
   public:
      bool search(int A[], int n, int target) {
         int i(0), j(n-1);
         while (i <= j) {
            int mid(i + (j-i)/2);
            if (A[mid] == target) {
               return true;
            }
            else if (A[i] < A[mid]) {
               if (A[i] <= target && target < A[mid])
                  j = mid-1;
               else
                  i = mid+1;
            }
            else if (A[i] > A[mid]) {
               if (A[mid] < target && target <= A[j])
                  i = mid+1;
               else
                  j = mid-1;
            }
            else ++i;
         }
         return false;
      }
};

int main() {
   Solution s;
   return 0;
}
