#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution {
   public:
      vector<vector<int> > threeSum(vector<int> &num) {
         vector<vector<int> > ret;
         sort(num.begin(), num.end());
         for (size_t i = 0, size = num.size(); i < size; ++i) {
            if (i != 0 && num[i] == num[i-1]) continue;
            for (ssize_t a(i+1), b(size-1); a < b;) {
               int sum = num[i] + num[a] + num[b];
               if (!sum) {
                  ret.push_back(vector<int>());
                  ret.back().push_back(num[i]);
                  ret.back().push_back(num[a]);
                  ret.back().push_back(num[b]);
                  ++a;
                  --b;
                  for (; a < b && num[a] == num[a-1]; ++a);
                  for (; a < b && num[b] == num[b+1]; --b);
               }
               else if (sum > 0)
                  --b;
               else
                  ++a;
            }
         }
         return ret;
      }
};

int main() {
   Solution s;
   int _input [] = {-1, 0, 1, 2, -1, -4};
   vector<int> input(begin(_input), end(_input));
   for (auto &v : s.threeSum(input)) {
      for (auto i : v)
         cout << i << " ";
      cout << endl;
   }
   return 0;
}
