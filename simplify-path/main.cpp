#include <iostream>
#include <algorithm>
#include <vector>
#include <stack>
#include <sstream>
using namespace std;

class Solution {
   public:
      string simplifyPath(string path) {
         if (path.empty())
            return path;
         stringstream res;
         string _path(path);
         replace( _path.begin(), _path.end(), '/', ' ' );
         stringstream ss(_path);
         vector<string> tokens; 
         for (string buf; ss >> buf;) {
            tokens.push_back(buf);
         }
         bool ignoreFirst = false;
         if (path[0] == '/') {
            if (tokens.empty())
               return "/";
         } 
         else {
            res << tokens[0];
            ignoreFirst = true;
         }
         stack<string> p, _p;

         for (int i = (int)ignoreFirst, size = tokens.size(); i < size; ++i) {
            if (tokens[i] == ".")
               continue;
            if (tokens[i] == "..") {
               for (int j = 0; j < 1 && !p.empty(); ++j)
                  p.pop();
               continue;
            }
            p.push(tokens[i]);
         }
         while (!p.empty()) {
            _p.push(p.top());
            p.pop();
         }
         while (!_p.empty()) {
            res << "/" << _p.top();
            _p.pop();
         }
         string S = res.str();
         return S.empty() ? "/" : S;
      }
};

int main() {
   Solution s;
   cout << s.simplifyPath("/home/") << ", expected /home" << endl;
   cout << s.simplifyPath("/a/./b/../../c/") << ", expected /c" << endl;
   cout << s.simplifyPath("/home//foo/") << ", expected /home/foo" << endl;
   return 0;
}
