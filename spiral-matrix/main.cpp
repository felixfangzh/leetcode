#include <iostream>
#include <utility>
#include <vector>
using namespace std;

class Solution {
   public:
      vector<int> spiralOrder(vector<vector<int> > &matrix) {
         vector<int> ret;
         if (matrix.empty()) return ret;
         pair<ssize_t, ssize_t> Y(0, matrix.size()-1), X(0, matrix[0].size()-1);
         while (true) {
            for (ssize_t i = X.first; i <= X.second; ++i)
               ret.push_back(matrix[Y.first][i]);
            if (++Y.first > Y.second) break;
            for (ssize_t i = Y.first; i <= Y.second; ++i)
               ret.push_back(matrix[i][X.second]);
            if (X.first > --X.second) break;
            for (ssize_t i = X.second; i >= X.first; --i)
               ret.push_back(matrix[Y.second][i]);
            if (Y.first > --Y.second) break;
            for (ssize_t i = Y.second; i >= Y.first; --i)
               ret.push_back(matrix[i][X.first]);
            if (++X.first > X.second) break;
         }
         return ret;
      }
};

#define ROW 2
#define COL 10
int main() {
   //int vals [][COL] = {
   //   { 1, 2, 3, 4},
   //   { 5, 6, 7, 8},
   //   { 9, 10, 11, 12 }
   //};
   int vals [][COL] = {
      {1,2,3,4,5,6,7,8,9,10},
      {11,12,13,14,15,16,17,18,19,20}
   };
   vector<vector<int> > matrix(ROW, vector<int>(COL));
   for (int i = 0; i < ROW; ++i) 
      for (int j = 0; j < COL; ++j) 
         matrix[i][j] = vals[i][j];
   Solution s;
   for (auto i : s.spiralOrder(matrix))
      cout << i << " ";
   cout << endl;
   return 0;
}
