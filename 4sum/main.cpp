#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution {
    public:
        vector<vector<int> > fourSum(vector<int> &num, int target) {
            vector<vector<int> > ret;
            sort(num.begin(), num.end());

            for (long i = 0, size = num.size(); i < size - 3; ++i) {
                if (i > 0 && num[i] == num[i-1]) continue;

                for (long j = i + 1; j < size - 2; ++j) {
                    if (j > i + 1 && num[j] == num[j-1]) continue;

                    long k = j + 1, l = size - 1;

                    while (k < l) {
                        int tmp[4] = {num[i], num[j], num[k], num[l]};
                        int sum = num[i] + num[j] + num[k] + num[l];

                        if (sum == target) {
                            ret.push_back(vector<int>(begin(tmp), end(tmp)));

                            for (; k < l - 1 && num[k] == num[k + 1]; ++k);
                            for (; l > k + 1 && num[l] == num[l - 1]; --l);
                            ++k; --l;
                        }

                        else if (sum < target) {
                            ++k;
                        }

                        else {
                            --l;
                        }
                    }
                }
            }

            return ret;
        }
};

int main() {
    Solution s;
    int _input [] = {1, 0, -1, 0, -2, 2};
    vector<int> input(begin(_input), end(_input));
    for (auto &v : s.fourSum(input, 0)) {
        for (auto i : v)
            cout << i << " ";
        cout << endl;
    }
    return 0;
}
