#include <vector>
#include <iostream>
#include <climits>
using namespace std;
/**
 * Definition for binary tree
 */
struct TreeNode {
   int val;
   TreeNode *left;
   TreeNode *right;
   TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
   public:
      vector<vector<int> > levelOrder(TreeNode *root) {
         vector<vector<int> > int_levels;
         if (!root) {
            return int_levels;
         }
         vector<vector<TreeNode*> > levels;
         vector<TreeNode*> level;
         level.push_back(root);
         levels.push_back(level);
         while (true) {
            vector<TreeNode*> _level;
            vector<TreeNode*> upperLevel = levels.back();
            for (auto iter = upperLevel.begin(); iter != upperLevel.end(); ++iter) {
               TreeNode* node = *iter;
               if (node->left) {
                  _level.push_back(node->left);
               }
               if (node->right) {
                  _level.push_back(node->right);
               }
            }
            if (_level.empty())
               break;
            levels.push_back(_level);
         }
         for (auto level = levels.begin(); level != levels.end(); ++level) {
            vector<int> _level;
            for (auto iter = level->begin(); iter != level->end(); ++iter) {
               _level.push_back((*iter)->val);
            }
            int_levels.push_back(_level);
         }
         return int_levels;
      }
};

#define SIZE 1

int main() {
   //int vals [] = {-9, -3, 2, INT_MIN, 4, 4, 0, INT_MIN, INT_MIN, -6, INT_MIN, -5};
   int vals [] = {1};
   TreeNode* nodes [SIZE];
   for (int i = 0; i < SIZE; ++i) {
      nodes[i] = vals[i] == INT_MIN ? NULL : new TreeNode(vals[i]);
   }
   for (int i = 0; i < SIZE; ++i) {
      if (!nodes[i])
         continue;
      if ((i+1)*2-1 < SIZE)
         nodes[i]->left = nodes[(i+1)*2-1];
      if ((i+1)*2 < SIZE)
         nodes[i]->right = nodes[(i+1)*2];
      cout << nodes[i]->val << ": (" 
         << (nodes[i]->left ? nodes[i]->left->val : INT_MIN)
         << ", "
         << (nodes[i]->right ? nodes[i]->right->val : INT_MIN)
         << "), " << endl;
   }
   Solution s;
   vector<vector<int> > levels = s.levelOrder(nodes[0]);
   for (auto level = levels.begin(); level != levels.end(); ++level) {
      cout << "[";
      for (auto iter = level->begin(); iter != level->end(); ++iter) {
         cout << *iter << ", ";
      }
      cout << "]" << endl;
   }
   return 0;
}
