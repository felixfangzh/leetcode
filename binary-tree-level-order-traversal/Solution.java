/**
 * Definition for binary tree
 */
public class Solution {
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) { val = x; }
    }

    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> levels = new ArrayList<List<Integer>>();

        if (null == root) return levels;

        int count = 1, count_ = 0;

        List<Integer> list = new ArrayList<Integer>();
        Deque<TreeNode> queue = new ArrayDeque<TreeNode>();
        queue.offer(root);

        while (!queue.isEmpty()) {
            TreeNode node = queue.poll();
            list.add(node.val);

            if (null != node.left) {
                ++count_;
                queue.offer(node.left);
            }
            if (null != node.right) {
                ++count_;
                queue.offer(node.right);
            }

            if (--count <= 0) {
                levels.add(list);
                list = new ArrayList<Integer>();
                count = count_;
                count_ = 0;
            }
        }

        if (!list.isEmpty())
            levels.add(list);

        return levels;
    }
}
