/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        if (headA == null || headB == null)
            return null;
            
        int countA = 0, countB = 0;
        ListNode tailA, tailB;
        for (tailA = headA; tailA.next != null; tailA = tailA.next)
            ++countA;
        for (tailB = headB; tailB.next != null; tailB = tailB.next)
            ++countB;
            
        if (tailA != tailB)
            return null;
            
        if (countA > countB)
            for (int i = 0; i < (countA - countB); ++i)
                headA = headA.next;
        else if (countA < countB)
            for (int i = 0; i < (countB - countA); ++i)
                headB = headB.next;
        
        while (headA != headB)
        {
            headA = headA.next;
            headB = headB.next;
        }
        
        return headA;
    }
}
