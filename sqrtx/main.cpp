#include <iostream>
#include <cmath>
using namespace std;

class Solution {
   public:
      int sqrt(int x) {
         if ( x < 2 )
            return x;
         double x0 = x/2;
         double x1;
         while (true) {
            x1 = (x0 + x/x0)/2.0;
            if (fabs(x1-x0) < 1) { return x1; }
            x0 = x1;
         }
      }
};

int main() {
   Solution s;
   cout << s.sqrt(4) << endl;
   return 0;
}
  
