class MyStack {
    Deque<Integer> queue = new ArrayDeque<Integer>();
    // Push element x onto stack.
    public void push(int x) {
        Deque<Integer> newQueue = new ArrayDeque<Integer>();
        newQueue.offer(x);
        while (!queue.isEmpty()) {
            newQueue.offer(queue.poll());
        }
        
        queue = newQueue;
    }

    // Removes the element on top of the stack.
    public void pop() {
        queue.pop();
    }

    // Get the top element.
    public int top() {
        return queue.peek();
    }

    // Return whether the stack is empty.
    public boolean empty() {
        return queue.isEmpty();
    }
}
