#include <iostream>
#include <vector>
using namespace std;

class Solution {
   public:
      vector<int> grayCode(int n) {
         vector<int> code;
         int max = 1 << n;
         for (int i = 0; i < max; ++i)
            code.push_back(i ^ (i >> 1));
         return code;
      }
};

int main() {
   Solution s;
   for (auto &i : s.grayCode(3))
      cout << i << " ";
   cout << endl;
   return 0;
}
