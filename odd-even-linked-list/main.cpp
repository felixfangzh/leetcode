#include <iostream>
#include <vector>
using namespace std;

struct ListNode {
   int val;
   ListNode *next;
   ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
   public:
      ListNode* oddEvenList(ListNode* head) {
         if (!head || !head->next) return head;
         ListNode* odd = head;       
         ListNode* even = odd->next;
         ListNode* evenHead = even;

         while (odd->next && even->next) {
            odd->next = even->next;
            odd = odd->next;
            even->next = odd->next;
            even = even->next;
         }

         odd->next = evenHead;

         return head;
      }
};

int main() {
   vector<ListNode> nodes(10, ListNode(0));
   for (size_t i = 0; i < nodes.size(); ++i) {
      nodes[i] = i + 1;
   }

   for (size_t i = 1; i < nodes.size(); ++i) {
      nodes[i-1].next = &nodes[i];
   }

   for(ListNode* node = &nodes[0]; node; node = node->next) {
      cout << node->val << " ";
   }

   cout << endl;

   Solution sol;

   for(ListNode* node = sol.oddEvenList(&nodes[0]); node; node = node->next) {
      cout << node->val << " ";
   }

   cout << endl;

   return 0;
}
