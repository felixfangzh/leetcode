class LRUCache:

    # @param capacity, an integer
    def __init__(self, capacity):
       self.capacity = capacity
       self.list = collections.deque()
       self.dict = {}


    # @return an integer
    def get(self, key):
       if key not in self.dict:
          return -1
       self.list.remove(key)
       self.list.append(key)
       return self.dict[key]


    # @param key, an integer
    # @param value, an integer
    # @return nothing
    def set(self, key, value):
       if self.capacity <= 0:
          return
       if key in self.dict:
          self.list.remove(key)
       elif len(self.list) >= self.capacity:
          del self.dict[self.list.popleft()]
       self.list.append(key)
       self.dict[key] = value
