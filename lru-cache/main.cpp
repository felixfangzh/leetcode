#include <iostream>
#include <list>
#include <unordered_map>

using namespace std;

class LRUCache {
    typedef list<pair<int, int>> value_list;
    typedef unordered_map<int, list<pair<int, int>>::iterator> list_iterator_map;

    private:
    int m_capacity;
    value_list m_value_list;
    list_iterator_map m_list_iterator_map;

    public:
    LRUCache(int capacity) {
        m_capacity = capacity;
        m_list_iterator_map.reserve(capacity);
    }

    int get(int key) {
        auto list_iterator_map_iter = m_list_iterator_map.find(key);
        if (list_iterator_map_iter == m_list_iterator_map.end()) return -1;

        int value = list_iterator_map_iter->second->second;
        m_value_list.erase(list_iterator_map_iter->second);
        m_list_iterator_map[key] = m_value_list.insert(m_value_list.end(), make_pair<int, int>(int(key), int(value)));

        return value;
    }

    void set(int key, int value) {
        if (m_capacity <= 0) return;

        auto list_iterator_map_iter = m_list_iterator_map.find(key);
        if (list_iterator_map_iter != m_list_iterator_map.end()) {
            m_value_list.erase(list_iterator_map_iter->second);
        }
        else {
            if (m_list_iterator_map.size() >= m_capacity) {
                auto value_list_iter = m_value_list.begin();
                m_list_iterator_map.erase(value_list_iter->first);
                m_value_list.erase(value_list_iter);
            }
        }

        m_list_iterator_map[key] = m_value_list.insert(m_value_list.end(), make_pair<int, int>(int(key), int(value)));
    }
};

int main() {
    LRUCache cache(1);
    cache.set(2, 1);
    cout << "Expected: 1, Actual: " << cache.get(2) << endl;
    cache.set(3, 2);
    cout << "Expected: 2, Actual: " << cache.get(3) << endl;
    cout << "Expected: -1, Actual: " << cache.get(2) << endl;
    cout << "Expected: 2, Actual: " << cache.get(3) << endl;
    return 0;
}
