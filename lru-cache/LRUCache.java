import java.util.*;

public class LRUCache {

   private int capacity;
   private Map<Integer, Integer> table;

   public LRUCache(int capacity) {
      this.capacity = capacity;
      this.table = new LinkedHashMap<Integer, Integer>(capacity);    
   }

   public int get(int key) {
      Integer value = table.get(key);
      if (null != value) {
         table.remove(key);
         table.put(key, value);
      }

      return value == null ? -1 : value;
   }

   public void set(int key, int value) {
      if (capacity <= 0) return;

      if (null != table.get(key))
         table.remove(key);
      else if (table.size() >= capacity)
         table.remove(table.keySet().iterator().next());

      table.put(key, value);
   }

   public static void main(String [] args) {
   }
}
