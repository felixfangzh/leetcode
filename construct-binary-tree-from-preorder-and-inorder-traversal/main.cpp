#include <iostream>
#include <vector>
#include <queue>
#include <climits>
#include <unordered_map>
using namespace std;

struct TreeNode {
   int val;
   TreeNode *left;
   TreeNode *right;
   TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

template <typename T>
void printVector(vector<T> &v) {
   for (T &i : v) 
      cout << i << " ";
   cout << endl;
}

bool isLeaveNode(TreeNode* root) {
   return root && (!root->left && !root->right);
}

void printTreeNode(TreeNode* root) {
   if (!root) {
      return;
   }
   cout << root->val << " ";
   if (isLeaveNode(root))
      return;
   queue<TreeNode*> q;
   q.push(root);
   while (!q.empty()) {
      TreeNode* node = q.front();
      q.pop();
      if (node->left) {
         cout << node->left->val << " ";
         if (!isLeaveNode(node->left))
            q.push(node->left);
      }
      else {
         cout << "# ";
      }
      if (node->right) {
         cout << node->right->val << " ";
         if (!isLeaveNode(node->right))
            q.push(node->right);
      }
      else {
         cout << "# ";
      }
   }
}

class Solution {
   private:
      unordered_map<int, int> inorder_ind;

      TreeNode *_buildTree(vector<int> &preorder, vector<int> &inorder, 
            int &preorder_iroot, int inorder_iroot, int inorder_left, int inorder_right) {
         TreeNode *root = new TreeNode(preorder[preorder_iroot]);
         if (inorder_left == inorder_right)
            return root;

         if (inorder_left < inorder_iroot) {
            ++preorder_iroot;
            root->left = _buildTree(preorder, inorder, 
                  preorder_iroot, inorder_ind[preorder[preorder_iroot]], inorder_left, inorder_iroot-1);
         }

         if (inorder_right > inorder_iroot) {
            ++preorder_iroot;
            root->right = _buildTree(preorder, inorder, 
                  preorder_iroot, inorder_ind[preorder[preorder_iroot]], inorder_iroot+1, inorder_right);
         }

         return root;
      }

   public:
      TreeNode *buildTree(vector<int> &preorder, vector<int> &inorder) {
         if (preorder.empty() || inorder.empty() || preorder.size() != inorder.size())
            return NULL;

         inorder_ind.clear();
         for (int j = 0, size = inorder.size(); j < size; ++j) {
            inorder_ind[inorder[j]] = j;
         }

         int preorder_iroot = 0;
         return _buildTree(preorder, inorder, preorder_iroot, inorder_ind[preorder[0]], 0, inorder.size()-1);
      }
};

int main() {
   Solution s;
   int pre [] = {2, 1, 3, 4};
   int in [] = {1, 2, 3, 4};
   vector<int> preorder(begin(pre), end(pre));
   vector<int> inorder(begin(in), end(in));
   printTreeNode(s.buildTree(preorder, inorder));
   return 0;
}
