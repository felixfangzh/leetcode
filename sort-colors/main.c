#include <stdio.h>
#include <string.h>

void sortColors(int A[], int n) {
    if (n <= 1) return;

    size_t counts[3] = {0}, i, k;

    for (i = 0; i < n; ++i)
        ++counts[A[i]];

    for (i = 0, k = counts[0]; i < k; ++i)
        A[i] = 0;

    for (k += counts[1]; i < k; ++i)
        A[i] = 1;

    for (k += counts[2]; i < k; ++i)
        A[i] = 2;
}

int main() {
    int A [] = {1, 1};
    int n = sizeof(A)/sizeof(A[0]);
    sortColors(A, n);
    size_t i;
    for (i = 0; i < n; ++i)
        printf("%d ", A[i]);
    printf("\b\n");

    return 0;
}
