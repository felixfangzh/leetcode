#include <iostream>
using namespace std;

class Solution {
   public:
      void sortColors(int A[], int n) {
         int color_count[3] = {0};
         for (int i = 0; i < n; ++i)
            ++color_count[A[i]];
         for (int i = 0, k = 0; i < 3; ++i)
            for (int j = 0; j < color_count[i]; ++j)
               A[k++] = i;
      }
};

int main() {
   int vals [] = {2, 1, 0, 1, 0, 2};
   Solution s;
   s.sortColors(vals, distance(begin(vals), end(vals)));
   for (auto i = begin(vals); i != end(vals); ++i)
      cout << *i << " ";
   cout << endl;
   return 0;
}
