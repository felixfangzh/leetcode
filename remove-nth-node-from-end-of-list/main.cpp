#include <iostream>
#include <vector>
using namespace std;

struct ListNode {
   int val;
   ListNode *next;
   ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
   public:
      ListNode *removeNthFromEnd(ListNode *head, int n) {
         if (n <= 0)
            return head;
         ListNode *slow = NULL, *fast = head;
         for (int i = 1; i < n && fast; ++i)
            fast = fast->next;
         if (!fast)
            return head;
         while (fast->next) {
            slow = slow ? slow->next : head;
            fast = fast->next;
         }
         
         if (!slow) {
            slow = head;
            head = head->next;
            delete slow;
         }
         else if (slow->next){
            fast = slow->next;
            slow->next = slow->next->next;
            delete fast;
         }
         return head; 
      }
};

int main() {
   int vals [] = {1, 2, 3, 4, 5};
   vector<ListNode*> nodes;
   for (auto i : vals)
      nodes.push_back(new ListNode(i));
   for (int i = 1; i < nodes.size(); ++i)
      nodes[i-1]->next = nodes[i];
   Solution s;
   cout << "head->";
   for (ListNode* node = s.removeNthFromEnd(nodes[0], 5); node; node = node->next)
      cout << node->val << "->";
   cout << "tail" << endl;
   return 0;
}
