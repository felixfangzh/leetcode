#include <iostream>
#include <vector>
#include <limits.h>

using namespace std;

class Solution {
   public:
      int findPeakElement(const vector<int> &num) {
         auto getElement = [&](int i) {
            if (i < 0 || i >= num.size()) return INT_MIN;
            return num[i];
         };

         int low = 0, high = num.size() - 1;
         while(low < high) {
            int mid = low + ((high - low) >> 1);
            if (getElement(mid) < getElement(mid - 1))
               high = mid - 1;
            else if (getElement(mid) < getElement(mid + 1))
               low = mid + 1;
            else
               return mid;
         }

         return low;
      }
};

int main() {
   Solution sol;
   return 0;
}
