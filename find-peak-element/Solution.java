public class Solution {
    private int getElement(int [] num, int i) {
        if (i < 0 || i >= num.length) return Integer.MIN_VALUE;
        return num[i];
    }
    public int findPeakElement(int[] num) {
        int low = 0, high = num.length;
        while (low < high) {
            int mid = low + ((high - low) >> 1);
            if (getElement(num, mid) < getElement(num, mid - 1))
                high = mid - 1;
            else if (getElement(num, mid) < getElement(num, mid + 1))
                low = mid + 1;
            else
                return mid;
        }
        
        return low;
    }
    public static void main(String [] args) {
       Solution sol = new Solution();
    }
}
