#include <iostream>
#include <vector>

using namespace std;

class Solution {
   public:
      vector<int> getRow(int rowIndex) {
         if (rowIndex < 0)
            return vector<int>();
         vector<int> row(1, 1);
         for (int i = 0; i < rowIndex; ++i) {
            for (int j = row.size() - 1; j > 0; --j) {
               row[j] += row[j-1];
            }
            row.push_back(1);
         }
         return row;
      }
};

int main() {
   Solution s;
   return 0;
}
