#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

int ** combinatorials = NULL;

int choose(int n, int k) {
    if (combinatorials[n][k]) return combinatorials[n][k];

    if (k == 0 || k == n) combinatorials[n][k] = 1;
    else if (k == 1 || k == n-1) combinatorials[n][k] = n;
    else combinatorials[n][k] = choose(n-1, k-1) + choose(n-1, k);

    return combinatorials[n][k]; 
}

int *getRow(int rowIndex) {
    int * row = malloc(sizeof(int)*(rowIndex+1));
    combinatorials = malloc(sizeof(int*)*(rowIndex+1));

    int i;
    for (i = 0; i <= rowIndex; ++i)
        combinatorials[i] = calloc(rowIndex+1, sizeof(int));

    for (i = 0; i <= rowIndex; ++i)
        row[i] = choose(rowIndex, i);

    for (i = 0; i <= rowIndex; ++i)
        free(combinatorials[i]);
    free(combinatorials);

    return row;
}

void printArray(int row[], int n) {
    int i;
    for (i = 0; i < n; ++i) {
        printf("%d ", row[i]);
    }
    printf("\b\r\n");
}

void test(int rowIndex) {
    printArray(getRow(rowIndex), rowIndex+1);
}

int main() {
    int i;
    for (i = 0; i < 5; ++i)
        test(i);
    return 0;
}
