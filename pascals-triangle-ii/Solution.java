import java.util.*;

public class Solution {
    private static int [][] combinatorials = null;

    private static int choose(int n, int k) {
        if (combinatorials[n][k] != 0) return combinatorials[n][k];

        if (k == 0 || k == n) combinatorials[n][k] = 1; 
        else if (k == 1 || k == n - 1) combinatorials[n][k] = n;
        else combinatorials[n][k] = choose(n-1, k-1) + choose(n-1, k);

        return combinatorials[n][k];
    }

    public List<Integer> getRow(int rowIndex) {
        List<Integer> row = new ArrayList<Integer>();

        combinatorials = new int [rowIndex+1][rowIndex+1];

        for (int k = 0; k <= rowIndex; ++k)
            row.add(choose(rowIndex, k));
        return row;
    }

    public static void printList(List<Integer> list) {
        for (Integer i : list)
            System.out.print(i + " ");
        System.out.println("\b");
    }

    public static void test(int rowIndex) {
        printList(new Solution().getRow(rowIndex));
    }

    public static void main(String[] args) {
        for (int i = 0; i < 22; ++i)
            test(i);
    }
}
