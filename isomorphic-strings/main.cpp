#include <iostream>
#include <vector>
using namespace std;

class Solution {
   public:
      bool isIsomorphic(string s, string t) {
         if (s.size() != t.size()) return false;

         size_t counter = 0;
         vector<size_t> map1(127, 0);
         vector<size_t> map2(127, 0);

         for(size_t i = 0; i < s.size(); ++i) {
            if (map1[s[i]] != map2[t[i]]) return false;

            if (!map1[s[i]]) {
               map1[s[i]] = map2[t[i]] = ++counter;
            }
         }

         return true;
      }
};
