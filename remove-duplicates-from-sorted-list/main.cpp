#include <iostream>
#include <vector>
using namespace std;

struct ListNode {
   int val;
   ListNode *next;
   ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
   public:
      ListNode *deleteDuplicates(ListNode *head) {
         for (ListNode* current = head; current;) {
            if (current->next && current->val == current->next->val) {
               ListNode* next_next = current->next->next;
               delete current->next;
               current->next = next_next;
            }
            else {
               current = current->next;
            }
         }
         return head;
      }
};

int main() {
   int vals [] = {1, 1, 2, 3, 3};
   vector<ListNode*> nodes;
   for (auto i = begin(vals); i != end(vals); ++i) {
      nodes.push_back(new ListNode(*i));
   }
   for (int i = 1; i < nodes.size(); ++i) {
      nodes[i-1]->next = nodes[i];
   }
   Solution s;
   for (ListNode* node = s.deleteDuplicates(nodes[0]); node; node = node->next)
      cout << node->val << " ";
   cout << endl;
   return 0;
}
