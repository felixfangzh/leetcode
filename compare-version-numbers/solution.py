import math

class Solution:
    # @param version1, a string
    # @param version2, a string
    # @return an integer
    def getList(self, version):
        l = map(int, version.split("."))
        while len(l) > 0 and l[-1] == 0:
           l.pop()
        return l

    def compareVersion(self, version1, version2):
        l1 = self.getList(version1)
        l2 = self.getList(version2)

        len1 = len(l1)
        len2 = len(l2)

        for i in range(min(len1, len2)):
            if l1[i] != l2[i]:
                return int(math.copysign(1, l1[i]-l2[i]))
        if len1 != len2:
            return int(math.copysign(1, len1-len2))
        return 0

sol = Solution()
print sol.compareVersion("1.2.0", "1.2")
print sol.compareVersion("0", "1.2")
