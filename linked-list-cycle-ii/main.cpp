/**
 * Definition for singly-linked list.
 */
#include <iostream>
using namespace std;

struct ListNode {
   int val;
   ListNode *next;
   ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
   public:
      ListNode *detectCycle(ListNode *head) {
         ListNode *slowNode, *fastNode;
         slowNode = fastNode = head;
         while (slowNode && fastNode && (slowNode = slowNode->next) && (fastNode = fastNode->next) && (fastNode = fastNode->next)){
            if (slowNode == fastNode) {
               slowNode = head;
               for (slowNode = head; 
                     slowNode != fastNode && (slowNode = slowNode->next) && (fastNode = fastNode->next);) {}
               return slowNode;
            }
         }
         return NULL;
      }
};

#define SIZE 28

int main() {
   ListNode* nodes[SIZE];
   int vals[] = {-21,10,17,8,4,26,5,35,33,-7,-16,27,-12,6,29,-12,5,9,20,14,14,2,13,-24,21,23,-21,5};
   for (int i = 0; i < SIZE; ++i) {
      nodes[i] = new ListNode(vals[i]);
      cout << vals[i] << " ";
   }
   cout << endl;
   for (int i = 0; i < SIZE-1; ++i) {
      nodes[i]->next = nodes[i+1]; 
   }
   nodes[SIZE-1]->next = nodes[24];
   cout << nodes[24] << ": " << nodes[24]->val << endl;

   Solution s;
   ListNode* n = s.detectCycle(nodes[0]);
   if (n)
      cout << n->val << endl;
   else
      cout << "NULL" << endl;
   return 0;
}
