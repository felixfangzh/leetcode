#include <iostream>
#include <vector>
#include <unordered_set>
#include <unordered_map>
using namespace std;

class Solution {
   private:
      unordered_map<char, vector<string> > word_map;
   public:
      vector<string> _wordBreak(string s, unordered_set<string> &dict) {
         vector<string> sen;
         if (s.empty() || dict.empty())
            return sen;

         auto iter = word_map.find(s[0]);
         if (iter == word_map.end())
            return sen;

         for (auto &S : iter->second) {
            if (!s.find(S) && dict.count(S)) {
               if (s.size() == S.size()) {
                  sen.push_back(S);
               }
               else if (s.size() > S.size()){
                  vector<string> _sen = _wordBreak(s.substr(S.size()), dict);
                  for (auto &str : _sen)
                     sen.push_back(S + " " + str);
               }
            }
         }
         return sen;
      }
      vector<string> wordBreak(string s, unordered_set<string> &dict) {
         vector<string> sen;
         if (s.empty() || dict.empty())
            return sen;
         word_map.clear();
         bool ending_char = false;
         for (auto &S : dict) {
            word_map[S.front()].push_back(S);
            if (S.back() == s.back())
               ending_char = true;
         }
         if (!ending_char)
            return sen;
         return _wordBreak(s, dict);
      }

};

int main() {
   Solution s;
   string words [] = {"a","b","bbb","bbbb"};
   unordered_set<string> dict;
   for (auto &i : words) {
      dict.insert(i);
   }
   for (auto &S : s.wordBreak("bb", dict))
      cout << S << endl;
   return 0;
}
