#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

struct TreeNode {
   int val;
   TreeNode *left;
   TreeNode *right;
   TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
   public:
      vector<vector<int> > levelOrderBottom(TreeNode *root) {
         vector<vector<int> > levels;
         traverse(root, 0, levels);
         reverse(levels.begin(), levels.end());
         return levels;
      }
      
      void traverse(TreeNode *root, int level, vector<vector<int> > &levels) {
         if (!root)
            return;
         if (levels.size() <= level)
            levels.push_back(vector<int>());
         levels[level].push_back(root->val);
         traverse(root->left, level+1, levels);
         traverse(root->right, level+1, levels);
      }
};

int main() {
   Solution s;
   return 0;
}
