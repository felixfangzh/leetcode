#include <iostream>
using namespace std;

class Solution {
   public:
      string minWindow(string S, string T) {
         string ret;
         size_t hasFound[256] = {0};
         size_t needToFind[256] = {0};
         for (auto &c : T)
            ++needToFind[c];
         for (size_t i(0), j(0), count(0), size(S.size()); j < size; ++j) {
            if (++hasFound[S[j]] <= needToFind[S[j]]) {
               ++count;
            }
            if (count == T.size()) {
               for(; hasFound[S[i]] > needToFind[S[i]]; --hasFound[S[i]], ++i);
               if (ret.empty() || j - i + 1 < ret.size())
                  ret = S.substr(i, j-i+1);
            }
         }
         return ret;
      }
};

int main() {
   Solution s;
   cout << s.minWindow("ADOBECODEBANC", "ABC") << ", expected BANC" << endl;
   return 0;
}
