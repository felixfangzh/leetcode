#include <stdio.h>
#include <stdlib.h>
/**
 * Definition for binary tree
 */
struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

int maxDepth(struct TreeNode *root) {
    int left, right;
    return !root ? 0 : 1 + ((left = maxDepth(root->left)) > (right = maxDepth(root->right)) ? left : right);
}

int main() {
    printf("%d\n", maxDepth(malloc(sizeof(struct TreeNode))));
}
