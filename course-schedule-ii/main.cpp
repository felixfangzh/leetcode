#include <vector>
#include <unordered_map>

using namespace std;

class Solution {
    public:
        vector<int> findOrder(int numCourses, vector<pair<int, int>>& prerequisites) {
            vector<int> order;
            order.reserve(numCourses);

            unordered_map<int, vector<int>> graph;
            for(int i = 0; i < numCourses; ++i) {
                graph[i] = vector<int>();
            }

            for(const pair<int, int>& p : prerequisites) {
                graph[p.second].push_back(p.first);
            }

            unordered_map<int, int> inDegrees;
            for(int i = 0; i < numCourses; ++i) {
                inDegrees[i] = 0;
            }

            for(auto& vertex : graph) {
                for(int neighbor : vertex.second) {
                    ++inDegrees[neighbor];
                }
            }

            while(!inDegrees.empty()) {
                bool foundVertexWithZeroInDegrees = false;
                for(auto iterator = inDegrees.begin(); iterator != inDegrees.end();) {
                    if (iterator->second == 0)
                    {
                        for(int neighbor : graph[iterator->first]) {
                            --inDegrees[neighbor];
                        }

                        order.push_back(iterator->first);
                        iterator = inDegrees.erase(iterator);
                        foundVertexWithZeroInDegrees = true;
                    }
                    else {
                        ++iterator;
                    }
                }

                if(!foundVertexWithZeroInDegrees) {
                    return vector<int>();
                }
            }

            return order;
        }
};

int main() {
    return 0;
}
