import java.util.*;

public class Solution {
    /**
     * Definition for singly-linked list.
     */
    static public class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {
            val = x;
            next = null;
        }
    }

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode head = null, current = null;

        int carry = 0;

        while (null != l1 || null != l2) {
            ListNode node = null;
            if (null != l1 && null != l2) {
                node = new ListNode(l1.val + l2.val + carry);
                l1 = l1.next;
                l2 = l2.next;
            }
            else if (null != l1) {
                node = new ListNode(l1.val + carry);
                l1 = l1.next;
            }
            else {
                node = new ListNode(l2.val + carry);
                l2 = l2.next;
            }

            carry = node.val/10;
            node.val %= 10;

            if (null == current) {
                current = node;
                head = current;
            }
            else {
                current.next = node;
                current = current.next;
            }
        }

        if (carry > 0) {
            current.next = new ListNode(carry);
        }

        return head;
    }

    public static ListNode toListNode(int [] a) {
        ListNode current = null;
        ListNode head = null;
        for (int i = 0; i < a.length; ++i) {
            if (current == null) {
                current = new ListNode(a[i]);
                head = current;
            }
            else {
                current.next = new ListNode(a[i]);
                current = current.next;
            }
        }

        return head;
    }

    public static void test(int [] a1, int [] a2) {
        for (ListNode current = new Solution().addTwoNumbers(toListNode(a1), toListNode(a2)), head = current; current != null; current = current.next) {
            if (current != head)
                System.out.print("->");
            System.out.print(current.val);
        }
        System.out.println();
    }

    public static void main(String [] args) {
        test(new int[] {0}, new int [] {0});
        test(new int[] {5}, new int [] {5});
    }
}

