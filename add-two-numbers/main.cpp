/**
 * Definition for singly-linked list.
 */
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
    public:
        ListNode *addTwoNumbers(ListNode *l1, ListNode *l2) {
            ListNode *head = NULL, *current = NULL;

            int carry = 0;

            while (l1 || l2) {
                int val;
                if (l1 && l2) {
                    val = l1->val + l2->val + carry;
                    l1 = l1->next;
                    l2 = l2->next;
                }
                else if (l1) {
                    val = l1->val + carry;
                    l1 = l1->next;
                }
                else {
                    val = l2->val + carry;
                    l2 = l2->next;
                }

                carry = val / 10;
                val %= 10;

                if (!current) {
                    current = new ListNode(val);
                    head = current;
                }
                else {
                    current->next = new ListNode(val);
                    current = current->next;
                }
            }

            if (carry > 0) {
                current->next = new ListNode(carry);
            }

            return head;
        }
};
