import java.util.*;

public class Solution {
    private void combinationSum(int[] candidates, int target, int index, List<Integer> list, List<List<Integer>> lists) {
        if (target == 0) {
            lists.add(new ArrayList(list));
            return;
        }

        for (int i = index; i < candidates.length; ++i) {
            if (candidates[i] > target) break;
            if (i > 0 && candidates[i] == candidates[i-1]) continue;

            list.add(candidates[i]);
            combinationSum(candidates, target - candidates[i], i, list, lists);
            list.remove(list.size()-1);
        }
    }

    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        Arrays.sort(candidates);
        List<List<Integer>> lists = new ArrayList<List<Integer>>();
        combinationSum(candidates, target, 0, new ArrayList<Integer>(), lists);
        return lists;
    }

    public static void main(String [] args) {
    }
}
