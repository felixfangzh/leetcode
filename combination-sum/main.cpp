#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution {
   private:
      void combinationSum(vector<int> &candidates, int start, int currentSum, int target, vector<int> &comb, vector<vector<int> > &combs) {
         if (currentSum == target) {
            combs.push_back(comb);
            return;
         }
         if (currentSum > target)
            return;
         for (int i = start, size = candidates.size(); i < size; ++i) {
            comb.push_back(candidates[i]);
            combinationSum(candidates, i, currentSum+candidates[i], target, comb, combs);
            comb.pop_back();
         }
      }

   public:
      vector<vector<int> > combinationSum(vector<int> &candidates, int target) {
         vector<vector<int> > combs;
         vector<int> comb;
         sort(candidates.begin(), candidates.end());
         combinationSum(candidates, 0, 0, target, comb, combs);
         return combs;
      }
};

int main() {
   Solution s;
   int vals [] = {2,3,6,7};
   vector<int> candidates(begin(vals), end(vals));
   for (auto &v : s.combinationSum(candidates, 7)) {
      for (auto &i : v)
         cout << i << " ";
      cout << endl;
   }
   return 0;
}
