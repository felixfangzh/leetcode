#include <iostream>
#include <vector>
using namespace std;

class Solution {
   public:
      vector<int> searchRange(int A[], int n, int target) {
         vector<int> range(2, -1);
         if (!n)
            return range;
         int lower, upper;
         lower = 0;
         upper = n;
         while (lower < upper) {
            int middle = (lower + upper) / 2;
            if (A[middle] < target)
               lower = middle + 1;
            else
               upper = middle;
         }

         if (A[lower] != target) return range;
         range[0] = lower;

         upper = n;
         while (lower < upper) {
            int middle = (lower + upper) / 2;
            if (A[middle] > target)
               upper = middle;
            else
               lower = middle + 1;
         }
         range[1] = upper - 1;
         return range;
      }
};

int main() {
   Solution s;
   int A [] = {5, 7, 7, 8, 8, 10};
   for (auto &i : s.searchRange(A, 6, 8))
      cout << i << " ";
   cout << endl;
   return 0;
}
