#include <iostream>
#include <vector>
using namespace std;

struct ListNode {
   int val;
   ListNode *next;
   ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
   private:
      size_t size(ListNode* head) {
         size_t i = 0;
         for (; head; ++i, head = head->next);
         return i;
      }
   public:
      ListNode *rotateRight(ListNode *head, int k) {
         if (!head || k <= 0)
            return head;

         k = k % size(head);
         ListNode *slow = head, *fast = head;
         int i;
         for (i = 0; i < k && fast->next; ++i, fast = fast->next);
         if (i < k)
            return head;
         while (fast->next) {
            fast = fast->next;
            slow = slow->next;
         }

         fast->next = head;
         head = slow->next;
         slow->next = NULL;
         return head;
      }
};

int main() {
   int vals [] = {1, 2, 3};
   vector<ListNode*> nodes;
   for (auto i : vals)
      nodes.push_back(new ListNode(i));
   for (int i = 0, size = nodes.size()-1; i < size; ++i)
      nodes[i]->next = nodes[i+1];
   Solution s;
   for (ListNode* ptr = s.rotateRight(nodes[0], 4); ptr; ptr = ptr->next)
      cout << ptr->val << " ";
   cout << endl;
   return 0;
}
