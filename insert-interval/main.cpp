#include <vector>
#include <algorithm> 
#include <iostream> 
using namespace std;

struct Interval {
   int start;
   int end;
   Interval() : start(0), end(0) {}
   Interval(int s, int e) : start(s), end(e) {}
};

bool compareIntervals(Interval i1, Interval i2) {
   return i1.start < i2.start;
}

class Solution {
   private:
      bool isOverlapped(Interval i1, Interval i2) {
         return i1.start <= i2.end && i2.start <= i1.end;
      }

      Interval mergeTwoIntervals(Interval i1, Interval i2) {
         i1.start = min(i1.start, i2.start);
         i1.end = max(i1.end, i2.end);
         return i1;
      }

      vector<Interval> merge(vector<Interval> &intervals) {
         if (intervals.size() < 2)
            return intervals;
         vector<Interval> newIntervals;
         newIntervals.reserve(intervals.size());

         Interval current = intervals[0];
         for (auto iter = intervals.begin()+1; iter != intervals.end(); ++iter) {
            if (isOverlapped(current, *iter)) {
               current = mergeTwoIntervals(current, *iter);
            }
            else {
               newIntervals.push_back(current);
               current = *iter;
            }
         }
         newIntervals.push_back(current);
         return newIntervals;
      }
   public:
      vector<Interval> insert(vector<Interval> &intervals, Interval newInterval) {
         int start = 0;
         int end = intervals.size();
         while (start < end) {
            int middle = (start + end) / 2;
            if (intervals[middle].start < newInterval.start)
               start = middle + 1;
            else
               end = middle;
         }
         intervals.insert(next(begin(intervals), start), newInterval);
         return merge(intervals);
      }
};

int main() {
   vector<Interval> intervals;
   Interval i(4, 9);
   Interval i1(1, 2);
   Interval i2(3, 5);
   Interval i3(6, 7);
   Interval i4(8, 10);
   Interval i5(12, 16);
   intervals.push_back(i1);
   intervals.push_back(i2);
   intervals.push_back(i3);
   intervals.push_back(i4);
   intervals.push_back(i5);
   Solution s;
   vector<Interval> newIntervals = s.insert(intervals, i);
   for (auto iter = newIntervals.begin(); iter != newIntervals.end(); ++iter) {
      cout << "[" << iter->start << ", " << iter->end << "], ";
   }
   cout << endl;
   return 0;
}
