/**
 * Definition for singly-linked list.
 */
#include <vector>
using namespace std;
#define NULL 0
struct ListNode {
   int val;
   ListNode *next;
   ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
   private:
      ListNode *mergeTwoLists(ListNode *l1, ListNode *l2) {
         if (!l1)
            return l2;
         if (!l2)
            return l1;
         ListNode *head, *tail, *current;
         head = tail = current = NULL;
         while (l1 || l2) {
            if (l1 && l2) {
               if (l1->val < l2->val) {
                  current = l1;
                  l1 = l1->next;
               }
               else {
                  current = l2;
                  l2 = l2->next;
               }
            }
            else if (l1) {
               current = l1;
               l1 = l1->next;
            }
            else if (l2) {
               current = l2;
               l2 = l2->next;
            }
            if (!head)
               head = tail = current;
            else {
               tail->next = current;
               tail = tail->next;
            }
         }
         return head;
      }

      ListNode *mergeSubLists(vector<ListNode *> &lists, size_t start, size_t end) {
          long int diff = end - start;
          if (diff <= 0) return NULL;
          if (diff == 1) return lists[start];

          size_t mid = start + diff / 2;
          return mergeTwoLists(mergeSubLists(lists, start, mid), mergeSubLists(lists, mid, end));
      }
   public:
      ListNode *mergeKLists(vector<ListNode *> &lists) {
          return mergeSubLists(lists, 0, lists.size());
      }
};

int main() {
   return 0;
}
