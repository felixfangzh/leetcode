#include <stdio.h>
/**
 * Definition for singly-linked list.
 */
struct ListNode {
    int val;
    struct ListNode *next;
};

struct ListNode *mergeTwoLists(struct ListNode *node1, struct ListNode *node2) {
    if (!node2) return node1;
    if (!node1) return node2;

    if (node1->val > node2->val) {
        struct ListNode *tmp = node1;
        node1 = node2;
        node2 = tmp;
    }

    struct ListNode *head = node1;

    while (node2) {
        for (; node1->next && node1->next->val <= node2->val; node1 = node1->next);
        struct ListNode *tmp1 = node1->next, *tmp2 = node2->next;
        node1->next = node2;
        node2->next = tmp1;
        node2 = tmp2;
    }

    return head;
}

struct ListNode *mergeSubLists(struct ListNode *lists[], int start, int end) {
    int diff = end - start;
    if (diff <= 0) return NULL;
    if (diff == 1) return lists[start];

    int mid = start + diff / 2;
    return mergeTwoLists(mergeSubLists(lists, start, mid), mergeSubLists(lists, mid, end));
}

struct ListNode *mergeKLists(struct ListNode *lists[], int k) {
    return mergeSubLists(lists, 0, k);
}

int main() {
    return 0;
}
