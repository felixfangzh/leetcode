import java.util.*;

public class Solution {
    /**
     * Definition for singly-linked list.
     */
    static public class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {
            val = x;
            next = null;
        }
    }

    public ListNode mergeTwoLists(ListNode node1, ListNode node2) {
        if (null == node2) return node1;
        if (null == node1) return node2;

        if (node1.val > node2.val) {
            ListNode tmp = node1;
            node1 = node2;
            node2 = tmp;
        }

        ListNode head = node1;

        while (node2 != null) {
            for (; node1.next != null && node1.next.val <= node2.val; node1 = node1.next);
            ListNode tmp1 = node1.next, tmp2 = node2.next;
            node1.next = node2;
            node2.next = tmp1;
            node2 = tmp2;
        }

        return head;
    }

    public ListNode mergeSubLists(ListNode [] lists, int start, int end) {
        int diff = end - start;
        if (diff <= 0) return null;
        if (diff == 1) return lists[start];

        int mid = start + diff/2;
        return mergeTwoLists(mergeSubLists(lists, start, mid), mergeSubLists(lists, mid, end));
    }

    public ListNode mergeKLists(List<ListNode> lists) {
        return mergeSubLists(lists.toArray(new ListNode[0]), 0, lists.size());
    }

    public static ListNode getListNode(int [] array) {
        if (array.length <= 0) return null;
        ListNode [] nodes = new ListNode [array.length];
        for (int i = 0; i < nodes.length; ++i) {
            nodes[i] = new ListNode(array[i]);
        }
        for (int i = 1; i < nodes.length; ++i) {
            nodes[i-1].next = nodes[i];
        }

        return nodes[0];
    }

    public static void main(String [] args) {
        List<ListNode> lists = new ArrayList<ListNode>();
        //lists.add(getListNode(new int [] {-1, -1, -1}));
        //lists.add(getListNode(new int [] {-2, -2, -1}));
        lists.add(null);
        lists.add(null);

        Solution sol = new Solution();
        for (ListNode node = sol.mergeKLists(lists); node != null; node = node.next) {
            System.out.print(node.val + " ");
        }
        System.out.println("\b");
    }
}
