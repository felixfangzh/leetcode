import java.util.*;

public class Solution {
    public int[] twoSum(int[] numbers, int target) {
        int [] res = new int[2];
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();

        for (int i = 0; i < numbers.length; ++i) {
            Integer j = map.get(target - numbers[i]);
            if (null != j) {
                res[0] = j + 1;
                res[1] = i + 1;
            }

            map.put(numbers[i], i);
        }

        return res;
    }
}
