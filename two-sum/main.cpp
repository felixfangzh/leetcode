#include <iostream>
#include <algorithm>
#include <vector>
#include <unordered_map>
using namespace std;

class Solution {
   public:
      vector<int> twoSum(vector<int> &numbers, int target) {
         vector<int> res;

         unordered_map<int, int> nums;

         for (size_t i = 0; i < numbers.size(); ++i) {
             auto iter = nums.find(target - numbers[i]);
             if (iter != nums.end()) {
                 res.push_back(iter->second+1);
                 res.push_back(i+1);
             }

             nums[numbers[i]] = i;
         }

         return res;
      }
};

int main() {
   Solution s;
   int vals [] = {0, 1, 4, 0};
   vector<int> numbers(begin(vals), end(vals));
   for (auto &i : s.twoSum(numbers, 0))
      cout << i << " ";
   cout << endl;
   return 0;
}
