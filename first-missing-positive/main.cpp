#include <iostream>

using namespace std;

class Solution {
    public:
        int firstMissingPositive(int A[], int n) {
            for (size_t i = 0; i < n; ++i)
                while (A[i] > 0 && A[i] <= n && A[i] != A[A[i] - 1])
                    swap(A[i], A[A[i] - 1]);
            for (size_t i = 0; i < n; ++i)
                if (A[i] != i + 1)
                    return i + 1;
            return n+1;
        }
};

template <size_t n>
void test(int (&A)[n], int answer) {
    int result = Solution().firstMissingPositive(A, n);

    if (answer != result)
        cout << "FAILED: Expected: " << answer << ", Actual: " << result << endl;
    else
        cout << "PASSED: Result: " << result << endl;
}

int main() {
    int A1[] = {1};
    test(A1, 2);
    int A2[] = {1,1};
    test(A2, 2);
    int A3[] = {0};
    test(A3, 1);
}
