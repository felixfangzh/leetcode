#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_map>
using namespace std;

class Solution {
   public:
      vector<string> anagrams(vector<string> &strs) {
         vector<string> ret;
         unordered_map<string, vector<size_t>> str_map;
         for (size_t i = 0; i < strs.size(); ++i) {
            string key = strs[i];
            sort(begin(key), end(key));
            str_map[key].push_back(i);
         }
         for (auto &p : str_map)
            if (p.second.size() > 1)
                for (size_t i : p.second) {
                    ret.push_back(strs[i]);
                }
         return ret;
      }
};

int main() {
    Solution s;
    string input [] = {"apple", "ppale", "paple", "nono", "onon", "hello", "hell"};
    vector<string> strs(begin(input), end(input));
    for (auto &str : s.anagrams(strs))
        cout << str << " ";
    cout << endl;
}
