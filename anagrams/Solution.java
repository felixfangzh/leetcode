import java.util.*;

public class Solution {
    public List<String> anagrams(String[] strs) {
        List<String> list = new ArrayList<String>();
        Map<String, List<String>> map = new HashMap<String, List<String>>();

        for (int i = 0; i < strs.length; ++i) {
            char [] array = strs[i].toCharArray();
            Arrays.sort(array);
            String key = new String(array);
            List<String> l = map.get(key);
            if (null == l) {
                l = new ArrayList<String>();
                map.put(key, list);
            }

            l.add(strs[i]);
        }

        for (List<String> l : map.values())
            if (l.size() > 1)
                list.addAll(l);

        return list;
    }

    public static void test(String [] strs) {
        for (String s : new Solution().anagrams(strs))
            System.out.print("\"" + s + "\" ");
        System.out.println("\b");
    }

    public static void main(String [] args) {
        test(new String [] {""});
    }
}
