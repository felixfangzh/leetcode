public class Solution {
   public int trailingZeroes(int n) {
      int count = 0;
      for (long divisor = 5, quotient; (quotient = n/divisor) >= 1; divisor *= 5)
         count += quotient;

      return count;
   }
}
