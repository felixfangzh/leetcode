#include <iostream>

using namespace std;

class Solution {
   public:
      int trailingZeroes(int n) {
         int count = 0;
         for (unsigned long int divisor = 5, quotient; (quotient = n/divisor) >= 1; divisor *= 5)
         {
            cout << "n: " << n << endl;
            cout << "divisor: " << divisor << endl;
            cout << "quotient: " << quotient << endl;
            count += quotient;
            cout << endl;
         }

         return count;

      }
};

int main() {
   Solution solution;
   cout << "Output: 1808548329, Expect: 452137076, Actual: " << solution.trailingZeroes(1808548329) << endl;
   return 0;
}
