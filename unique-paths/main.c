int uniquePaths(int m, int n) {
    int * counts = calloc(m, sizeof(int));

    size_t i, j;
    for (j = 0; j < m; ++j)
        counts[j] = 1;

    for (i = 1; i < n; ++i)
        for (j = 1; j < m; ++j)
            counts[j] += counts[j-1];

    int val = counts[m-1];
    free(counts);

    return val;
}
