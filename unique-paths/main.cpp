#include <iostream>
#include <vector>
using namespace std;

class Solution {
   public:
      int uniquePaths(int m, int n) {
          vector<int> counts (m, 1);

          for (size_t i = 1; i < n; ++i)
              for (size_t j = 1; j < m; ++j)
                  counts[j] += counts[j-1];

          return counts[m-1];
      }
};

int main() {
   Solution s;
   cout << s.uniquePaths(23, 12) << endl;
   return 0;
}
