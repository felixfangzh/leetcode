#include <iostream>
#include <vector>
using namespace std;

class Solution {
   public:
      vector<vector<int> > generate(int numRows) {
         vector<vector<int> > ptriangle;
         if (numRows <= 0)
            return ptriangle;
         vector<int> first_row;
         first_row.push_back(1);
         ptriangle.push_back(first_row);
         for (int i = 1; i < numRows; ++i) {
            vector<int> row;
            for (int j = 0; j <= i; ++j) {
               vector<int> &prev = ptriangle.back();
               row.push_back((j > 0 ? prev[j-1] : 0)+(j < i ? prev[j] : 0));
            }
            ptriangle.push_back(row);
         }
         return ptriangle;
      }
};

int main() {
   Solution s;
   for (auto &v : s.generate(5)) {
      cout << "[";
      for (auto &i : v) {
         cout << i << " ";
      }
      cout << "]" << endl;
   }
   return 0;
}
