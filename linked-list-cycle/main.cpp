#include <iostream>
using namespace std;

struct ListNode {
   int val;
   ListNode *next;
   ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
   public:
      bool hasCycle(ListNode *head) {
         ListNode *slowNode, *fastNode1, *fastNode2;
         slowNode = fastNode1 = fastNode2 = head;
         while (slowNode && (fastNode1 = fastNode2->next) && (fastNode2 = fastNode1->next)){
            if (slowNode == fastNode1 || slowNode == fastNode2) {
               return true;
            }
            slowNode = slowNode->next;
         }
         return false;
      }
};

#define SIZE 28

int main() {
   ListNode* nodes[SIZE];
   int vals[] = {-21,10,17,8,4,26,5,35,33,-7,-16,27,-12,6,29,-12,5,9,20,14,14,2,13,-24,21,23,-21,5};
   for (int i = 0; i < SIZE; ++i) {
      nodes[i] = new ListNode(vals[i]);
      cout << vals[i] << " ";
   }
   nodes[SIZE-1]->next = nodes[24];
   cout << endl;
   for (int i = 0; i < SIZE-1; ++i) {
      nodes[i]->next = nodes[i+1]; 
   }

   Solution s;
   cout << (s.hasCycle(nodes[0]) ? "true" : "false") << endl;
   return 0;
}
