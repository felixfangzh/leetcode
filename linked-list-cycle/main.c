/**
 * Definition for singly-linked list.
 */
struct ListNode {
   int val;
   struct ListNode *next;
};

bool hasCycle(struct ListNode *head) {
   if (!head) return false;

   struct ListNode *t, *r;
   for (t = head, r = head->next; t && r && r->next; t = t->next, r = r->next->next)
      if (t == r) return true;

   return false;
}
