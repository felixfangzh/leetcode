public class Solution {
   /**
    * Definition for singly-linked list.
    */
   class ListNode {
      int val;
      ListNode next;
      ListNode(int x) {
         val = x;
         next = null;
      }
   }
   public boolean hasCycle(ListNode head) {
      if (head == null) return false;
      for (ListNode t = head, r = head.next; t != null && r != null && r.next != null; t = t.next, r = r.next.next)
         if (r == t) return true;

      return false;
   }
}
