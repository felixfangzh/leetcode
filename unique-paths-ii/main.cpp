#include <iostream>
#include <vector>
using namespace std;

class Solution {
   private:
      int nums[100][100];
   public:
      int uniquePathsWithObstacles(vector<vector<int> > &obstacleGrid) {
         if (obstacleGrid.empty())
            return 0;
         int m(obstacleGrid.size()), n(obstacleGrid[0].size());
         nums[m-1][n-1] = obstacleGrid.back().back() ^ 1;

         for (int i = m*n-2; i >=0; --i) {
            int p(i/n), q(i - p*n);
            nums[p][q] = 0;
            if (obstacleGrid[p][q])
               continue;
            if (p < m-1)
               nums[p][q] += nums[p+1][q];
            if (q < n-1)
               nums[p][q] += nums[p][q+1];
         }

         return nums[0][0];

      }
};

int main() {
   Solution s;
   return 0;
}
