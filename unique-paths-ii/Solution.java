public class Solution {
    public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        int [] row = new int[obstacleGrid[0].length];

        row[row.length-1] = 1 - obstacleGrid[obstacleGrid.length-1][obstacleGrid[0].length-1];

        for (int i = row.length - 2; i >= 0; --i) {
            row[i] = obstacleGrid[obstacleGrid.length-1][i] == 1 ? 0 : row[i+1];
        }

        for (int j = obstacleGrid.length - 2; j >=0; --j) {
            for (int i = row.length - 1; i >= 0; --i) {
                if (obstacleGrid[j][i] == 1)
                    row[i] = 0;
                else if (i < row.length - 1)
                    row[i] += row[i+1];
            }
        }

        return row[0];
    }
}
