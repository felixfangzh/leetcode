#include <iostream>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <queue>
using namespace std;

class Solution {
   public:
      int ladderLength(string start, string end, unordered_set<string> &dict) {
         int s, e;
         unordered_map<string, size_t> distance;
         dict.insert(end);
         distance[start] = 1;
         queue<string> q;
         q.push(start);
         while (!q.empty()) {
            string S = q.front(); q.pop();
            string s(S);
            for (size_t j = 0, _size = s.size(); j < _size; ++j) {
               char tmp = s[j]; 
               for (char c = 'a'; c <= 'z'; ++c) { 
                  if (c == tmp) continue; 
                  s[j] = c;
                  if (dict.count(s) && !distance.count(s)) {
                     distance[s] = distance[S] + 1;
                     if (s == end)
                        return distance[end];
                     q.push(s);
                  }
               }
               s[j] = tmp;
            }
         }
         return distance[end];
      }
};

int main() {
   Solution s;
   string str[] = {"hot","dot","dog","lot","log"};
   unordered_set<string> dict(begin(str), end(str));
   cout <<  s.ladderLength("hit", "cog", dict) << ", expect 5" << endl;
   return 0;
}
