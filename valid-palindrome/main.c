#include <stdio.h>
#include <ctype.h>

bool isPalindrome(char *s) {
   if (!s) return false;
   char *e = s + strlen(s) - 1;

   while (s != e) {
      if (!isalnum(*s)) {
         ++s; continue;
      }
      if (!isalnum(*e)) {
         --e; continue;
      }
      if (tolower(*s++) != tolower(*e--)) return false;
   }

   return true;
}

int main() {
   printf("%d\n", isPalindrome("aA"));
   return 0;
}
