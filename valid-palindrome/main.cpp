#include <iostream>
#include <algorithm>
#include <cctype>
using namespace std;

class Solution {
   public:
      bool isPalindrome(string s) {
         for (ssize_t i = 0, j = s.size() - 1; i < j; ) {
            if (!isalnum(s[i])) {
               ++i;
               continue;
            }
            if (!isalnum(s[j])) {
               --j;
               continue;
            }
            if (tolower(s[i++]) != tolower(s[j--])) {
               return false;
            }
         }
         return true;
      }
};

int main() {
   Solution s;
   cout << s.isPalindrome("aA") << endl;
   cout << s.isPalindrome("A man, a plan, a canal: Panama") << endl;
   cout << s.isPalindrome("race a car") << endl;
   cout << s.isPalindrome("a.") << endl;
   return 0;
}
