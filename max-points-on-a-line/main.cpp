#include <iostream>
#include <climits>
#include <cmath>
#include <vector>
#include <unordered_map>
#include <unordered_set>
using namespace std;

struct Point {
   int x;
   int y;
   Point() : x(0), y(0) {}
   Point(int a, int b) : x(a), y(b) {}
};

class Solution {
#if 0
   private:
      const int scale = 100;
      const double pi = atan2(0, -1);
      double houghTransform(Point &p, double theta) {
         return p.x*cos(theta) + p.y*sin(theta);
      }
   public:
      int maxPoints(vector<Point> &points) {
         if (points.size() <= 2)
            return points.size();
         int max_count = 0;
         unordered_map<int, int> rho;
         for (int i = 0, size = 180*scale; i < size; ++i) {
            rho.clear();
            double theta = (double)i / 180 / scale * pi;
            for (auto &p : points) {
               ++rho[(int)(houghTransform(p, theta) * scale)];
            }
            for (auto i = rho.begin(); i != rho.end(); ++i)
               max_count = max(max_count, i->second);
         }
         return max_count;
      }
#endif
   private:
      const int scale = 10000;
      int computeSlope(Point &p1, Point &p2) {
         return p1.x == p2.x ? INT_MAX : (int)((p1.y - p2.y) / (double)(p1.x - p2.x) * scale);
      }
   public:
      int maxPoints(vector<Point> &points) {
         if (points.size() == 1)
            return 1;
         int max_count = 0;
         unordered_map<int, int> slope_count;
         unordered_set<int> visited;
         for (int i = 0, size = points.size(); i < size; ++i) {
            if (visited.count(i))
               continue;
            slope_count.clear();
            int duplicates = 0;
            for (int j = i+1; j < size; ++j) {
               if (points[i].x == points[j].x && points[i].y == points[j].y) {
                  ++duplicates;
                  visited.insert(j);
                  continue;
               }
               int slope = computeSlope(points[i], points[j]);
               auto iter = slope_count.find(slope);
               if (iter == slope_count.end())
                  slope_count[slope] = 2;
               else
                  iter->second++;
            }
            int _max_count = 1;
            for (auto iter = slope_count.begin(); iter != slope_count.end(); ++iter)
               _max_count = max(_max_count, iter->second);
            max_count = max(duplicates + _max_count, max_count); 
         }
         return max_count;
      }
};

int main() {
   Solution s;
   int p [][2] = { {560,248},{0,16},{30,250},{950,187},{630,277},{950,187},{-212,-268},{-287,-222},{53,37},{-280,-100},{-1,-14},{-5,4},{-35,-387},{-95,11},{-70,-13},{-700,-274},{-95,11},{-2,-33},{3,62},{-4,-47},{106,98},{-7,-65},{-8,-71},{-8,-147},{5,5},{-5,-90},{-420,-158},{-420,-158},{-350,-129},{-475,-53},{-4,-47},{-380,-37},{0,-24},{35,299},{-8,-71},{-2,-6},{8,25},{6,13},{-106,-146},{53,37},{-7,-128},{-5,-1},{-318,-390},{-15,-191},{-665,-85},{318,342},{7,138},{-570,-69},{-9,-4},{0,-9},{1,-7},{-51,23},{4,1},{-7,5},{-280,-100},{700,306},{0,-23},{-7,-4},{-246,-184},{350,161},{-424,-512},{35,299},{0,-24},{-140,-42},{-760,-101},{-9,-9},{140,74},{-285,-21},{-350,-129},{-6,9},{-630,-245},{700,306},{1,-17},{0,16},{-70,-13},{1,24},{-328,-260},{-34,26},{7,-5},{-371,-451},{-570,-69},{0,27},{-7,-65},{-9,-166},{-475,-53},{-68,20},{210,103},{700,306},{7,-6},{-3,-52},{-106,-146},{560,248},{10,6},{6,119},{0,2},{-41,6},{7,19},{30,250} };
   //int p [][2] {{0, 0}, {1, 1}, {0, 0}};
   vector<Point> points;
   for (int i = 0, size = distance(begin(p), end(p)); i < size; ++i) {
      points.push_back(Point(p[i][0], p[i][1]));
   }
   cout << s.maxPoints(points) << endl;
   return 0;
}
