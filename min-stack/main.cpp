#include <iostream>
#include <stack>

using namespace std;

class MinStack {
   private:
      stack<int> m_stack;
      stack<int> m_minStack;
   public:
      void push(int x) {
         m_stack.push(x);
         if (m_minStack.empty() || m_minStack.top() >= x)
            m_minStack.push(x);
      }

      void pop() {
         if (m_minStack.top() == m_stack.top())
            m_minStack.pop();
         m_stack.pop();
      }

      int top() {
         return m_stack.top();
      }

      int getMin() {
         return m_minStack.top();
      }
};

int main() {
   MinStack minStack;
   minStack.push(512);
   minStack.push(-1024);
   minStack.push(-1024);
   minStack.push(512);
   minStack.pop();
   cout << "Expected: -1024, Actual: " <<  minStack.getMin() << endl;
   minStack.pop();
   cout << "Expected: -1024, Actual: " <<  minStack.getMin() << endl;
   minStack.pop();
   cout << "Expected: 512, Actual: " <<  minStack.getMin() << endl;
}
