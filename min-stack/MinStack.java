import java.util.*;

class MinStack {
   private Deque<Integer> minStack = new ArrayDeque<Integer>();
   private Deque<Integer> stack = new ArrayDeque<Integer>();

   public void push(int x) {
      stack.push(x);
      if (minStack.isEmpty() || minStack.peek() >= x)
         minStack.push(x);
   }

   public void pop() {
      if (stack.peek().equals(minStack.peek()))
         minStack.pop();
      stack.pop();
   }

   public int top() {
      return stack.peek();
   }

   public int getMin() {
      return minStack.peek();
   }

   public static void main(String [] args) {
      MinStack stack = new MinStack();
      stack.push(512);
      stack.push(-1024);
      stack.push(-1024);
      stack.push(512);
      stack.pop();
      System.out.println("Expected: -1024, Actual: " + stack.getMin());
      stack.pop();
      System.out.println("Expected: -1024, Actual: " + stack.getMin());
      stack.pop();
      System.out.println("Expected: 512, Actual: " + stack.getMin());
   }
}

