#include <iostream>
using namespace std;

struct TreeNode {
   int val;
   TreeNode *left;
   TreeNode *right;
   TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
   private:
      int depth(TreeNode *root) {
         return root? 1 + max(depth(root->left), depth(root->right)) : 0; 
      }
   public:
      bool isBalanced(TreeNode *root) {
         return !root ? true : abs(depth(root->left) - depth(root->right)) <= 1 && isBalanced(root->left) && isBalanced(root->right);
      }
};

int main() {
   Solution s;
   return 0;
}
