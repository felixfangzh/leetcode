#include <iostream>
#include <vector>
using namespace std;

class Solution {
   public:
      bool isInterleave(string s1, string s2, string s3) {
         ssize_t i, j, size1(s1.size()), size2(s2.size()), size3(s3.size());
         if (size1 + size2 != size3) return false;
         vector<bool> dp(size2+1);
         dp[size2] = true;
         for (j = size2 - 1; j >=0; --j)
            if (!(dp[j] = s2[j] == s3[size1+j] && dp[j+1]))
               break;
         for (i = size1 - 1; i >=0; --i) {
            for (j = size2 - 1; j >=0; --j)
               dp[j] = (s3[i+j] == s1[i] && dp[j]) || (s3[i+j] == s2[j] && dp[j+1]);
            dp[size2] = s3[size2 + i] == s1[i] && dp[size2];
         }
         return dp[0];
      }
};

int main() {
   Solution s;
   cout << s.isInterleave("aabcc", "dbbca", "aadbbcbcac") << ", expect true" << endl;
   cout << s.isInterleave("aabcc", "dbbca", "aadbbbaccc") << ", expect false" << endl;
   return 0;
}
