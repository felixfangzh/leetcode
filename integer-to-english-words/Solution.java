import java.util.*;

public class Solution {
   private String smallNumberToWords(int num) {
      switch (num) {
         case 10:
            return "Ten";
         case 11:
            return "Eleven";
         case 12:
            return "Twelve";
         case 13:
            return "Thirteen";
         case 14:
            return "Fourteen";
         case 15:
            return "Fifteen";
         case 16:
            return "Sixteen";
         case 17:
            return "Seventeen";
         case 18:
            return "Eighteen";
         case 19:
            return "Nineteen";
         default:
            break;
      }

      Deque<String> stack = new ArrayDeque<String>();

      switch(num % 10) {
         case 1:
            stack.push("One");
            break;
         case 2:
            stack.push("Two");
            break;
         case 3:
            stack.push("Three");
            break;
         case 4:
            stack.push("Four");
            break;
         case 5:
            stack.push("Five");
            break;
         case 6:
            stack.push("Six");
            break;
         case 7:
            stack.push("Seven");
            break;
         case 8:
            stack.push("Eight");
            break;
         case 9:
            stack.push("Nine");
            break;
         default:
            break;
      }

      switch((num / 10) % 10) {
         case 2:
            stack.push("Twenty");
            break;
         case 3:
            stack.push("Thirty");
            break;
         case 4:
            stack.push("Forty");
            break;
         case 5:
            stack.push("Fifty");
            break;
         case 6:
            stack.push("Sixty");
            break;
         case 7:
            stack.push("Seventy");
            break;
         case 8:
            stack.push("Eighty");
            break;
         case 9:
            stack.push("Ninety");
            break;
         default:
            break;
      }

      return joinStack(stack);
   }

   private String hundredToWords(int num) {
      Deque<String> stack = new ArrayDeque<String>();

      stack.push(smallNumberToWords(num % 100));
      
      num = (num / 100) % 10;
      if(num > 0) {
         stack.push("Hundred");
         stack.push(smallNumberToWords(num));
      }

      return joinStack(stack);
   }

   private String thousandToWords(int num) {
      Deque<String> stack = new ArrayDeque<String>();

      stack.push(hundredToWords(num % 1000));
      
      num = (num / 1000) % 1000;
      if(num > 0) {
         stack.push("Thousand");
         stack.push(hundredToWords(num));
      }

      return joinStack(stack);
   }

   private String millionToWords(int num) {
      Deque<String> stack = new ArrayDeque<String>();

      stack.push(thousandToWords(num % 1000000));
      
      num = (num / 1000000) % 1000;
      if(num > 0) {
         stack.push("Million");
         stack.push(thousandToWords(num));
      }

      return joinStack(stack);
   }

   private String billionToWords(int num) {
      Deque<String> stack = new ArrayDeque<String>();

      stack.push(millionToWords(num % 1000000000));
      
      num = (num / 1000000000) % 1000;
      if(num > 0) {
         stack.push("Billion");
         stack.push(thousandToWords(num));
      }

      return joinStack(stack);
   }

   private String joinStack(Deque<String> stack) {
      StringBuilder builder = new StringBuilder();
      while(!stack.isEmpty()) {
         String e = stack.pop();
         if (e.isEmpty()) {
            continue;
         }

         if (builder.length() > 0) {
            builder.append(" ");
         }

         builder.append(e);
      }

      return builder.toString();
   }

   public String numberToWords(int num) {
      if(num == 0) return "Zero";

      return billionToWords(num);
   }
}
