/**
 * Definition for an interval.
 */
#include <vector>
#include <algorithm> 
#include <iostream> 
using namespace std;

struct Interval {
   int start;
   int end;
   Interval() : start(0), end(0) {}
   Interval(int s, int e) : start(s), end(e) {}
};

bool compareIntervals(Interval i1, Interval i2) {
   return i1.start < i2.start;
}

class Solution {
   private:
      bool isOverlapped(Interval i1, Interval i2) {
         return i1.start <= i2.end && i2.start <= i1.end;
      }

      Interval mergeTwoIntervals(Interval i1, Interval i2) {
         i1.start = min(i1.start, i2.start);
         i1.end = max(i1.end, i2.end);
         return i1;
      }

   public:
      vector<Interval> merge(vector<Interval> &intervals) {
         if (intervals.size() < 2)
            return intervals;
         vector<Interval> newIntervals;
         newIntervals.reserve(intervals.size());

         sort(intervals.begin(), intervals.end(), compareIntervals);

         Interval current = intervals[0];
         for (auto iter = intervals.begin()+1; iter != intervals.end(); ++iter) {
            if (isOverlapped(current, *iter)) {
               current = mergeTwoIntervals(current, *iter);
            }
            else {
               newIntervals.push_back(current);
               current = *iter;
            }
         }
         newIntervals.push_back(current);
         return newIntervals;
      }
};

int main() {
   vector<Interval> intervals;
   Interval i1(1, 3);
   Interval i2(2, 6);
   Interval i3(8, 10);
   Interval i4(15, 18);
   intervals.push_back(i3);
   intervals.push_back(i4);
   intervals.push_back(i1);
   intervals.push_back(i2);
   Solution s;
   vector<Interval> newIntervals = s.merge(intervals);
   for (auto iter = newIntervals.begin(); iter != newIntervals.end(); ++iter) {
      cout << "[" << iter->start << ", " << iter->end << "], ";
   }
   cout << endl;
   return 0;
}
