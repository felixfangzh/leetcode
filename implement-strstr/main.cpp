#include <iostream>
#include <vector>
using namespace std;

class Solution {
   private:
      void buildTable(char *needle, vector<int> &T){
         T.push_back(-1);
         T.push_back(0);
         if (!needle[0])
            return;

         for (int pos = 1, cnd = 0; needle[pos]; ) {
            if (needle[pos] == needle[cnd]) {
               T.push_back(++cnd);
               ++pos;
            }
            else if (cnd > 0) {
               cnd = T[cnd];
            }
            else {
               T.push_back(0);
               ++pos;
            }
         }
      }
   public:
      char *strStr(char *haystack, char *needle) {
         if (!haystack || !needle)
            return NULL;
         if (!needle[0])
            return haystack;
         vector<int> T;
         buildTable(needle, T);
         for (int i = 0, j = 0; haystack[i + j]; ) {
            if (needle[i] == haystack[i + j]) {
               if (!needle[++i]) {
                  return haystack+j;
               }
            }
            else {
               j = j + i - T[i];
               i = max(0, T[i]);
            }
         }

         return NULL;
      }
};

void print(char * s) {
   cout << (s ? s : "NULL") << endl;
}

int main() {
   Solution s;
   print(s.strStr("mississippi", "mississippi"));
   print(s.strStr("", ""));
   return 0;
}
