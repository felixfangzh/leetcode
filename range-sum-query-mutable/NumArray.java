public class NumArray {
    private int [] nums;
    private int [] bitree;
    public NumArray(int[] n) {
        nums = new int[n.length];
        bitree = new int[n.length + 1];

        for (int i = 0; i < nums.length; ++i) {
            nums[i] = 0;
        }  

        for (int i = 0; i < bitree.length; ++i) {
            bitree[i] = 0;
        }  

        for (int i = 0; i < n.length; ++i) {
            update(i, n[i]);
        }
    }

    void update(int i, int val) {
        if(nums[i] != val) {
            int diff = val - nums[i];
            for (int index = i + 1; index < bitree.length; index += index & -index) {
                bitree[index] += diff;
            }

            nums[i] = val;
        }
    }

    private int sumRange(int i) {
        int sum = 0;

        for(int index = i + 1; index > 0; index -= index & -index) {
            sum += bitree[index];
        }

        return sum;
    }

    public int sumRange(int i, int j) {
        return sumRange(j) - sumRange(i - 1);
    }
}
