#include <vector>
#include <iostream>

using namespace std;

class NumArray {
    private:
        vector<int> bitree;
        vector<int> nums;
    public:
        NumArray(vector<int> &n) {
            nums = vector<int>(n.size(), 0);
            bitree = vector<int>(nums.size() + 1, 0);

            for(int i = 0; i < n.size(); ++i) {
                update(i, n[i]);
            }
        }

        void update(int i, int val) {
            if(nums[i] != val) {
                int diff = val - nums[i];
                for (int index = i + 1; index < bitree.size(); index += index & (-index)) {
                    bitree[index] += diff;
                }

                nums[i] = val;
            }
        }

        int sumRange(int i) {
            int sum = 0;

            for (int index = i + 1; index > 0; index -= index & (-index)) {
                sum += bitree[index];
            }

            return sum;
        }

        int sumRange(int i, int index) {
            return sumRange(index) - sumRange(i - 1);
        }
};

int main() {
    vector<int> nums = {1, 3, 5};
    NumArray numArray(nums);
    cout << numArray.sumRange(0, 2) << endl;
    numArray.update(1, 2);
    cout << numArray.sumRange(0, 2) << endl;
    return 0;
}
