#include <iostream>
#include <utility>
#include <climits>
#include <vector>
using namespace std;

class Solution {
   public:
      int trap(int A[], int n) {
         if (n < 3)
            return 0;
         int sum = 0;
         vector<pair<int, int> > maxes(n, make_pair(0, 0));
         for (int i = 1; i < n; ++i) {
            maxes[i].first = max(maxes[i-1].first, A[i-1]);
            maxes[n-i-1].second = max(maxes[n-i].second, A[n-i]);
         }
         for (int i = 0; i < n; ++i) {
            sum += max(0, (min(maxes[i].first, maxes[i].second) - A[i]));
         }
         return sum;
      }
};

int main() {
   //int A [] = {0,1,0,2,1,0,1,3,2,1,2,1};
   //int A [] = {8,2,8,9,0,1,7,7,9};
   int A [] = {2, 0, 2};
   Solution s;
   cout << s.trap(A, distance(begin(A), end(A))) << endl;
   return 0;
}
