#include <iostream>
#include <stdio.h>
#include <stack>

using namespace std;

//#define USE_STACK

/**
 * Definition for binary tree
 */
struct TreeNode {
   int val;
   TreeNode *left;
   TreeNode *right;
   TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class BSTIterator {
#ifdef USE_STACK
   private:
      stack<TreeNode*> nodeStack;
      void populateStack() {
         if (nodeStack.empty())
            return;
         for (
               TreeNode* current = nodeStack.top(); 
               current->left; 
               nodeStack.push(current->left), current = current->left
             );
      }
   public:
      BSTIterator(TreeNode *root) {
         if (!root)
            return;
         nodeStack.push(root);
         populateStack();
      }

      /** @return whether we have a next smallest number */
      bool hasNext() {
         return !nodeStack.empty();
      }

      /** @return the next smallest number */
      int next() {
         if (!hasNext())
            return int();
         TreeNode* node = nodeStack.top();
         int val = node->val;
         nodeStack.pop();

         if (node->right) {
            nodeStack.push(node->right);
            populateStack();
         }

         return val;
      }
#else
   private:
      TreeNode* current;
      void updateCurrent() {
         if (!current)
            return;

         for (;current->left; current = current->left) {
            TreeNode* leftRight = current->left;
            for (; leftRight->right && leftRight->right != current; leftRight = leftRight->right);
            
            if (leftRight->right == current) {
               leftRight->right = NULL;
               return;
            }

            if (leftRight->right == NULL)
               leftRight->right = current;
         }
      }
   public:
      BSTIterator(TreeNode *root) : current(root) {
         updateCurrent();
      }

      /** @return whether we have a next smallest number */
      bool hasNext() {
         return current;
      }

      /** @return the next smallest number */
      int next() {
         if (!hasNext())
            return int();

         int val = current->val;
         current = current->right;
         updateCurrent();

         return val;
      }
#endif
};

/**
 * Your BSTIterator will be called like this:
 * BSTIterator i = BSTIterator(root);
 * while (i.hasNext()) cout << i.next();
 */
int main() {
   TreeNode root(2), node(1);
   root.left = &node;
   BSTIterator i = BSTIterator(&root);
   while (i.hasNext()) cout << i.next() << " ";
   cout << endl;
   return 0;
}
