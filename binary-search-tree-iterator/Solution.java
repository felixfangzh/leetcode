public class BSTIterator {
    /**
     * Definition for binary tree
     */
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) { val = x; }
    }

    private TreeNode current;

    private void updateCurrent() {
        if (null == current) return;

        for (; null != current.left; current = current.left) {
            TreeNode leftRight = current.left;
            for (; null != leftRight.right && current != leftRight.right; leftRight = leftRight.right);
            if (null == leftRight.right) {
                leftRight.right = current;
            }
            else if (current == leftRight.right) {
                leftRight.right = null;
                break;
            }
        }
    }

    public BSTIterator(TreeNode root) {
        current = root;
        updateCurrent();
    }

    /** @return whether we have a next smallest number */
    public boolean hasNext() {
        return current != null;
    }

    /** @return the next smallest number */
    public int next() {
        if (!hasNext()) return -1;

        int val = current.val;

        current = current.right;
        updateCurrent();

        return val;
    }
}

/**
 * Your BSTIterator will be called like this:
 * BSTIterator i = new BSTIterator(root);
 * while (i.hasNext()) v[f()] = i.next();
 */
