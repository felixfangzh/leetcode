#include <iostream>
#include <unordered_map>
using namespace std;

class Solution {
   private:
      unordered_map<int, double> pows;
      double _pow(double x, int n) {
         if (n == 0)
            return 1;
         if (n == 1)
            return x;
         if (n == -1)
            return 1/x;
         int n1(n/2), n2(n-n1);
         if (!pows.count(n1))
            pows[n1] = _pow(x, n1);
         if (!pows.count(n2))
            pows[n2] = _pow(x, n2);
         return pows[n1] * pows[n2];
      }
   public:
      double pow(double x, int n) {
         pows.clear();
         return _pow(x, n);
      }
};

int main() {
   Solution s;
   cout << s.pow(34.00515, -3) << endl;
   return 0;
}
