#include <iostream>
#include <unordered_map>
using namespace std;

class Solution {
   private:
      unordered_map<string, int> decodings;
      bool isValidDecoding(string s) {
         int i = atoi(s.c_str());
         return (s.size() == 1 && i >= 1 && i <= 9)
            || (s.size() == 2 && i >= 10 && i <= 26);
      }
   public:
      void printDecodings() {
         for (auto i = decodings.begin(); i != decodings.end(); ++i)
            cout << i->first << ":" << i->second << endl;
      }
      int numDecodings(string s) {
         if (s.empty())
            return 0;
         if (decodings.count(s))
            return decodings[s];
         int num;
         if (s.size() == 1) {
            num = (int)isValidDecoding(s);
            decodings[s] = num;
            return num;
         }
         if (s.size() == 2) {
            num = (int)isValidDecoding(s) + numDecodings(s.substr(0, 1))*numDecodings(s.substr(1, 1));
            decodings[s] = num;
            return num;
         }

         num = 0;
         for (int i = 1; i <= 2; ++i)
            num += (int)isValidDecoding(s.substr(0, i)) * numDecodings(s.substr(i));
         decodings[s] = num;
         return num;
      }
};

int main() {
   Solution s;
   cout << s.numDecodings("3555") << endl;
   s.printDecodings();
   return 0;
}
