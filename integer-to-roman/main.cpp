#include <iostream>
#include <sstream>
#include <vector>
#include <utility>
using namespace std;

class Solution {
   private:
      vector<pair<int, string>> conversion;
   public:
      Solution() {
         conversion.push_back(make_pair(1000, "M"));
         conversion.push_back(make_pair(900 , "CM"));
         conversion.push_back(make_pair(500 , "D"));
         conversion.push_back(make_pair(400 , "CD"));
         conversion.push_back(make_pair(100 , "C"));
         conversion.push_back(make_pair(90  , "XC"));
         conversion.push_back(make_pair(50  , "L"));
         conversion.push_back(make_pair(40  , "XL"));
         conversion.push_back(make_pair(10  , "X"));
         conversion.push_back(make_pair(9   , "IX"));
         conversion.push_back(make_pair(5   , "V"));
         conversion.push_back(make_pair(4   , "IV"));
         conversion.push_back(make_pair(1   , "I"));

      }
      string intToRoman(int num) {
         stringstream ret;
         for (auto it = conversion.begin(); it != conversion.end() && num; ) {
            if (num >= it->first) {
               ret << it->second;
               num -= it->first;
            }
            else {
               ++it;
            }
         }
         return ret.str();
      }
};

int main() {
   Solution s;
   cout << s.intToRoman(1954) << ", expected MCMLIV" << endl;
   return 0;
}
