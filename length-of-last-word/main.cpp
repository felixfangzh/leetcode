#include <iostream>
using namespace std;

class Solution {
   public:
      int lengthOfLastWord(const char *s) {
         int count = 0;
         for (; *s; ++s) {
            if (*s == ' ') {
               if (*(s+1) && *(s+1) != ' ')
                  count = 0;
            }
            else
               ++count;
         }
         return count;
      }
};

int main () {
   Solution s;
   cout << s.lengthOfLastWord("Hello    ") << endl;
   return 0;
}
