#include <iostream>
#include <climits>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
   private:
      void _maxPathSum(TreeNode *root, int &sum, int &maxSum) {
         if (!root) {
            sum = 0;
            return;
         }
         int lsum, rsum;
         _maxPathSum(root->left, lsum, maxSum);
         _maxPathSum(root->right, rsum, maxSum);
         sum = max(root->val, max(root->val + lsum, root->val + rsum));
         maxSum = max(maxSum, max(sum, root->val + lsum + rsum));
      }
   public:
      int maxPathSum(TreeNode *root) {
         int sum, maxSum = INT_MIN;
         _maxPathSum(root, sum, maxSum);
         return maxSum;
      }
};

int main() {
   Solution s;
   cout << s.maxPathSum(new TreeNode(0)) << endl;
   return 0;
}
