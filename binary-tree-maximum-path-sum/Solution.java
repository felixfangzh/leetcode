/**
 * Definition for binary tree
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    private void maxPathSum(TreeNode root, int [] sums) {
        if (null == root) {
            sums[0] = 0;
            return;
        }

        maxPathSum(root.left, sums);
        int lsum = sums[0];
        maxPathSum(root.right, sums);
        int rsum = sums[0];
        
        sums[0] = Math.max(root.val, Math.max(root.val + lsum, root.val + rsum));
        sums[1] = Math.max(sums[1], Math.max(sums[0], root.val + lsum + rsum));
    }
    
    public int maxPathSum(TreeNode root) {
        if (null == root) return 0;
        int [] sums = new int [] {Integer.MIN_VALUE, Integer.MIN_VALUE};
        maxPathSum(root, sums);
        return sums[1];
    }
}
