#include <iostream>
#include <vector>
using namespace std;

class Solution {
   public:
      vector<vector<int> > generateMatrix(int n) {
         vector<vector<int> > ret(max(0, n), vector<int>(max(0, n)));
         if (n <= 0) return ret;
         pair<ssize_t, ssize_t> Y(0, n-1), X(0, n-1);
         size_t j(0);
         while (true) {
            for (ssize_t i = X.first; i <= X.second; ++i)
               ret[Y.first][i] = ++j;
            if (++Y.first > Y.second) break;
            for (ssize_t i = Y.first; i <= Y.second; ++i)
               ret[i][X.second] = ++j;
            if (X.first > --X.second) break;
            for (ssize_t i = X.second; i >= X.first; --i)
               ret[Y.second][i] = ++j;
            if (Y.first > --Y.second) break;
            for (ssize_t i = Y.second; i >= Y.first; --i)
               ret[i][X.first] = ++j;
            if (++X.first > X.second) break;
         }
         return ret;
      }
};

int main() {
   Solution s;
   for (auto &v : s.generateMatrix(3)) {
      for (auto &i : v)
         cout << i << " ";
      cout << endl;
   }
   return 0;
}
