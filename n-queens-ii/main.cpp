#include <iostream>
#include <vector>
#include <stack>
#include <utility>
using namespace std;

class Solution {
   private:
      void updateAvail(vector<vector<bool> > &avail, int i, int j, stack<pair<int, int> > &changes) {
         for (int p = 0, size = avail.size(), _size = avail[0].size(); p < size; ++p) {
            for (int q = 0; q < _size; ++q) {
               if ((p == i || q == j || abs(p-i) == abs(q-j)) && avail[p][q]) {
                  avail[p][q] = false;
                  changes.push(make_pair(p, q));
               }
            }
         }
      }

      void undoAvail(vector<vector<bool> > &avail, stack<pair<int, int> > &changes) {
         while (!changes.empty()) {
            pair<int, int> t = changes.top(); changes.pop();
            avail[t.first][t.second] = true;
         }
      }

      void findSolutions(vector<vector<bool> > &avail, int current_row, int n, int &count) {
         stack<pair<int, int> > changes;
         for (int i = 0; i < n; ++i) {
            if (avail[current_row][i]) {
               if (current_row == n - 1) {
                  ++count;
                  return;
               }
               updateAvail(avail, current_row, i, changes);
               findSolutions(avail, current_row+1, n, count);
               undoAvail(avail, changes);
            }
         }
      }
   public:
      int totalNQueens(int n) {
         int count = 0;
         if (n <= 0)
            return count;
         vector<vector<bool> > avail(n, vector<bool>(n, true));
         findSolutions(avail, 0, n, count);
         return count;
      }
};

int main() {
   Solution s;
   cout << s.totalNQueens(5) << endl;
   return 0;
}
