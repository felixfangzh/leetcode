#include <iostream>
#include <vector>
using namespace std;

class Solution {
   public:
      int candy(vector<int> &ratings) {
         vector<int> candies(ratings.size(), 1);
         for (int i = 1, size = ratings.size(); i < size; ++i)
            if (ratings[i] > ratings[i-1])
               candies[i] = candies[i-1] + 1;
         for (int i = ratings.size() - 2; i >= 0; --i)
            if (ratings[i] > ratings[i+1] && candies[i] <= candies[i+1])
               candies[i] = candies[i+1] + 1;
         int sum = 0;
         for (auto i : candies)
            sum += i;
         return sum;
      }
};

int main() {
   int vals [] = {5, 3, 1};
   vector<int> ratings(begin(vals), end(vals));
   Solution s;
   cout << s.candy(ratings) << endl;
   return 0;
}
