#include <iostream>
#include <vector>
using namespace std;

class Solution {
   public:
      vector<string> summaryRanges(vector<int>& nums) {
         vector<string> result;

         for (size_t i = 0; i < nums.size(); ) {
            size_t s = i;
            size_t e = i + 1;

            for (; e < nums.size() && nums[e] - nums[e - 1] == 1; ++e);

            if (s == e - 1) {
                result.push_back(to_string(nums[s]));
            }
            else {
                result.push_back(to_string(nums[s]) + "->" + to_string(nums[e - 1]));
            }

            i = e;
         }

         return result;
      }
};
