#include <iostream>
#include <vector>
#include <queue>
#include <unordered_map>
using namespace std;

struct TreeNode {
   int val;
   TreeNode *left;
   TreeNode *right;
   TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

template <typename T>
void printVector(vector<T> &v) {
   for (T &i : v) 
      cout << i << " ";
   cout << endl;
}

bool isLeaveNode(TreeNode* root) {
   return root && (!root->left && !root->right);
}

void printTreeNode(TreeNode* root) {
   if (!root) {
      return;
   }
   cout << root->val << " ";
   if (isLeaveNode(root))
      return;
   queue<TreeNode*> q;
   q.push(root);
   while (!q.empty()) {
      TreeNode* node = q.front();
      q.pop();
      if (node->left) {
         cout << node->left->val << " ";
         if (!isLeaveNode(node->left))
            q.push(node->left);
      }
      else {
         cout << "# ";
      }
      if (node->right) {
         cout << node->right->val << " ";
         if (!isLeaveNode(node->right))
            q.push(node->right);
      }
      else {
         cout << "# ";
      }
   }
}

class Solution {
   unordered_map<int, int> inorder_ind;
   private:
      TreeNode *_buildTree(vector<int> &inorder, vector<int> &postorder, 
            int &post_root, int inorder_root, int inorder_left, int inorder_right) {
         TreeNode* root = new TreeNode(postorder[post_root]);
         if (inorder_left == inorder_right)
            return root;
         if (inorder_right > inorder_root) {
            --post_root;
            root->right = _buildTree(inorder, postorder, post_root, inorder_ind[postorder[post_root]], inorder_root+1, inorder_right);
         }
         if (inorder_left < inorder_root) {
            --post_root;
            root->left = _buildTree(inorder, postorder, post_root, inorder_ind[postorder[post_root]], inorder_left, inorder_root-1);
         }
         return root;
      }
   public:
      TreeNode *buildTree(vector<int> &inorder, vector<int> &postorder) {
         if (inorder.empty() || postorder.empty())
            return NULL;
         inorder_ind.clear();
         for (int i = 0, size = inorder.size(); i < size; ++i)
            inorder_ind[inorder[i]] = i;
         int post_root = postorder.size()-1;
         return _buildTree(inorder, postorder, post_root, inorder_ind[postorder[post_root]], 0, inorder.size()-1);
      }
};

int main() {
   Solution s;
   int in [] = {1, 2, 3, 4};
   int po [] = {1, 4, 3, 2};
   vector<int> inorder(begin(in), end(in));
   vector<int> postorder(begin(po), end(po));
   printTreeNode(s.buildTree(inorder, postorder));
   return 0;
}
