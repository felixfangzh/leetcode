public class Solution {
    public int longestValidParentheses(String s) {
        int [] dp = new int [s.length() + 1];
        for (int i = 0; i < dp.length(); ++i)
            dp[i] = 0;

        int count = 0;
        for (int i = 1; i <= s.length(); ++i) {
            if (s.charAt(i-1) == '(')
                ++count;
            else if (count > 0 && s.charAt(i-1) == ')') {
                --count;
                dp[i] = 2;
                if (s.charAt(i-2) == ')')
                    dp[i] += dp[i-1];
                dp[i] += dp[i - dp[i]];
            }
        }

        int maxValue = 0;
        for (int i = 0; i < dp.length(); ++i)
            if (maxValue < dp[i]) maxValue = dp[i];

        return maxValue;
    }
}
