#include <iostream>
#include <vector>

using namespace std;

class Solution {
    public:
        int longestValidParentheses(string s) {
            ssize_t count = 0;

            vector<size_t> dp(s.size()+1, 0);

            for (size_t i = 1; i <= s.size(); ++i) {
                if (s[i-1] == '(')
                    ++count;
                else if (count > 0 && s[i-1] == ')') {
                    --count;
                    dp[i] = 2;
                    if (s[i-2] == ')')
                        dp[i] += dp[i-1];
                    dp[i] += dp[i-dp[i]];
                }
            }

            return *max_element(dp.begin(), dp.end());
        }
};

int main() {
    return 0;
}
