#include <iostream>
using namespace std;

class Solution {
   public:
      int removeDuplicates(int A[], int n) {
         int l = n;
         for (int i = 1, j = 0; i < n; ++i) {
            if (A[i] != A[j]) {
               A[++j] = A[i];
            }
            else {
               --l;
            }
         }
         return l;
      }
};

int main() {
   Solution s;
   int vals [] = {1, 1, 2};
   for (int i = 0, size = s.removeDuplicates(vals, distance(begin(vals), end(vals))); i < size; ++i)
      cout << vals[i] << " ";
   cout << endl;
   return 0;
}
