#include <iostream>
#include <vector>
#include <limits.h>

using namespace std;

class Solution {
   public:
      int calculateMinimumHP(vector<vector<int> > &dungeon) {
         if (dungeon.empty()) return 1;
         vector<int> row (dungeon[0].size(), INT_MIN);
         row[row.size()-1] = 0;

         for (ssize_t i = dungeon.size() - 1; i >= 0; --i)
            for (ssize_t j = dungeon[0].size() - 1; j >= 0; --j) {
               int next_step = 
                  (j == dungeon[0].size() - 1) ? 
                  row[j] : max(row[j + 1], row[j]);
               row[j] = min(0, min(dungeon[i][j], dungeon[i][j] + next_step));
            }

         return 1 - row[0];
      }
};

template<size_t n, size_t m>
void test(Solution &sol, int (&input)[n][m], int answer) {
   vector<vector<int>> dungeon(n, vector<int>(m, 0));
   for (size_t i = 0; i < n; ++i)
      for (size_t j = 0; j < m; ++j)
         dungeon[i][j] = input[i][j];

   int res = sol.calculateMinimumHP(dungeon);
   if (res != answer)
      cout << "Failed: Expected: " << answer << ", Actual: " << res << endl;
   else
      cout << "Passed: " << answer << endl;
}

int main() {
   Solution sol;
   int input1 [1][1] = {
      {-200}
   };
   test(sol, input1, 201);

   int input2 [1][2] = {
      {-3, 5}
   };
   test(sol, input2, 4);

   int input3 [2][2] = {
      {0, 0},
      {-5, -4}
   };
   test(sol, input3, 5);

   return 0;
}
