#include <stdlib.h>
#include <limits.h>

inline int static max(int a, int b) {
   return a > b ? a : b;
}

inline static int min(int a, int b) {
   return a < b ? a : b;
}

int calculateMinimumHP(int **dungeon, int m, int n) {
   if (!dungeon || !dungeon[0] || m <= 0 || n <= 0) return 1;
   int* row = calloc(n, sizeof(int));
   ssize_t i, j;
   for (j = n - 2; j >=0; --j)
      row[j] = INT_MIN;
   row[n-1] = 0;

   for (i = m - 1; i >=0; --i)
      for (j = n - 1; j >=0; --j)
         row[j] = min(0, min(dungeon[i][j], dungeon[i][j] + ((j == n - 1) ? row[j] : max(row[j+1], row[j]))));

   int res = 1 - row[0];
   free(row);

   return res;
}

int main() {
   return 0;
}
