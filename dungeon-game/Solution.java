public class Solution {
    public int calculateMinimumHP(int[][] dungeon) {
        int [] row = new int [dungeon[0].length];
        row[row.length - 1] = Math.min(0, dungeon[dungeon.length-1][dungeon[0].length-1]);

        for (int i = row.length - 2; i >= 0; --i) {
            row[i] = Math.min(0, dungeon[dungeon.length-1][i] + row[i+1]);
        }

        for (int i = dungeon.length - 2; i >= 0; --i)
            for (int j = dungeon[0].length - 1; j >= 0; --j) {
                int nextStep = (j == dungeon[0].length - 1) ? row[j] : Math.max(row[j+1], row[j]);
                row[j] = Math.min(dungeon[i][j] + nextStep, 0);
            }

        return 1 - row[0];
    }
}
