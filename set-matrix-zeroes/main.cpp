#include <iostream>
#include <vector>
using namespace std;

class Solution {
   public:
      void setZeroes(vector<vector<int> > &matrix) {
         if (matrix.empty())
            return;
         size_t n = matrix.size();
         size_t m = matrix[0].size();
         vector<int> row(n, 0);
         vector<int> column(m, 0);
         for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
               if (!matrix[i][j])
                  row[i] = column[j] = 1;
            }
         }
         for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
               if (row[i] || column[j])
                  matrix[i][j] = 0;
            }
         }
         return;
      }
};

int main() {
   Solution s;
   return 0;
}
