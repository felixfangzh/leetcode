#include <stdlib.h>

void setZeroes(int **matrix, int m, int n) {
    int* rows = calloc(m, sizeof(int));
    int* cols = calloc(n, sizeof(int));

    size_t i, j;
    for (i = 0; i < m; ++i)
        for (j = 0; j < n; ++j)
            if (!matrix[i][j])
                rows[i] = cols[j] = 1;

    for (i = 0; i < m; ++i)
        for (j = 0; j < n; ++j)
            if (rows[i] || cols[j])
                matrix[i][j] = 0;

    free(rows);
    free(cols);
}

int main() {
    return 0;
}
