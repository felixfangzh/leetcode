#include <stdlib.h>

/**
 * Definition for singly-linked list.
 */
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
    private:
        ListNode* reverse(ListNode* head) {
            ListNode *newHead = nullptr;

            while (head) {
                ListNode* tmp = head->next;
                head->next = newHead;
                newHead = head;
                head = tmp;
            }

            return newHead;
        }

    public:
        ListNode *reverseKGroup(ListNode *head, int k) {
            if (!head || !head->next || k <= 1)  return head;

            int count = k - 1;
            ListNode *newHead = head;
            ListNode *previous = nullptr;

            for (ListNode *tmpHead = head, *current = head->next; current; current = current->next) {
                if (--count >= 0) {
                    ListNode *tmp = current->next;
                    current->next = nullptr;
                    ListNode *tmpNewHead = reverse(tmpHead);

                    if (newHead == head) newHead = tmpNewHead;
                    if (previous) previous->next = tmpNewHead;

                    tmpHead->next = tmp;
                    current = tmpHead;
                    previous = current;
                    tmpHead = tmp;

                    count = k;
                }
            }

            return newHead;
        }
};

int main() {
    return 0;
}
