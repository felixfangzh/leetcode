public class Solution {
    /**
     * Definition for singly-linked list.
     */
    static public class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {
            val = x;
            next = null;
        }
    }
    public ListNode reverse(ListNode head) {
        ListNode newHead = null;

        while (head != null) {
            ListNode tmp = head.next;
            head.next = newHead;
            newHead = head;
            head = tmp;
        }

        return newHead;
    }

    public ListNode reverseKGroup(ListNode head, int k) {
        if (null == head || null == head.next || k <= 1) return head;

        int count = k - 1;
        ListNode newHead = head;
        ListNode previous = null;
        for (ListNode tmpHead = head, current = head.next; current != null; current = current.next) {
            if (--count <= 0) {
                ListNode tmp = current.next;
                current.next = null;
                ListNode newTmpHead = reverse(tmpHead);
                if (newHead == head) newHead = newTmpHead;
                if (previous != null) previous.next = newTmpHead;
                tmpHead.next = tmp;
                current = tmpHead;
                previous = current;
                tmpHead = tmp;
                count = k;
            }
        }

        return newHead;
    }

    public static void print(ListNode head) {
        for (ListNode node = head; node != null; node = node.next) {
            if (node != head)
                System.out.print("->");
            System.out.print(node.val);
        }
        System.out.println();
    }

    public static void test(int [] vals, int k) {
        ListNode [] nodes = new ListNode[vals.length];

        for (int i = 0; i < nodes.length; ++i)
            nodes[i] = new ListNode(vals[i]);

        for (int i = 0; i < nodes.length - 1; ++i)
            nodes[i].next = nodes[i+1];

        print(new Solution().reverseKGroup(nodes[0], k));
    }

    public static void main(String [] args) {
        test(new int [] {1, 2, 3, 4}, 2);
    }
}
