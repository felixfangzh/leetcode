#include <iostream>
#include <queue>
using namespace std;

struct TreeLinkNode {
   int val;
   TreeLinkNode *left, *right, *next;
   TreeLinkNode(int x) : val(x), left(NULL), right(NULL), next(NULL) {}
};

class Solution {
   private:
      void traverse(TreeLinkNode* root, int level, vector<TreeLinkNode*> &nodes) {
         if (!root)
            return;
         if (nodes.size() == level)
            nodes.push_back(root);
         else {
            nodes[level]->next = root;
            nodes[level] = root;
         }

         traverse(root->left, level+1, nodes);
         traverse(root->right, level+1, nodes);

      }
   public:
      void connect(TreeLinkNode *root) {
         vector<TreeLinkNode*> nodes;
         traverse(root, 0, nodes);
      }
};

int main () {
   Solution s;
   int vals [] = { 0 };
   vector<TreeLinkNode*> nodes;
   for (auto i : vals) {
      nodes.push_back(new TreeLinkNode(i));
   }
   s.connect(nodes[0]);
   return 0;
}
