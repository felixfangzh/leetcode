import java.util.*;

public class Solution {
    public boolean wordPattern(String pattern, String str) {
        String[] words = str.split(" ");

        if (words.length != pattern.length()) return false;

        Map<Character, String> charMap = new HashMap<Character, String>();
        Set<String> wordSet = new HashSet<String>();

        for(int i = 0; i < words.length; ++i) {
            char c = pattern.charAt(i);
            String word = charMap.get(c);
            if (word == null) {
                if (!wordSet.add(words[i])) return false;

                charMap.put(c, words[i]);
            }
            else if (!word.equals(words[i])){
                return false;
            }
        }

        return true;
    }
}
