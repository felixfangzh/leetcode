public class Solution {
   private boolean[][] rows = new boolean[9][9];
   private boolean[][] columns = new boolean[9][9];
   private boolean[][] cells = new boolean[9][9];

   public Solution() {
      for (int i = 0; i < 9; ++i) {
         rows[i] = new boolean[9];
         columns[i] = new boolean[9];
         cells[i] = new boolean[9];
      }
   }
   private int _solveSudoku(char[][] board, int index) {
      int i = 0, j = 0;

      for (; index < 81; ++index) {
         i = index / 9;
         j = index - i*9;
         if (board[i][j] == '.')
            break;
      }

      if (index >= 81) return index;

      for (int k = 0; k < 9; ++k) {
         if (rows[i][k] || columns[j][k] || cells[i/3*3 + j/3][k])
            continue;
         rows[i][k] = columns[j][k] = cells[i/3*3 + j/3][k] = true;
         board[i][j] = (char)(k + '1');
         if (_solveSudoku(board, index + 1) >= 81) return 81;
         rows[i][k] = columns[j][k] = cells[i/3*3 + j/3][k] = false;
      }

      board[i][j] = '.';
      return index;
   }

   public void solveSudoku(char[][] board) {
      for (int i = 0; i < 9; ++i) {
         for (int j = 0; j < 9; ++j) {
            rows[i][j] = false;
            columns[i][j] = false;
            cells[i][j] = false;
         }
      }
      for (int i = 0; i < 9; ++i) {
         for (int j = 0; j < 9; ++j) {
            if (board[i][j] == '.') continue;
            rows[i][board[i][j]-'1'] = true;
            columns[j][board[i][j]-'1'] = true;
            cells[i/3*3 + j/3][board[i][j]-'1'] = true;
         }
      }

      _solveSudoku(board, 0);
   }

   public static void main(String [] args) {
      Solution sol = new Solution();
      String [] input = new String[] {
         "53..7....","6..195...",".98....6.","8...6...3","4..8.3..1","7...2...6",".6....28.","...419..5","....8..79"
      };
      char[][] board = new char [9][9];
      for (int i = 0; i < 9; ++i) {
         board[i] = new char[9];
         for (int j = 0; j < 9; ++j) {
            board[i][j] = input[i].charAt(j);
         }
      }

      sol.solveSudoku(board);
      for (int i = 0; i < 9; ++i) {
         for (int j = 0; j < 9; ++j) {
            System.out.print(board[i][j] + " "); 
         }
         System.out.println();
      }
   }
}
