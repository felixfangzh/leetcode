#include <iostream>
#include <vector>
using namespace std;

class Solution {
   private:
      void _solveSudoku(vector<vector<char> > &board, int current, int maxInd, 
            bool row[][9], bool col[][9], bool cell[][3][9], bool &completed) {
         for (; current <= maxInd; ++current) {
            int p(current/9), q(current - p*9);
            if (board[p][q] == '.')
               break;
         }
         
         if (current > maxInd) {
            completed = true;
            return;
         }

         int p(current/9), q(current - p*9);
         for (int i = 0; i < 9; ++i) {
            if (!row[p][i] && !col[q][i] && !cell[p/3][q/3][i]) {
               row[p][i] = col[q][i] = cell[p/3][q/3][i] = true;
               board[p][q] = i+1+'0';
               _solveSudoku(board, current+1, maxInd, row, col, cell, completed);
               if (completed)
                  return;
               row[p][i] = col[q][i] = cell[p/3][q/3][i] = false;
            }
         }
         board[p][q] = '.';
      }
   public:
      void solveSudoku(vector<vector<char> > &board) {
         if (board.empty())
            return;

         bool row[9][9] = {{false}};
         bool col[9][9] = {{false}};
         bool cell[3][3][9] = {{{false}}};

         for (int i = 0, size = board.size(), _size = board[0].size(); i < size; ++i)
         {
            for (int j = 0; j < _size; ++j)
            {
               if (board[i][j] != '.')
                  row[i][board[i][j]-1-'0'] = col[j][board[i][j]-1-'0'] = cell[i/3][j/3][board[i][j]-1-'0'] = true;
            }
         }

         bool completed = false;
         _solveSudoku(board, 0, board.size()*board[0].size()-1, row, col, cell, completed);
      }
};

int main() {
   Solution s;
   string input[] = {
      "53..7....","6..195...",".98....6.","8...6...3","4..8.3..1","7...2...6",".6....28.","...419..5","....8..79"
   };

   vector<vector<char> > board(9, vector<char>(9));
   for (int i = 0; i < 9; ++i) {
      for (int j = 0; j < 9; ++j) {
         board[i][j] = input[i][j];
      }
   }
   s.solveSudoku(board);
   for (int i = 0; i < 9; ++i) {
      for (int j = 0; j < 9; ++j) {
         cout << board[i][j] << " ";
      }
      cout << endl;
   }
   return 0;
}
