#include <iostream>
#include <unordered_set>
#include <unordered_map>
#include <vector>
#include <queue>
#include <list>
using namespace std;

class Solution {
   private:
      vector<string> vdict;
      vector<vector<int>> adj;
      void build(unordered_set<string> &dict) {
         vdict.clear();
         adj.clear();
         unordered_map<string, int> indices;
         for (auto &s : dict) {
            indices[s] = vdict.size();
            vdict.push_back(s);
         }
         adj.resize(vdict.size());
         for (int k = 0; k < vdict.size(); ++k) {
            string s = vdict[k];
            for (int i = 0, size = s.size(); i < size; ++i) {
               for (char c = 'a'; c <= 'z'; ++c) {
                  if (s[i] == c)
                     continue;
                  s[i] = c;
                  if (indices.count(s))
                     adj[k].push_back(indices[s]);
                  s[i] = vdict[k][i];
               }
            }
         }
      }
      void generate(int v1, int v2, vector<int> &path, vector<vector<string> > &paths, vector<vector<int> > &prev) {
         path.push_back(v2);
         if (v1 == v2 && !path.empty()) {
            paths.push_back(vector<string>());
            for (auto it = path.rbegin(); it != path.rend(); ++it) {
               paths.back().push_back(vdict[*it]);
            }
         }
         else {
            for (int i = 0; i < prev[v2].size(); ++i) 
               generate(v1, prev[v2][i], path, paths, prev);
         }
         path.pop_back();
      }
   public:
      vector<vector<string>> findLadders(string start, string end, unordered_set<string> &dict) {
         dict.insert(start);
         dict.insert(end);
         build(dict);

         queue<int> q;

         vector<vector<int> > prev(vdict.size());
         vector<int> distance(vdict.size()); 
         int v, v1, v2;
         for (v1 = 0; vdict[v1] != start; ++v1);
         for (v2 = 0; vdict[v2] != end; ++v2);
         for (int i = 0; i < adj[v1].size(); ++i) {
            v = adj[v1][i];
            q.push(v);
            prev[v].push_back(v1);
            distance[v] = 1;
         }


         while (!q.empty()) {
            int _v = q.front(); q.pop();
            if (_v == v2)
               break;
            int d = distance[_v] + 1;
            for (int i = 0; i < adj[_v].size(); ++i) {
               v = adj[_v][i];
               if (prev[v].empty()) {
                  prev[v].push_back(_v);
                  distance[v] = d;
                  q.push(v);
               }
               else if (distance[v] == d) {
                  prev[v].push_back(_v);
               }
            }
         }

         vector<vector<string> > paths;
         vector<int> path;
         generate(v1, v2, path, paths, prev);
         return paths;
      }
};

int main() {
   Solution s;
#if 0
   string str[] = {"hot","dot","dog","lot","log"};
   unordered_set<string> dict(begin(str), end(str));
   for (auto &v : s.findLadders("hit", "cog", dict)) {
      for (auto &s : v) {
         cout << s << " ";
      }
      cout << endl;
   }
#endif
   string str[] = {"si","go","se","cm","so","ph","mt","db","mb","sb","kr","ln","tm","le","av","sm","ar","ci","ca","br","ti","ba","to","ra","fa","yo","ow","sn","ya","cr","po","fe","ho","ma","re","or","rn","au","ur","rh","sr","tc","lt","lo","as","fr","nb","yb","if","pb","ge","th","pm","rb","sh","co","ga","li","ha","hz","no","bi","di","hi","qa","pi","os","uh","wm","an","me","mo","na","la","st","er","sc","ne","mn","mi","am","ex","pt","io","be","fm","ta","tb","ni","mr","pa","he","lr","sq","ye"};
   unordered_set<string> dict(begin(str), end(str));
   for (auto &v : s.findLadders("qa", "sq", dict)) {
      for (auto &s : v) {
         cout << s << " ";
      }
      cout << endl;
   }
   return 0;
}
