import java.util.*;
// Java Iterator interface reference:
// https://docs.oracle.com/javase/8/docs/api/java/util/Iterator.html
class PeekingIterator implements Iterator<Integer> {
   private Iterator<Integer> iterator;
   private Integer cache;

   public PeekingIterator(Iterator<Integer> iterator) {
      this.iterator = iterator;
      this.cache = null;
   }

   // Returns the next element in the iteration without advancing the iterator.
   public Integer peek() {
      if (cache != null) {
         return cache;
      }

      cache = iterator.next();

      return cache;
   }

   // hasNext() and next() should behave the same as in the Iterator interface.
   // Override them if needed.
   @Override
      public Integer next() {
         Integer result = null;

         if (cache != null) {
            result = cache;
            cache = null;
         }
         else {
            result = iterator.next();
         }

         return result;
      }

   @Override
      public boolean hasNext() {
         return cache != null ? true : iterator.hasNext();
      }
}
