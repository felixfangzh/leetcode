#include <iostream>
using namespace std;

class Solution {
public:
    int jump(int A[], int n) {
       int steps = 0;
       for (int i = 1, current = A[0], next = 0; i < n; ++i) {
          if (i > next) {
            if (current < i) return -1;
            next = current;
            ++steps;
          }
          current = max(current, i+A[i]);
       }
       return steps;
    }
};

int main() {
   Solution s;
   return 0;
}
