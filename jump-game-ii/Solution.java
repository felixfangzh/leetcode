public class Solution {
    public int jump(int[] A) {
        if (A.length <= 1) return 0;

        int jumps = 0;
        for (int i = 1, next = 0, current = A[0]; i < A.length; ++i) {
            if (next < i) {
                if (current < i) return -1;
                next = current;
                ++jumps;
            }
            current = Math.max(current, i + A[i]);
        }

        return jumps;
    }
}
