import java.util.*;

public class Solution {
    private void binaryTreePaths(TreeNode root, List<String> currentList, List<String> result) {
        currentList.add(String.valueOf(root.val));

        if(root.left == null && root.right == null) {
            result.add(String.join("->", currentList));
        }
        else 
        {
            if(root.left != null) {
                binaryTreePaths(root.left, currentList, result);
            }

            if(root.right != null) {
                binaryTreePaths(root.right, currentList, result);
            }
        }

        currentList.remove(currentList.size() - 1);
    }

    public List<String> binaryTreePaths(TreeNode root) {
        List<String> result = new ArrayList<String>();
        if (root == null) return result;

        binaryTreePaths(root, new ArrayList<String>(), result);
        return result;
    }
}
