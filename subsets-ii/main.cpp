#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

class Solution {
   private:
      void _subsetsWithDup(vector<int> &S, int start, vector<int> &path, vector<vector<int>> &subsets) {
         subsets.push_back(path);

         for (int i = start, size = S.size(); i < size; ++i) {
            if (i != start && S[i] == S[i-1])
               continue;
            path.push_back(S[i]);
            _subsetsWithDup(S, i+1, path, subsets);
            path.pop_back();
         }
      }
   public:
      vector<vector<int> > subsetsWithDup(vector<int> &S) {
         vector<vector<int> > subsets;
         vector<int> path;
         sort(S.begin(), S.end());
         _subsetsWithDup(S, 0, path, subsets);
         return subsets;
      }
};

int main() {
   Solution s;
   int vals [] = {1, 2, 2};
   vector<int> S(begin(vals), end(vals));
   for (auto &v : s.subsetsWithDup(S)) {
      for (auto i : v)
         cout << i << " ";
      cout << endl;
   }
   return 0;
}
