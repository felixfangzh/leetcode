#include <vector>
#include <iostream>
using namespace std;

class Solution {
   public:
      void rotate(vector<vector<int> > &matrix) {
         for (int i = 0, size = matrix.size(); i < size/2; ++i) {
            for (int j = i; j < size - i - 1; ++j) {
               int t = matrix[i][j];
               matrix[i][j] = matrix[size-j-1][i];
               matrix[size-j-1][i] = matrix[size-i-1][size-j-1];
               matrix[size-i-1][size-j-1] = matrix[j][size-i-1];
               matrix[j][size-i-1] = t;
            }
         }
      }
};

int main () {
   Solution s;
   vector<vector<int> > matrix;
   int vals [][3] = {
      {1, 2, 3},
      {4, 5, 6},
      {7, 8, 9},
   };
   for (int i = 0; i < 3; ++i) {
      vector<int> row(begin(vals[i]), end(vals[i]));
      matrix.push_back(row);
   }
   s.rotate(matrix);
   for (auto &v : matrix) {
      cout << "[";
      for (auto &i : v) {
      cout << i << " ";
      }
      cout << "]" << endl;
   }
   return 0;
}
