#include <iostream>
using namespace std;

struct TreeNode {
   int val;
   TreeNode *left;
   TreeNode *right;
   TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
   private:
      bool contains(TreeNode* root, TreeNode* p) {
         if (root == p) return true;

         if (!root) return false;

         if (root->left == p || root->right == p) return true;

         return contains(root->left, p) || contains(root->right, p);

      }
   public:
      TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
         if (root == p || root == q) return root;

         if (contains(root->left, p) && contains(root->left, q)) 
            return lowestCommonAncestor(root->left, p, q);

         if (contains(root->right, p) && contains(root->right, q)) 
            return lowestCommonAncestor(root->right, p, q);

         return root;
      }
};

int main() {
   return 0;
}
