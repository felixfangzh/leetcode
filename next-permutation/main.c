void nextPermutation(int num[], int n) {
    if (n <= 1) return;

    ssize_t i;
    for (i = n - 2; i >=0; --i) {
        if (num[i+1] > num[i]) break;
    }

    size_t a, b;
    int tmp;
    for (a = i + 1, b = n - 1; a < b; ++a, --b) {
        tmp = num[a];
        num[a] = num[b];
        num[b] = tmp;
    }

    if (i < 0) return;

    for (a = i + 1; a < n; ++a) {
        if (num[a] > num[i]) break;
    }


    tmp = num[a];
    num[a] = num[i];
    num[i] = tmp;
}
