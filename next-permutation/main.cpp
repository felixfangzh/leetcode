#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

class Solution {
   public:
      void nextPermutation(vector<int> &num) {
         if (num.size() <= 1) return;

         ssize_t i;

         for (i = num.size() - 2; i >= 0; --i) {
             if (num[i+1] > num[i]) break;
         }

         reverse(num.begin()+i+1, num.end());

         if (i < 0) return;

         swap(num[i], *upper_bound(num.begin()+i+1, num.end(), num[i]));
      }
};

#define ROW 3
#define COL 3
int main() {
   Solution s;
   int _input [][3] = {
      {1, 2, 3},
      {3, 2, 1},
      {1, 1, 5}
   };
   vector<vector<int>> input(ROW, vector<int>(COL));
   for (size_t i = 0; i < ROW; ++i)
      for (size_t j = 0; j < COL; ++j)
         input[i][j] = _input[i][j];
   for (auto &v : input)
      s.nextPermutation(v);
   for (size_t i = 0; i < ROW; ++i) {
      for (size_t j = 0; j < COL; ++j)
         cout << input[i][j] << " ";
      cout << endl;
   }
   return 0;
}
