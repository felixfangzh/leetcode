import java.util.*;

public class Solution {
   private static int hash(String s) {
      int hashValue = 0;
      
      for (int i = 0, n = Math.min(s.length(), 15); i < n; ++i) {
         int newHashValue = 0;
         switch(s.charAt(i)) {
            case 'A':
               newHashValue = 0;
               break;
            case 'C':
               newHashValue = 1;
               break;
            case 'G':
               newHashValue = 2;
               break;
            case 'T':
               newHashValue = 3;
               break;
         }
         hashValue = (int)((hashValue << 2) + newHashValue);
      }

      return hashValue;
   }

   public List<String> findRepeatedDnaSequences(String s) {
      List<String> sequences = new ArrayList<String>();
      if (null == s) return sequences;

      Map<Integer, Integer> hashMap = new HashMap<Integer, Integer>();

      for (int i = 0, n = s.length()-10; i <= n; ++i) {
         String substr = s.substring(i, i+10);
         int hashValue = hash(substr);
         Integer count = hashMap.get(hashValue);
         if (null == count) {
            hashMap.put(hashValue, 0);
         }
         else if (count == 0) {
            sequences.add(substr);
            hashMap.put(hashValue, 1);
         }
      }

      return sequences;
   }

   public static void main(String [] args) {
      Solution sol = new Solution();
      for (String s : sol.findRepeatedDnaSequences("AAAAAAAAAAA"))
         System.out.println(s);
      for (String s : sol.findRepeatedDnaSequences("AAAAAAAAAAAA"))
         System.out.println(s);
      for (String s : sol.findRepeatedDnaSequences("AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT"))
         System.out.println(s);
   }
}
