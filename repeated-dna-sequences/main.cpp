#include <iostream>
#include <vector>
#include <functional>
#include <unordered_map>

using namespace std;

class Solution {
   public:
      vector<string> findRepeatedDnaSequences(string s) {
         vector<string> ret;
         hash<string> hash_fn;
         unordered_map<size_t, size_t> map;

         for (ssize_t i = 0, n = s.size() - 10; i <= n; ++i) {
            string substr = s.substr(i, 10);
            if (++map[hash_fn(substr)] == 2) ret.push_back(substr);
         }

         return ret;
      }
};

int main() {
   Solution sol;
   for (auto &s : sol.findRepeatedDnaSequences(""))
      cout << s << " ";
   cout << endl;
   for (auto &s : sol.findRepeatedDnaSequences("AAAAAAAAAAAA"))
      cout << s << " ";
   cout << endl;
   return 0;
}
