#include <iostream>
using namespace std;

class Solution {
   public:
      int searchInsert(int A[], int n, int target) {
         int i = 0, j = n;
         while (i < j) {
            int mid = (i + j)/2;
            if (A[mid] == target)
               return mid;
            else if (A[mid] < target)
               i = mid + 1;
            else 
               j = mid;
         }
         return i;
      }
};

int main() {
   Solution s;
   int vals [] = {1,3,5,6};
   int targets [] = {5, 2, 7, 0};
   int answers [] = {2, 1, 4, 0};
   for (int i = 0, size = distance(begin(vals), end(vals)); i < size; ++i) {
      cout << "Expected: " << answers[i] << ", Actual: " <<  s.searchInsert(vals, size, targets[i]) << endl;
   }
   return 0;
}
