#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

class Solution {
   private:
      unordered_map<int, int> seq_size;
      int merge(int left, int right) {
         int upper = right + (seq_size[right] - 1);
         int lower = left - (seq_size[left] - 1);
         int len = upper - lower + 1;
         seq_size[upper] = seq_size[lower] = len;
         return len;
      }
   public:
      int longestConsecutive(vector<int> &num) {
         if (num.size() <= 1)
            return num.size();
         seq_size.clear();
         int longest = 1;
         for (auto &i : num) {
            if (seq_size.count(i))
               continue;
            seq_size[i] = 1;
            if (seq_size.count(i-1))
               longest = max(longest, merge(i-1, i));
            if (seq_size.count(i+1))
               longest = max(longest, merge(i, i+1));
         }

         return longest;
      }
};

int main() {
   return 0;
}
