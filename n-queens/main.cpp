#include <iostream>
#include <vector>
#include <stack>
#include <utility>
using namespace std;

class Solution {
   private:
      void updateAvail(vector<vector<bool> > &avail, int i, int j, stack<pair<int, int> > &changes) {
         for (int p = 0, size = avail.size(), _size = avail[0].size(); p < size; ++p) {
            for (int q = 0; q < _size; ++q) {
               if ((p == i || q == j || abs(p-i) == abs(q-j)) && avail[p][q]) {
                  avail[p][q] = false;
                  changes.push(make_pair(p, q));
               }
            }
         }
      }

      void undoAvail(vector<vector<bool> > &avail, stack<pair<int, int> > &changes) {
         while (!changes.empty()) {
            pair<int, int> t = changes.top(); changes.pop();
            avail[t.first][t.second] = true;
         }
      }

      void findSolutions(vector<string> &sol, vector<vector<string> > &solutions, vector<vector<bool> > &avail, int current_row, int n) {
         stack<pair<int, int> > changes;
         for (int i = 0; i < n; ++i) {
            if (avail[current_row][i]) {
               sol[current_row][i] = 'Q';
               if (current_row == n - 1) {
                  solutions.push_back(sol);
                  sol[current_row][i] = '.';
                  return;
               }
               updateAvail(avail, current_row, i, changes);
               findSolutions(sol, solutions, avail, current_row+1, n);
               undoAvail(avail, changes);
               sol[current_row][i] = '.';
            }
         }
      }
   public:
      vector<vector<string> > solveNQueens(int n) {
         vector<vector<string> > solutions;
         if (n <= 0)
            return solutions;
         vector<string> sol(n, string(n, '.'));
         vector<vector<bool> > avail(n, vector<bool>(n, true));
         findSolutions(sol, solutions, avail, 0, n);
         return solutions;
      }
};

int main() {
   Solution s;
   for (auto &v : s.solveNQueens(3)) {
      for (auto &S : v)
         cout << S << endl;
      cout << endl;
   }
   return 0;
}
