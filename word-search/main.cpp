#include <iostream>
#include <vector>
using namespace std;

class Solution {
   private:
      bool search(vector<vector<char> > &board, string &word, vector<vector<bool> > &visited, int current, int i, int j) {
         if (board[i][j] == word[current] && !visited[i][j]) {
            if (word.size() == current+1) 
               return true;
            visited[i][j] = true;
            if (i > 0 && search(board, word, visited, current+1, i-1, j))
               return true;
            if (i < board.size()-1 && search(board, word, visited, current+1, i+1, j))
               return true;
            if (j > 0 && search(board, word, visited, current+1, i, j-1))
               return true;
            if (j < board[0].size()-1 && search(board, word, visited, current+1, i, j+1))
               return true;
            visited[i][j] = false;
         }
         return false;
      }
   public:
      bool exist(vector<vector<char> > &board, string word) {
         if (board.empty() || word.empty())
            return false;
         vector<vector<bool> > visited(board.size(), vector<bool>(board[0].size(), false));

         for (int i = 0, size = board.size(), _size = board[0].size(); i < size; ++i) {
            for (int j = 0; j < _size; ++j) {
               if (search(board, word, visited, 0, i, j))
                  return true;
            }
         }
         return false;
      }
};

int main() {
   Solution s;
   int chars [][4] = {
      {'A', 'B', 'C', 'E'},
      {'S', 'F', 'C', 'S'},
      {'A', 'D', 'E', 'E'}
   };
   vector<vector<char> > board(3, vector<char>(4));
   for (int i = 0; i < 3; ++i) {
      for (int j = 0; j < 4; ++j) {
         board[i][j] = chars[i][j];
      }
   }

   cout << s.exist(board, "ABCCED") << ", expected true" << endl;
   cout << s.exist(board, "SEE") << ", expected true" << endl;
   cout << s.exist(board, "ABCB") << ", expected false" << endl;
   return 0;
}
