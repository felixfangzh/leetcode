#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution {
   private:
      void _combinationSum2(vector<int> &num, int currentSum, int start, int target, vector<int> &comb, vector<vector<int> > &combs) {
         if (currentSum == target) {
            combs.push_back(comb);
            return;
         }
         if (currentSum > target)
            return;
         for (int i = start, size = num.size(); i < size; ++i) {
            if (i != start && num[i] == num[i-1])
               continue;
            comb.push_back(num[i]);
            _combinationSum2(num, currentSum+num[i], i+1, target, comb, combs);
            comb.pop_back();
         }
      }
   public:
      vector<vector<int> > combinationSum2(vector<int> &num, int target) {
         vector<vector<int> > combs;
         vector<int> comb;
         sort(num.begin(), num.end());
         _combinationSum2(num, 0, 0, target, comb, combs);
         return combs;
      }
};

int main() {
   Solution s;
   //int vals [] = {10,1,2,7,6,1,5};
   int vals [] = {1, 1};
   vector<int> candidates(begin(vals), end(vals));
   for (auto &v : s.combinationSum2(candidates, 1)) {
      for (auto &i : v)
         cout << i << " ";
      cout << endl;
   }
   return 0;
}
