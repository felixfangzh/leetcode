int removeElement(int A[], int n, int elem) {
    int i, j, l = n;
    for (i = 0, j = 0; i < n; ++i) {
        if (A[i] != elem) {
            A[j++] = A[i];
        }
        else {
            --l;
        }
    }
    return l;
}

int main() {
    return 0;
}
