#include <iostream>
using namespace std;

class Solution {
   public:
      int removeElement(int A[], int n, int elem) {
         int l = n;
         for (int i = 0, j = 0; i < n; ++i) {
            if (A[i] != elem) {
               A[j++] = A[i];
            }
            else {
               --l;
            }
         }
         return l;
      }
};

int main() {
   Solution s;
   int A [] = {1, 2, 1, 3, 4};
   for (int i = 0, size = s.removeElement(A, distance(begin(A), end(A)), 1); i < size; ++i)
      cout << A[i] << " ";
   cout << endl;
   return 0;
}
