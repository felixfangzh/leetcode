#include <iostream>
#include <vector>
using namespace std;

class Solution {
   public:
      string getPermutation(int n, int k) {
         string res;
         int i, d(1), q;
         vector<int> num(n);
         for (i = 1; i <= n; ++i) {
            num[i-1] = i;
            d *= i;
         }
         --k;
         for (i = n; i > 0; --i) {
            d /= i;
            q = k/d;
            k = k - q*d;
            res += num[q] + '0';
            num.erase(num.begin() + q);
         }
         return res;
      }
};

int main() {
   Solution s;
   cout << s.getPermutation(3, 5) << endl;
   return 0;
}
