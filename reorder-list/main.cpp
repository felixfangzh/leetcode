#include <iostream>
#include <vector>
using namespace std;

struct ListNode {
   int val;
   ListNode *next;
   ListNode(int x) : val(x), next(NULL) {}
};

void printList(ListNode* head) {
   cout << "head->";
   for (ListNode* node = head; node; node = node->next) {
      cout << node->val << "->";
   }
   cout << "end" << endl;
}

class Solution {
   public:
      void reorderList(ListNode *head) {
         if (!head)
            return;
         if (!head->next)
            return;
         ListNode *p, *q;
         p = q = head;
         while (p && q 
               && (q = q->next) 
               && (q = q->next)
               && (p = p->next));
         ListNode* r = nullptr;
         q = p->next;
         p->next = nullptr;
         while (q) {
            ListNode* next = q->next;
            q->next = r;
            r = q;
            q = next;
         }

         p = head;
         
         while (p && r){
            ListNode* next = p->next;
            ListNode* next2 = r->next;
            p->next = r;
            r->next = next;
            if (next && next2) {
               r = next2->next;
               next2->next = next->next;
               p = next->next;
               next->next = next2;
            }
            else {
               break;
            }
         }
      }
};

int main() {
   int vals [] = {1,2,3,4,5,6};
   vector<ListNode*> nodes;
   for (auto i = begin(vals); i != end(vals); ++i)
      nodes.push_back(new ListNode(*i));
   for (auto it = nodes.begin(); it != prev(nodes.end()); ++it) {
      (*it)->next = *next(it);
   }
   printList(nodes.front());
   Solution s;
   s.reorderList(nodes.front());
   printList(nodes.front());
   return 0;
}
