#include <iostream>
#include <vector>
using namespace std;

class Solution {
private:
    void permute(vector<int> &num, size_t n, vector<vector<int> > &perms) {
        if (n <= 1) {
            perms.push_back(num);
            return;
        }
        
        for (size_t i = 0; i < n; ++i) {
            permute(num, n-1, perms);
            swap(num[(n & 1) == 1 ? 0 : i], num[n-1]);
        }
    }
    
public:
    vector<vector<int> > permute(vector<int> &num) {
        vector<vector<int> > perms;
        permute(num, num.size(), perms);
        return perms;
    }
};

int main() {
    int vals [] = {1, 2, 3};
    vector<int> num(begin(vals), end(vals));
    Solution s;
    for (auto &v : s.permute(num)) {
        for (auto &i : v)
            cout << i << " ";
        cout << endl;
    }
    return 0;
}
