import java.util.*;

public class Solution {
    private void permute(int[] num, LinkedHashSet<Integer> set, List<List<Integer>> lists) {
        if (set.size() == num.length) {
            lists.add(new ArrayList<Integer>(set));
            return;
        }

        for (int i = 0; i < num.length; ++i) {
            if (set.contains(num[i]))
                continue;
            set.add(num[i]);
            permute(num, set, lists);
            set.remove(num[i]);
        }
    }

    public List<List<Integer>> permute(int[] num) {
        List<List<Integer>> lists = new ArrayList<List<Integer>>();
        permute(num, new LinkedHashSet<Integer>(), lists);
        return lists;
    }

    public static void main(String [] args) {
    }
}
