#include <iostream>
#include <vector>
using namespace std;

struct ListNode {
   int val;
   ListNode *next;
   ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
   public:
      ListNode *swapPairs(ListNode *head) {
         if (!head || !head->next)
            return head;
         for (ListNode *current = head, *next = head->next; current && next; ) {
            ListNode *tmp = current;
            current->next = next->next && next->next->next ? next->next->next : next->next;
            current = next->next;
            next->next = tmp;
            if (tmp == head)
               head = next;
            if (current)
               next = current->next;
         }
         return head;
      }
};

int main() {
   Solution s;
   int vals [] = {1, 2, 3, 4};
   vector<ListNode*> nodes;
   for (auto i : vals)
      nodes.push_back(new ListNode(i));
   for (int i = 1; i < nodes.size(); ++i)
      nodes[i-1]->next = nodes[i];
   for (ListNode *current = s.swapPairs(nodes[0]); current; current = current->next)
      cout << current->val << " ";
   cout << endl;
   return 0;
}
