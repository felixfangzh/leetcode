/**
 * Definition for singly-linked list.
 */
public class Solution {
   public class ListNode {
      int val;
      ListNode next;
      ListNode(int x) {
         val = x;
         next = null;
      }
   }
   public ListNode swapPairs(ListNode head) {
      if (head == null) return head;
      int count = 1;
      ListNode prevprev = null, prev = head, current = head.next;
      for (; current != null; prev = current, current = current.next) {
         if (count < 1) {
            prevprev = prev; 
            ++count;
            continue;
         }
         count = 0;

         prev.next = current.next;
         current.next = prev;
         if (head == prev) head = current;
         if (prevprev != null) prevprev.next = current;
         prevprev = current;
         current = prev;
      }

      return head;
   }
}
