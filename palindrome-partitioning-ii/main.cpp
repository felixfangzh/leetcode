#include <iostream>
#include <climits>
#include <vector>
#include <unordered_map>
using namespace std;

class Solution {
   private:
      unordered_map<string, int> cuts;
      bool isPanlidrom(string s) {
         size_t n = s.size();
         for (int i = 0, size = n/2; i < size; ++i) 
            if (s[i] != s[n-i-1])
               return false;
         return true;
      }
   public:
      int minCut(string s) {
         if (isPanlidrom(s))
            return 0;

         int minCount = INT_MAX;

         for (int i = s.size() - 1; i > 0; --i) {
            string _s = s.substr(0, i);
            if (isPanlidrom(_s)) {
               string __s = s.substr(i);
               if (!cuts.count(__s))
                  cuts[__s] = minCut(__s);
               minCount = min(minCount, 1 + cuts[__s]);
               if (minCount == 1)
                  return 1;
            }
         }

         return minCount;
      }
};

int main() {
   Solution s;
   cout << s.minCut("fifgbeajcacehiicccfecbfhhgfiiecdcjjffbghdidbhbdbfbfjccgbbdcjheccfbhafehieabbdfeigbiaggchaeghaijfbjhi") << endl;
   return 0;
}

