#include <iostream>
#include <vector>
using namespace std;

class Solution {
   public:
      bool isValidSudoku(vector<vector<char> > &board) {
         if (board.empty())
            return false;

         bool row[9][9] = {{false}};
         bool col[9][9] = {{false}};
         bool cell[3][3][9] = {{{false}}};

         for (int i = 0, size = board.size(), _size = board[0].size(); i < size; ++i)
         {
            for (int j = 0; j < _size; ++j)
            {
               if (board[i][j] == '.')
                  continue;
               if (!row[i][board[i][j]-1-'0'] && !col[j][board[i][j]-1-'0'] && !cell[i/3][j/3][board[i][j]-1-'0'])
                  row[i][board[i][j]-1-'0'] = col[j][board[i][j]-1-'0'] = cell[i/3][j/3][board[i][j]-1-'0'] = true;
               else
                  return false;
            }
         }

         return true;

      }
};

int main() {
   Solution s;
   char vals [][9] = {
      {'5', '3', '.', '.', '7', '.', '.', '.', '.'}, 
      {'6', '.', '.', '1', '9', '5', '.', '.', '.'}, 
      {'.', '9', '8', '.', '.', '.', '.', '6', '.'}, 
      {'8', '.', '.', '.', '6', '.', '.', '.', '3'}, 
      {'4', '.', '.', '8', '.', '3', '.', '.', '1'}, 
      {'7', '.', '.', '.', '2', '.', '.', '.', '6'}, 
      {'.', '6', '.', '.', '.', '.', '2', '8', '.'}, 
      {'.', '.', '.', '4', '1', '9', '.', '.', '5'}, 
      {'.', '.', '.', '.', '8', '.', '.', '7', '9'}, 
   };

   vector<vector<char> > board(9, vector<char>(9));
   for (int i = 0; i < 9; ++i) {
      for (int j = 0; j < 9; ++j) {
         board[i][j] = vals[i][j];
      }
   }
   cout << s.isValidSudoku(board) << endl;
   return 0;
}
