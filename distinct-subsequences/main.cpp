#include <iostream>
#include <unordered_set>
#include <unordered_map>
#include <vector>
using namespace std;

class Solution {
   public:
      int numDistinct(string S, string T) {
         unordered_set<char> chars;
         unordered_map<char, vector<int>> inds;
         for (auto &c : T)
            chars.insert(c);
         for (int i = 0, size = S.size(); i < size; ++i)
            if (chars.count(S[i]))
               inds[S[i]].push_back(i);
         if (inds.size() != chars.size())
            return 0;
         unordered_map<int, int> counts;
         for (auto i : inds[T.back()])
            counts[i] = 1;
         for (int i = T.size()-2; i >= 0; --i) {
            unordered_map<int, int> previous;
            for (auto a : inds[T[i]])
               for (auto b : inds[T[i+1]])
                  if (a < b)
                     previous[a] += counts[b];
            counts = previous;
         }

         int num = 0;
         for (auto &i : counts)
            num += i.second;

         return num;
      }
};

int main() {
   Solution s;
   cout << s.numDistinct("rabbbit", "rabbit") << endl;
   return 0;
}
