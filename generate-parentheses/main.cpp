#include <iostream>
#include <vector>
using namespace std;

class Solution {
   public:
      vector<string> generateParenthesis(int n) {
         vector<string> ret;
         if (n <= 0)
            return ret;
         for (size_t i = 0, size = n; i < n; ++i) {
            vector<string> res, res2;
            for (auto &s : generateParenthesis(i))
               res.push_back("(" + s + ")");
            if (res.empty())
               res.push_back("()");
            for (auto &s : res)
               for (auto &_s : generateParenthesis(n-i-1))
                  res2.push_back(s + _s);
            if (res2.empty())
               res2.swap(res);
            ret.insert(ret.end(), res2.begin(), res2.end());
         }
         return ret;
      }
};

int main() {
   Solution s;
   for (auto &S : s.generateParenthesis(1))
      cout << S << " ";
   cout << endl;
   return 0;
}
