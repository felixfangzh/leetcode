import java.util.*;

public class Solution {
    public List<String> generateParenthesis(int n) {
        List<String> [] stringLists = new List[n + 1];
        stringLists[0] = Collections.EMPTY_LIST;
        return _generateParenthesis(stringLists, n);
    }

    public List<String> _generateParenthesis(List<String> [] stringLists, int n) {
        if (n < 0) return null;

        if (stringLists[n] != null) return stringLists[n];

        List<String> result = new ArrayList<String>();

        for (int i = 0; i < n; ++i) {
            List<String> subResult1 = new ArrayList<String>();
            List<String> subResult2 = new ArrayList<String>();
            for (String s : _generateParenthesis(stringLists, i))
                subResult1.add("(" + s + ")");
            if (subResult1.isEmpty())
                subResult1.add("()");
            for (String s1 : subResult1)
                for (String s2 : _generateParenthesis(stringLists, n - i -1))
                    subResult2.add(s1 + s2);
            if (subResult2.isEmpty())
                subResult2 = subResult1;

            result.addAll(subResult2);
        }

        return (stringLists[n] = new ArrayList<String>(result));
    }

    public static void test(int n) {
        Solution sol = new Solution();
        for (String s : sol.generateParenthesis(n))
            System.out.print(s + " ");
        System.out.println("\b");
    }

    public static void main(String [] args) {
        test(0);
        test(1);
        test(2);
        test(3);
        test(4);
    }
}
