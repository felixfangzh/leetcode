#include <iostream>
#include <vector>
#include <sstream>
using namespace std;

class Solution {
   public:
      string longestCommonPrefix(vector<string> &strs) {
         if (strs.empty()) return "";
         stringstream prefix;
         for (size_t i = 0, size = strs.size(); ;++i) {
            if (strs[0].empty() || i >= strs[0].size()) 
               return prefix.str();
            char c = strs[0][i];
            for (size_t j = 1; j < size; ++j)
               if (strs[j].empty() || i >= strs[j].size() || strs[j][i] != c)
                  return prefix.str();
            prefix << c;
         }
         return prefix.str();
      }
};

int main() {
   string _input [] = {"abb", "abc"};
   //string _input [] = {"fable", "fade", "favorite", "fan"};
   vector<string> input(begin(_input), end(_input));
   Solution s;
   cout << s.longestCommonPrefix(input) << endl;
   return 0;
}
