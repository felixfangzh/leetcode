public class Solution {
    public String longestCommonPrefix(String str1, String str2) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0, n = Math.min(str1.length(), str2.length()); i < n; ++i)
            if (str1.charAt(i) == str2.charAt(i)) builder.append(str1.charAt(i));
            else break;

        return builder.toString();
    }

    public String longestCommonPrefix(String[] strs) {
        if (strs.length == 0) return "";

        String prefix = strs[0];
        for (int i = 1; i < strs.length; ++i)
            prefix = longestCommonPrefix(prefix, strs[i]);

        return prefix;
    }
}
