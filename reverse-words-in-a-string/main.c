#include <stdio.h>
#include <string.h>

void reverse(char *start, char *end) {
    if (!start || !end) return;

    char tmp;
    for (; start < end; ++start, --end) {
        tmp = *start;
        *start = *end;
        *end = tmp;
    }
}

void reverseWords(char *s) {
    if (!s) return;

    reverse(s, s + strlen(s) - 1);

    char *p, *q = s, *r = s;

    while (1) {
        for (p = q; *p && *p == ' '; ++p); 
        if (!*p) {
            *r = '\0';
            return;
        }
        for (q = p; *q && *q != ' '; ++q); 
        reverse(p, q-1);
        if (s != r) *r++ = ' ';
        strncpy(r, p, q - p);
        r += q - p;
    }
}

int main() {
    char s [] = "   a   b ";
    reverseWords(s);
    printf("%s\n", s);
    return 0;
}
